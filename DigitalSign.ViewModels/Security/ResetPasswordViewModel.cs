﻿using DigitalSign.ViewModels.Validation;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Password can't be empty ")]
        [MinLength(10, ErrorMessage = "Password minimum 10 characters")]
        [PasswordCheck]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password can't be empty ")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
        public string Email { get; set; }
    }
}
