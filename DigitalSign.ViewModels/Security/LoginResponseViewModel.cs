﻿using DigitalSign.DataAccess.Models;
using DigitalSign.ViewModels.Master.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class LoginResponseViewModel
    {
        public bool IsSuccess { get; set; }
        public MasterUserViewModel Data { get; set; }
        public string Token { get; set; }
    }
}
