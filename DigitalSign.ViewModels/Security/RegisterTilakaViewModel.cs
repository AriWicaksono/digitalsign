﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class RegisterTilakaViewModel
    {
        public string registration_id { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string company_name { get; set; }
        public string nik { get; set; }
        public string photo_ktp { get; set; }
        public string photo_selfie { get; set; }
        public string phone_number { get; set; }
        public string date_expire { get; set; }
    }
}
