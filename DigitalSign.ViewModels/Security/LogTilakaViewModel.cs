﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class LogTilakaViewModel
    {
        public string? ID { get; set; }
        public string? ApiName { get; set; }
        public string? Method { get; set; }
        public string? EndPoint { get; set; }
        public string? RequestBody { get; set; }
        public string? ResponseBody { get; set; }
        public DateTime? RequestStart { get; set; }
        public DateTime? RequestEnd { get; set; }
        public string? RequestBy { get; set; }
        public string? ResponseStatus { get; set; }
        public string? ErrorMessage { get; set; }
        public int? ReffID { get; set; }
    }
}
