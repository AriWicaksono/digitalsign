﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class ResponseRegistrasi
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string[] Data { get; set; }

        public ResponseRegistrasi() { }
        public ResponseRegistrasi(bool success, string message, string[] data)
        {
            this.Success = success;
            this.Message = message;
            this.Data = data;
        }
    }

    public class CheckAccountResponse
    {
        public string tilaka_id { get; set; }
        public string message { get; set; }
        public bool status { get; set; }

        public CheckAccountResponse() { }
        public CheckAccountResponse(string id, string message, bool status)
        {
            this.tilaka_id = id;
            this.message = message;
            this.status = status;
        }
    }

    public class ResponeRequestSign
    {
        public bool success { get; set; }
        public string message { get; set; }
        public List<AuthUrls>? auth_urls { get; set; }
        public string failed_doc_name { get; set; }
    }

    public class AuthUrls
	{
        public string url { get; set; }
		public string user_identifier { get; set; }
	}

    public class ResponseDownloadPDF
    {
        public bool success { get; set; }
        public string message { get; set; }
        public string request_id { get; set; }
        public string status { get; set; }
        public List<PDFFile> signed_pdf { get; set; }
    }

    public class PDFFile
    {
        public int? signing_id { get; set; }
        public string pdf { get; set; }
        public string pdf_name { get; set; }
    }

    public class ResponeDocumentType
    {
        public bool success { get; set; }
        public string? message { get; set; }
        public List<DocumentType>? data { get; set; }
    }

    public class DocumentType
    {
        public string? Id { get; set; }
        public string? Kode { get; set; }
        public string? Nama { get; set; }
    }

    public class ResponseMaterai
    {
        public bool success { get; set; }
        public string? message { get; set; }
        public MateraiResult? data { get; set; }
    }

    public class MateraiResult
    {
        public string? result_doc { get; set; }
        public string? serial_number { get; set; }
        public string? materai_img { get; set; }
    }

    public class ResponseCheckAkunDS {
        public string? tilaka_id { get; set; }
        public string? message { get; set; }
        public bool status { get; set; }
    }
}
