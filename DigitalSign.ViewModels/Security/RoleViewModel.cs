﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class RoleViewModel
    {
        public int ID { get; set; } //(int, not null)
        public string Name { get; set; } //(varchar(50), not null)
        [Display(Name = "Active")]
        public bool IsActive { get; set; } //(bit, not null)
        public bool IsAdmin { get; set; }
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public string CreatedBy { get; set; } //(varchar(30), not null)
        public DateTime UpdatedDate { get; set; } //(datetime, not null)
        public string UpdatedBy { get; set; } //(varchar(30), not null)
        public List<RoleAccessViewModel> RoleAcccess { get; set; }
    }

    public class RoleDataLogViewModel
    {
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDate { get; set; }
        public string Activity { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string NameNew { get; set; }
        public string IsActive { get; set; }
        public string IsActiveNew { get; set; }
        public string IsAdmin { get; set; }
        public string IsAdminNew { get; set; }

    }
}
