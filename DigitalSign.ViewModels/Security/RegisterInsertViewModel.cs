﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class RegisterInsertViewModel
    {
        public string? Registration_Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string? CompanyName { get; set; }
        public string FirstName { get; set; }
        public string? MidleName { get; set; }
        public string? LastName { get; set; }
        public string? Code { get; set; }
        public string NIK { get; set; }
        public string BirthPlace { get; set; }
        public DateTime BirthDate { get; set; }
        public string? Address { get; set; }
        public string KTPFile { get; set; }
        public string KTPName { get; set; }
        public string PhotoFile { get; set; }
        public string PhotoName { get; set; }
    }

    public class RegisterAdminInsertViewModel
    {
        public string RoleID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string? CompanyName { get; set; }
        public string FirstName { get; set; }
        public string? MidleName { get; set; }
        public string? LastName { get; set; }
        public string? Code { get; set; }
        public string NIK { get; set; }
        public string BirthPlace { get; set; }
        public DateTime BirthDate { get; set; }
        public string? Address { get; set; }
        public string KTPFile { get; set; }
        public string KTPName { get; set; }
        public string PhotoFile { get; set; }
        public string PhotoName { get; set; }
    }
}
