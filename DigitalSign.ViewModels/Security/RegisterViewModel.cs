﻿using DigitalSign.ViewModels.Validation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Username can't be empty")]
        [MinLength(6, ErrorMessage = "Username Minimum 6 character")]
        [UsernameCheck]
        [StringLength(30, ErrorMessage = "Username can't longer than 30 characters")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Email can't be empty")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone Number can't be empty")]
        [StringLength(14, ErrorMessage = "Phone Number can't longer than 14 digits")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Phone number can't be filled other than numbers")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Password can't be empty")]
        [MinLength(10, ErrorMessage = "Password minimum 10 characters")]
        [PasswordCheck]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password can't be empty")]
        [ConfirmPassword]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Company Name can't be empty")]
        [StringLength(50, ErrorMessage = "Company Name can't longer than 50 characters")]
        public string? CompanyName { get; set; }

        //Identity
        [Required(ErrorMessage = "First Name can't be empty")]
        [StringLength(50, ErrorMessage = "First Name can't longer than 50 characters")]
        public string FirstName { get; set; }

        [StringLength(50, ErrorMessage = "Midle Name can't longer than 50 characters")]
        public string? MidleName { get; set; }

        [StringLength(50, ErrorMessage = "Last Name can't longer than 50 characters")]
        public string? LastName { get; set; }

        [Required(ErrorMessage = "NIK number can't be empty")]
        [StringLength(16, MinimumLength = 16, ErrorMessage = "NIK number must be 16 characters")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "NIK number can't be filled other than numbers")]
        public string NIK { get; set; }

        [Required(ErrorMessage = "Birth Place can't be empty")]
        [StringLength(50, ErrorMessage = "Birth Place can't longer than 50 character")]
        public string BirthPlace { get; set; }

        [Required(ErrorMessage = "Birth Date can't be empty")]
        public DateTime? BirthDate { get; set; }
        public string? Code { get; set; }

        [Required(ErrorMessage = "Address can't be empty")]
        [StringLength(50, ErrorMessage = "Addres can't longer than 500 character")]
        public string? Address { get; set; }

        [Required(ErrorMessage = "KTP file can't be empty")]
        [DisplayName("KTP")]
        [IsImage]
        [MaxImage]
        public IFormFile KTPFile { get; set; }

        [Required(ErrorMessage = "Photo file can't be empty")]
        [DisplayName("Profile")]
        [IsImage]
        [MaxImage]
        public IFormFile PhotoFile { get; set; }
    }

    public class RegisterAdminViewModel
    {
        [Required(ErrorMessage = "Username can't be empty")]
        [MinLength(6, ErrorMessage = "Username Minimum 6 character")]
        [StringLength(30, ErrorMessage = "Username can't longer than 30 characters")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Email can't be empty")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone Number can't be empty")]
        [StringLength(14, ErrorMessage = "Phone Number can't longer than 14 digits")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Phone number can't be filled other than numbers")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Password can't be empty")]
        [MinLength(8, ErrorMessage = "Password minimum 8 characters")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password can't be empty")]
        [ConfirmPasswordAdmin]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Company Name can't be empty")]
        [StringLength(50, ErrorMessage = "Company Name can't longer than 50 characters")]
        public string? CompanyName { get; set; }

        //Identity
        [Required(ErrorMessage = "First Name can't be empty")]
        [StringLength(50, ErrorMessage = "First Name can't longer than 50 characters")]
        public string FirstName { get; set; }

        [StringLength(50, ErrorMessage = "Midle Name can't longer than 50 characters")]
        public string? MidleName { get; set; }

        [StringLength(50, ErrorMessage = "Last Name can't longer than 50 characters")]
        public string? LastName { get; set; }

        [Required(ErrorMessage = "NIK number can't be empty")]
        [StringLength(16, MinimumLength = 16, ErrorMessage = "NIK number must be 16 characters")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "NIK number can't be filled other than numbers")]
        public string NIK { get; set; }

        [Required(ErrorMessage = "Role can't be empty")]
        public string RoleID { get; set; }
        public string? RoleName { get; set; }


        [Required(ErrorMessage = "Birth Place can't be empty")]
        [StringLength(50, ErrorMessage = "Birth Place can't longer than 50 character")]
        public string BirthPlace { get; set; }

        [Required(ErrorMessage = "Birth Date can't be empty")]
        public DateTime? BirthDate { get; set; }
        public string? Code { get; set; }

        [Required(ErrorMessage = "Address can't be empty")]
        [StringLength(50, ErrorMessage = "Addres can't longer than 500 character")]
        public string? Address { get; set; }

        [Required(ErrorMessage = "KTP file can't be empty")]
        [DisplayName("KTP")]
        [IsImage]
        [MaxImage]
        public IFormFile KTPFile { get; set; }

        [Required(ErrorMessage = "Photo file can't be empty")]
        [DisplayName("Profile")]
        [IsImage]
        [MaxImage]
        public IFormFile PhotoFile { get; set; }
    }
}
