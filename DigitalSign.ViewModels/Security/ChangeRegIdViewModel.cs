﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class ChangeRegIdViewModel
    {
        public string NIK { get; set; }
        public string RegistrationId { get; set; }
    }
}
