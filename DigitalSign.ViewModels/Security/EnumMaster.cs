﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
	public class ResponseStatus
	{
		public static readonly string Started = "started";
		public static readonly string Processing = "processing";
		public static readonly string Success = "success";
		public static readonly string Failed = "failed";
		public static readonly string Error = "error";
	}

	public class RoleUserRequester
	{
		public static readonly string SuperAdmin = "Admin";
		public static readonly string Signer = "User";
	}
}
