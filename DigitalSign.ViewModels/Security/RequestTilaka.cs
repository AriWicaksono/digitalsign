﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class CheckAccountRequest
    {
        public string request_id { get; set; }
        public string nik { get; set; }

        public CheckAccountRequest(string request_id, string nik)
        {
            this.request_id = request_id;
            this.nik = nik;
        }
    }
}
