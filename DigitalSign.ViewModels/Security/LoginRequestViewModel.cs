﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class LoginRequestViewModel
    {
        [Required(ErrorMessage = "Username harus di isi")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password harus di isi")]
        public string Password { get; set; }
    }
}
