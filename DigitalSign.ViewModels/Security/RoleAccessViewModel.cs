﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class RoleAccessViewModel
    {
        public long ID { get; set; }

        public int RoleId { get; set; }

        public string Code { get; set; }

        public bool IsAllow { get; set; }
    }
}
