﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Security
{
    public class MenuViewModel
    {
        public int ID { get; set; } //(int, not null)
        public string GroupMenu { get; set; } //(varchar(20), not null)
        public string Title { get; set; } //(varchar(50), not null)
        public string NavigationTo { get; set; } //(varchar(50), not null)
        public int Seq { get; set; } //(int, not null)
        public string Icon { get; set; } //(varchar(30), not null)
        public int? ParentMenuID { get; set; } //(int, null)
        public bool IsActive { get; set; } //(bit, not null)
        public bool IsRoot { get; set; } //(bit, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public string CreatedBy { get; set; } //(varchar(30), not null)
        public DateTime UpdatedDate { get; set; } //(datetime, not null)
        public string UpdatedBy { get; set; } //(varchar(30), not null)
    }

    public class Menus
    {
        public int id { get; set; }
        public string Name { get; set; }
        public bool hasChildren { get; set; }
    }

    public class AccessMenuViewModel
    {
        public int MenuID { get; set; }
        public string MenuTitle { get; set; }
        public IDictionary<RoleViewModel, bool> ToggleAccessMenu { get; set; }
        public IEnumerable<RoleViewModel> GetRole { get; set; }
        public IEnumerable<MenuViewModel> MenuChild { get; set; }
        public IEnumerable<MenuViewModel> MenuParent { get; set; }
        public List<int> ListFromDB { get; set; }
        [Display(Name = "Role")]
        public int RoleID { get; set; }
    }
}
