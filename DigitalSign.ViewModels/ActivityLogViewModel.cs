﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels
{
	public class ActivityLogViewModel
	{
		public int ID { get; set; }
		public int UserID { get; set; }
		public string Activity { get; set; }
		public DateTime TimeStamp { get; set; }
		public string Deskripsi { get; set; }
	}
}
