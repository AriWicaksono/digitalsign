﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels
{
    public class AjaxViewModel
    {
        public AjaxViewModel()
        {
        }

        public AjaxViewModel(bool isSuccess, object data, string message)
        {
            this.isSucceed = isSuccess;
            Data = data;
            Message = message;
        }

        public void SetValues(bool isSuccess, object data, string message)
        {
            this.isSucceed = isSuccess;
            Data = data;
            Message = message;
        }

        public void SetFailedWithEmptyList(string message)
        {
            isSucceed = false;
            Data = new List<object>();
            Message = message;
        }
        public void SetOkWithEmptyList(string message)
        {
            isSucceed = true;
            Data = new List<object>();
            Message = message;
        }
        public bool isSucceed { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }
    }

    public class AjaxViewModel<T> where T : class
    {
        public AjaxViewModel()
        {
        }

        public AjaxViewModel(bool isSuccess, T data, string message)
        {
            this.isSucceed = isSuccess;
            Data = data;
            Message = message;
        }

        public void SetValues(bool isSuccess, T data, string message)
        {
            this.isSucceed = isSuccess;
            Data = data;
            Message = message;
        }

        public bool isSucceed { get; set; }
        public T Data { get; set; }
        public string Message { get; set; }
    }
}
