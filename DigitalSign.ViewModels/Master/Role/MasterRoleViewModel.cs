﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Role
{
    public class MasterRoleViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Nama tidak boleh kosong!")]
        public string? Name { get; set; }

        [Required(ErrorMessage = "Code tidak boleh kosong!")]
        [MinLength(10, ErrorMessage = "Role Minimum 6 character")]
        public string? Code { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }
        public string? Description { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DelDate { get; set; }
    }
}
