﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Document
{
    public class DocumentSignViewModel
    {
        public int? ID { get; set; }
        public int DocumentID { get; set; }
        public int UserID { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public decimal CoorX { get; set; }
        public decimal CoorY { get; set; }
        public int PageNumber { get; set; }
    }
}
