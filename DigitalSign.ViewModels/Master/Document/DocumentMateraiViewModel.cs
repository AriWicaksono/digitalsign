﻿using DigitalSign.ViewModels.Validation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Document
{
    public class DocumentMateraiViewModel
    {
        public int ID { get; set; }
        public int? MateraiID { get; set; }
        [Required(ErrorMessage = "Title can't be null")]
        public string? DocumentName { get; set; }
        public string? FileName { get; set; }
        [Required(ErrorMessage = "Location can't be null")]
        public string? Location { get; set; }
        public decimal VisLLX { get; set; }
        public decimal VisLLY { get; set; }
        public decimal VisURX { get; set; }
        public decimal VisURY { get; set; }
        public int Page { get; set; }
        [Required(ErrorMessage = "Reason can't be null")]
        public string? Reason { get; set; }
        [Required(ErrorMessage = "Document Type can't be null")]
        public string? DocumentType { get; set; }
        [Required(ErrorMessage = "Document Type can't be null")]
        public string? DocumentTypeCode { get; set; }
        public string? StampedDoc { get; set; }
        public string? SerialNumber { get; set; }
        public string? Materai { get; set; }
        public DateTime? StampDate { get; set; }
        public string? ContentPDF { get; set; }
        public string? UserLogin { get; set; }
        public IFormFile DocumentFile { get; set; }
    }
    public class DocumentMateraiNewViewModel {
        //public int ID { get; set; }
        public string ID { get; set; }
        public int MateraiID { get; set; }
        public string? DocumentName { get; set; }
        public string? FileName { get; set; }
        public string? FilePath { get; set; }
        public string? Location { get; set; }
        public decimal VisLLX { get; set; }
        public decimal VisLLY { get; set; }
        public decimal VisURX { get; set; }
        public decimal VisURY { get; set; }
        public int Page { get; set; }
        public string? Reason { get; set; }
        public string? DocumentType { get; set; }
        public string? DocumentTypeCode { get; set; }
        public string? StampedDoc { get; set; }
        public string? SerialNumber { get; set; }
        public string? Materai { get; set; }
        public DateTime? StampDate { get; set; }
        public string? ContentPDF { get; set; }
        public string UserLogin { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? LastSubmit { get; set; }
        public bool IsSuperAdmin { get; set; }
    }

    public class DocumentMateraiDetailViewModel
    {
        public string ID { get; set; }
        public int MateraiID { get; set; }
        public string? DocumentName { get; set; }
        public string? FileName { get; set; }
        public string? FilePath { get; set; }
        public string? Location { get; set; }
        public decimal VisLLX { get; set; }
        public decimal VisLLY { get; set; }
        public decimal VisURX { get; set; }
        public decimal VisURY { get; set; }
        public int Page { get; set; }
        public string? Reason { get; set; }
        public string? DocumentType { get; set; }
        public string? DocumentTypeCode { get; set; }
        public string? StampedDoc { get; set; }
        public bool IsStampped { get; set; }
        public string? SerialNumber { get; set; }
        public string? Materai { get; set; }
        public DateTime? StampDate { get; set; }
        public string? ContentPDF { get; set; }
        public string UserLogin { get; set; }
    }
}
