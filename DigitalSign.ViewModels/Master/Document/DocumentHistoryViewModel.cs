﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Document
{
    public class DocumentHistoryViewModel
    {
        public int? ID { get; set; }
        public int UserId { get; set; }
        public string? Username { get; set; }
        public int? DocumentID { get; set; }
        public string Activity { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
