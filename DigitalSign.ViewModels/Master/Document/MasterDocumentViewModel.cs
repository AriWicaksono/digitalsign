﻿using DigitalSign.ViewModels.Master.RoleMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Document
{
    public class MasterDocumentViewModel
    {
        public int? ID { get; set; }
        public string Title { get; set; }
        public string DocumentName { get; set; }
        public string DocumentFile { get; set; }
        public string? Status { get; set; }
        public string? Category { get; set; }
        public string? CategoryName { get; set; }
        public string? RequestID { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DelDate { get; set; }
        public List<DropdownList>? ListIdUser { get; set; }
        public List<DocumentDiscussionViewModel>? DocumentDiscussion { get; set; }
        public List<DocumentParticipantViewModel>? DocumentParticipant { get; set; }
        public List<DocumentHistoryViewModel>? DocumentHistory { get; set; }

    }
}
