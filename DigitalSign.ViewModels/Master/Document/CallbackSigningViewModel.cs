﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Document
{
    public class CallbackSigningViewModel
    {
        public string request_id { get; set; }
        public bool success { get; set; }
        public string signer { get; set; }
        public string? signer_next { get; set; }
        public string? url { get; set; }
    }
}
