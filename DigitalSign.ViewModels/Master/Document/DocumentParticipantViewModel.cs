﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Document
{
    public class DocumentParticipantViewModel
    {
        public int? ID { get; set; }
        public int? DocumentID { get; set; }
        public int UserID { get; set; }
        public string? Username { get; set; }
        public int Sequence { get; set; }
        public string? Status { get; set; }
    }
}
