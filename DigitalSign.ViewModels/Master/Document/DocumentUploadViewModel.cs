﻿using DigitalSign.ViewModels.Validation;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace DigitalSign.ViewModels.Master.Document
{
    public class DocumentUploadViewModel
    {
        [Required(ErrorMessage = "Title can't be null")]
        public string Title { get; set; }
        public string Category { get; set; }
        public string? CategoryName { get; set; }

        [DocumentCheck]
        public List<IFormFile>? DocumentFileNew { get; set; }

        [CheckParticipantList]
        public string ListParticipant { get; set; }
    }
}
