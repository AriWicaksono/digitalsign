﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Document
{
    public class RequestSignViewModel
    {
        public string? request_id { get; set; }
        public bool send_email { get; set; }
        public List<signature> signatures { get; set; }
        public List<document> list_pdf { get; set; }

    }

    public class document
    {
        public string file_name { get; set; }
        public string file { get; set; }
        public List<signatureConfig> signatures { get; set; }
    }

    public class signature
    {
        public string user_identifier { get; set; }
        public string signature_image { get; set; }
        public int sequence { get; set; }
    }

    public class signatureConfig
    {
        public string user_identifier { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        public decimal coordinate_x { get; set; }
        public decimal coordinate_y { get; set; }
        public int page_number { get; set; }
    }

    public class RequestStampMaterai
    {
        public string file_name { get; set; }
        public string content_pdf { get; set; }
        public string nama_doc { get; set; }
        public string location { get; set; }
        public decimal visLLX { get; set; }
        public decimal visLLY { get; set; }
        public decimal visURX { get; set; }
        public decimal visURY { get; set; }
        public string signature_page { get; set; }
        public string reason { get; set; }
    }

    public class RequestRestampMaterai 
    {
        public string file_name { get; set; }
        public string content_pdf { get; set; }
        public string nama_doc { get; set; }
        public string location { get; set; }
        public decimal visLLX { get; set; }
        public decimal visLLY { get; set; }
        public decimal visURX { get; set; }
        public decimal visURY { get; set; }
        public string signature_page { get; set; }
        public string reason { get; set; }
        public string serial_number { get; set; }
        public string materai_img { get; set; }
    }
}
