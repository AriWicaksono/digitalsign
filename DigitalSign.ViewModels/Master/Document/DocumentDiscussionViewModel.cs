﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Document
{
    public class DocumentDiscussionViewModel
    {
        public int ID { get; set; }
        public int DocumentID { get; set; }
        public int UserID { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Message { get; set; }
        public string? Poto { get; set; }
        public string? FullName { get; set; }
    }
}
