﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Document
{
    public class MateraiViewModel
    {
        public int ID { get; set; }
        public string UserID { get; set; }
        public int Quota { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalPaid { get; set; }
        public int QuotaUsed { get; set; }
        public DateTime TrxDate { get; set; }
    }
}
