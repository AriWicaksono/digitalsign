﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master
{
    public class DropDownList
    {
        public string? Text { get; set; }
        public string? Value { get; set; }

    }
}
