﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.RoleMenu
{
    public class MasterRoleMenuViewModel
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "Role tidak boleh kosong!")]
        public int? RoleID { get; set; }
        public string? RoleName { get; set; }
        [Required(ErrorMessage = "Menu tidak boleh kosong!")]
        public int? MenuID { get; set; }
        public string? Menu { get; set; }
        public List<DropdownList>? ListRole { get; set; }
        public List<DropdownList>? ListMenu { get; set; }

    }
    public class DropdownList
    {
        public string? Text { get; set; }
        public string? Value { get; set; }
    }
}
