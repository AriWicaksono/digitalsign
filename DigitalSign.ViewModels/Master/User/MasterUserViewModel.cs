﻿using DigitalSign.ViewModels.Validation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.User
{
    public class MasterUserViewModel
    {
        public int ID { get; set; }
        public string? RegistrationID { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? Fullname { get; set; }
        public DateTime? DateExpire { get; set; }
        public int RoleID { get; set; }
        public string? RoleName { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; } = null!;
        public string PhoneNumber { get; set; }
        public string? CompanyName { get; set; }
        public string? Status { get; set; }
        public string FirstName { get; set; }
        public string? MiddleName { get; set; }
        public string? LastName { get; set; }
        public string NIK { get; set; }
        public string BirthPlace { get; set; }
        public DateTime BirthDate { get; set; }
        public string? Address { get; set; }
        public string? KTPFile { get; set; }
        public string? PhotoFile { get; set; }
        public string? Signature { get; set; }
        public IFormFile? SignatureFile { get; set; }
        public string? Certificate { get; set; }
        public IFormFile? CertificateFile { get; set; }
        public string? TilakaName { get; set; }
        public List<DropDownList>? ListRole { get; set; }

        public AccountInfoUserViewModel AccountInfo() {
            AccountInfoUserViewModel accountInfoUserViewModel = new AccountInfoUserViewModel()
            {
                ID = this.ID,
                Email = this.Email,
                Username = this.Username,
                PhoneNumber = this.PhoneNumber,
                CompanyName = this.CompanyName,
                NIK = this.NIK,
                PhotoFile = this.PhotoFile,
                Photo = null,
                Certificate = this.Certificate,
                CertificateFile = null,
                Signature = this.Signature,
                SignatureFile = null
            };

            return accountInfoUserViewModel;
        }

        public ChangePasswordUserViewModel ChangePassword() {
            ChangePasswordUserViewModel changePasswordUserViewModel = new ChangePasswordUserViewModel()
            {
                ID = this.ID,
                PhotoFile = this.PhotoFile
            };
            return changePasswordUserViewModel;
        }

    }

    public class AccountInfoUserViewModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage ="*Username harus diisi")]
        [StringLength(30,ErrorMessage ="*Username maksimal 30 karakter")]
        [MinLength(6,ErrorMessage = "*Username minimal 6 karakter")]
        [UsernameCheck]
        public string Username { get; set; }
        
        [Required(ErrorMessage = "*Email harus diisi")]
        public string Email { get; set; }
        
        [Required(ErrorMessage = "*Phone Number harus diisi")]
        public string PhoneNumber { get; set; }
        
        [Required(ErrorMessage = "*Company Name harus diisi")]
        [StringLength(50, ErrorMessage ="*Company Name maksimal 50 karakter")]
        public string CompanyName { get; set; }
        
        public string? NIK { get; set; }
        public string? PhotoFile { get; set; }
        public IFormFile? Photo { get; set; }
        public string? Signature { get; set; }
        public IFormFile? SignatureFile { get; set; }
        public string? Certificate { get; set; }
        public IFormFile? CertificateFile { get; set; }

    }

    public class ChangePasswordUserViewModel
    {
        public int ID { get; set; }
        
        [Required(ErrorMessage = "*Current Password harus diisi")]
        [DisplayName("Current Password")]
        public string CurrentPassword { get; set; }
        
        [Required(ErrorMessage = "*New Password harus diisi")]
        [MinLength(10, ErrorMessage = "Password minimum 10 characters")]
        [PasswordCheck]
        [DisplayName("New Password")]
        public string NewPassword { get; set; }
        
        [Required(ErrorMessage = "*Confirm Password harus diisi")]
        [Compare("NewPassword", ErrorMessage = "*New Password dan Confirm Password tidak cocok")]
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }
        public string? PhotoFile { get; set; }
    }

    public class UploadCertificateViewModel
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Certificate { get; set; }
    }

    public class CreateSignViewModel
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Signature { get; set; }
    }

    public class Dropdown
    {
        public string? Text { get; set; }
        public string? Value { get; set; }
    }
}
