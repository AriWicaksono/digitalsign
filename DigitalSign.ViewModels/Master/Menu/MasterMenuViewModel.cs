﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Menu
{
    public class MasterMenuViewModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string NavigationTo { get; set; } = null!;
        public int Seq { get; set; }
        public int? ParentMenuID { get; set; }
        public bool IsRoot { get; set; }
        public bool IsActive { get; set; }
        public string? Icon { get; set; }
        public string? Description { get; set; }


        //
        public string? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime? DelDate { get; set; }
    }
}
