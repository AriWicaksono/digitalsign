﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Master.Notification
{
    public class NotificationViewModel
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public string? Detail { get; set; }
        public int? UserID { get; set; }
        public string Icon { get; set; }
        public string RedirectTo { get; set; }
        public DateTime? ReadDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string? CreatedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public string? DeletedBy { get; set; }

    }
}
