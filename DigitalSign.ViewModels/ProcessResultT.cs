﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels
{
    public class ProcessResultT<T> : ProcessResult
    {
        public T returnValue
        {
            get;
            set;
        }

        public ProcessResultT()
        {
        }

        public ProcessResultT(bool isSucceed, string message)
        {
            base.isSucceed = isSucceed;
            base.message = message;
        }

        public ProcessResultT(bool isSucceed, string message, T returnValue)
        {
            base.isSucceed = isSucceed;
            base.message = message;
            this.returnValue = returnValue;
        }

        public void InsertSucceed(T returnValue)
        {
            this.returnValue = returnValue;
            base.InsertSucceed();
        }

        public void UpdateSucceed(T returnValue)
        {
            this.returnValue = returnValue;
            base.UpdateSucceed();
        }

        public void UploadSucceed(T returnValue)
        {
            this.returnValue = returnValue;
            base.UploadSucceed();
        }

        public void DeleteSucceed(T returnValue)
        {
            base.isSucceed = true;
            base.DeleteSucceed();
        }

        public void ProcessSucceed(T returnValue)
        {
            base.isSucceed = true;
            base.message = "Process Succeed.";
            this.returnValue = returnValue;
        }

        public void ValidateSucceed(T returnValue)
        {
            base.isSucceed = true;
            base.message = "Validate Succeed.";
            this.returnValue = returnValue;
        }

        public void DataIsExistT(string id)
        {
            base.isSucceed = true;
            base.DataIsExist(id);
        }
    }
}
