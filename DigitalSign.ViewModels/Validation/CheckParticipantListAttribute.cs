﻿using DigitalSign.ViewModels.Master.Document;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Validation
{
    public class CheckParticipantListAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                var list = GetProperty(validationContext);
                if (Check(list))
                {
                    return ValidationResult.Success;
                }
            }
            catch (Exception)
            {

                throw;
            }
            ErrorMessage = "Participant can't be null";
            return new ValidationResult(ErrorMessage);
        }

        private string? GetProperty(ValidationContext validationContext)
        {
            var model = (DocumentUploadViewModel)validationContext.ObjectInstance;
            return model.ListParticipant;
        }

        private bool Check(string ListParticipant)
        {
            var listParticipant = JsonConvert.DeserializeObject<List<DocumentParticipantViewModel>>(ListParticipant);
            if (listParticipant.Count() < 1)
            {
                return false;
            }
            return true;
        }
    }
}
