﻿using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Validation
{
    public class UsernameCheckAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                //var username = GetUsername(validationContext);
                var username = value == null ? null : value.ToString();
                var isNumber = Regex.IsMatch(username, @"[0-9]");
                var isLower = Regex.IsMatch(username, @"[a-z]");
                var isUppercase = Regex.IsMatch(username, @"[A-Z]");
                var isWrongCaracter = Regex.IsMatch(username, @"[^a-zA-Z0-9]");
                if (isNumber && isLower && isLower)
                {
                    if (isWrongCaracter)
                    {
                        ErrorMessage = "Username tidak boleh mengandung character spesial";
                        return new ValidationResult(ErrorMessage);
                    }
                    else
                    {
                        return ValidationResult.Success;
                    }
                }
                else
                {
                    ErrorMessage = "Username harus mengandung minimal 1 uppercase, 1 lowercase, 1 angka";
                    return new ValidationResult(ErrorMessage);
                }

            }
            catch (Exception e)
            {
                return new ValidationResult(e.Message);
            }
            
        }

        private string? GetUsername(ValidationContext validationContext)
        {
            var model = (RegisterViewModel)validationContext.ObjectInstance;
            return model.Username;
        }
    }
}
