﻿using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Validation
{
    public class PasswordCheckAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                //var password = GetPassword(validationContext);
                var password = value == null ? null : value.ToString();
                var isNumber = Regex.IsMatch(password, @"[0-9]");
                var isLower = Regex.IsMatch(password, @"[a-z]");
                var isUppercase = Regex.IsMatch(password, @"[A-Z]");
                var isSpCaracter = Regex.IsMatch(password, @"[@#$%^&+=]");
                if (isNumber && isLower && isLower && isSpCaracter)
                {
                    return ValidationResult.Success;
                }
                else
                {
                    ErrorMessage = @"Password harus mengandung minimal 1 uppercase, 1 lowercase, 1 angka, 1 spesial character @#$%^&+=";
                    return new ValidationResult(ErrorMessage);
                }

            }
            catch (Exception e)
            {
                return new ValidationResult(e.Message);
            }
        }

        private string? GetPassword(ValidationContext validationContext)
        {
            var model = (RegisterViewModel)validationContext.ObjectInstance;
            return model.Password;
        }
    }
}
