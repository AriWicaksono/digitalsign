﻿using DigitalSign.ViewModels.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Validation
{
    public class ConfirmPasswordAdminAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                var pswrd = GetPassword(validationContext);
                var confPswrd = GetConfirmPassword(validationContext);

                if (pswrd != confPswrd)
                {
                    ErrorMessage = "Confirm Password Tidak Sesuai";
                    return new ValidationResult(ErrorMessage);
                }
                else
                {
                    return ValidationResult.Success;
                }

            }
            catch (Exception e)
            {
                return new ValidationResult(e.Message);
            }
        }

        private string? GetPassword(ValidationContext validationContext)
        {
            var model = (RegisterAdminViewModel)validationContext.ObjectInstance;
            return model.Password;
        }

        private string? GetConfirmPassword(ValidationContext validationContext)
        {
            var model = (RegisterAdminViewModel)validationContext.ObjectInstance;
            return model.ConfirmPassword;
        }

    }
}