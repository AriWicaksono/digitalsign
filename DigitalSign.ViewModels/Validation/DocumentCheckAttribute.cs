﻿using DigitalSign.ViewModels.Master.Document;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace DigitalSign.ViewModels.Validation
{
    public class DocumentCheckAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                var documents = GetDocument(validationContext);
                if (documents == null)
                {
                    return new ValidationResult("Document can't be null");
                }
                else
                {
                    bool check = true;
                    foreach (var document in documents)
                    {
                        if (document.ContentType != "application/pdf")
                        {
                            ErrorMessage = "Document must be .pdf format";
                            check = false;
                            break;
                        }
                        else
                        {
                            if (document.Length > 30000000)
                            {
                                ErrorMessage = "Maximum Document is 30MB";
                                check = false;
                                break;
                            }
                        }

                    }

                    if (!check)
                    {
                        return new ValidationResult(ErrorMessage);
                    }
                    else
                    {
                        return ValidationResult.Success;
                    }
                }
                
            }
            catch (Exception e)
            {
                return new ValidationResult(e.Message);
            }
            
        }

        private List<IFormFile>? GetDocument(ValidationContext validationContext)
        {
            var model = (DocumentUploadViewModel)validationContext.ObjectInstance;
            return model.DocumentFileNew;
        }
    }
}
