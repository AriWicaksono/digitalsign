﻿using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Validation
{
    public class IsImageAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                ErrorMessage = "Image file can't be empty";
                return new ValidationResult(ErrorMessage);
            }
            else
            {
                IFormFile image = (IFormFile)value;
                List<string> validFormat = new List<string> { "image/jpeg", "image/png" };
                if (validFormat.Contains(image.ContentType.ToLower()))
                {
                    return ValidationResult.Success;
                }
                else
                {
                    ErrorMessage = $"Input {validationContext.DisplayName} must be type ( image/jpg or image/png)";
                    return new ValidationResult(ErrorMessage);
                }
            }
            
        }
    }
}
