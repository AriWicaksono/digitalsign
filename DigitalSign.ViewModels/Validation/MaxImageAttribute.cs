﻿using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.ViewModels.Validation
{
    public class MaxImageAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                ErrorMessage = "Image file can't be empty";
                return new ValidationResult(ErrorMessage);
            }
            else
            {
                IFormFile image = (IFormFile)value;
                if (image.Length > 500000)
                {
                    ErrorMessage = $"{validationContext.DisplayName} Image can't grather than from 0.5 MB";
                    return new ValidationResult(ErrorMessage);
                }
                return ValidationResult.Success;
            }
        }
    }
}
