﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class Role
    {
        public Role()
        {
            RoleMenu = new HashSet<RoleMenu>();
            Users = new HashSet<Users>();
        }

        [Key]
        public int ID { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string Code { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string Name { get; set; } = null!;
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }
        [StringLength(200)]
        [Unicode(false)]
        public string? Description { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DelDate { get; set; }

        [InverseProperty("Role")]
        public virtual ICollection<RoleMenu> RoleMenu { get; set; }
        [InverseProperty("Role")]
        public virtual ICollection<Users> Users { get; set; }
    }
}
