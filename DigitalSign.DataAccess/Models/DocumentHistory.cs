﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class DocumentHistory
    {
        [Key]
        public int ID { get; set; }
        public int DocumentID { get; set; }
        public int UserID { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string Activity { get; set; } = null!;
        [Column(TypeName = "datetime")]
        public DateTime TimeStamp { get; set; }

        [ForeignKey("DocumentID")]
        [InverseProperty("DocumentHistory")]
        public virtual Document Document { get; set; } = null!;
        [ForeignKey("UserID")]
        [InverseProperty("DocumentHistory")]
        public virtual Users User { get; set; } = null!;
    }
}
