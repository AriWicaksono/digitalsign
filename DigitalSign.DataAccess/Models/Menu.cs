﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class Menu
    {
        public Menu()
        {
            RoleMenu = new HashSet<RoleMenu>();
        }

        [Key]
        public int ID { get; set; }
        [StringLength(30)]
        [Unicode(false)]
        public string Title { get; set; } = null!;
        [StringLength(200)]
        [Unicode(false)]
        public string NavigationTo { get; set; } = null!;
        public int Seq { get; set; }
        public int? ParentMenuID { get; set; }
        public bool IsRoot { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Icon { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string? Description { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DelDate { get; set; }

        [InverseProperty("Menu")]
        public virtual ICollection<RoleMenu> RoleMenu { get; set; }
    }
}
