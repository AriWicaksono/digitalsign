﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class GlobalConfiguration
    {
        [Key]
        [StringLength(50)]
        [Unicode(false)]
        public string KeyName { get; set; } = null!;
        [Unicode(false)]
        public string KeyValue { get; set; } = null!;
        [Column(TypeName = "text")]
        public string? KeyDescription { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? CreatedWho { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedWhen { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? ChangedWho { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ChangedWhen { get; set; }
    }
}
