﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class TilakaApiLog
    {
        [StringLength(200)]
        [Unicode(false)]
        public string? ApiName { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Method { get; set; }
        [Unicode(false)]
        public string? EndPoint { get; set; }
        [Unicode(false)]
        public string? RequestBody { get; set; }
        [Unicode(false)]
        public string? ResponseBody { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RequestStart { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RequestEnd { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string? RequestBy { get; set; }
        [Key]
        public int ID { get; set; }
        [StringLength(20)]
        [Unicode(false)]
        public string? ResponseStatus { get; set; }
        [Unicode(false)]
        public string? ErrorMessage { get; set; }
        public int? ReffID { get; set; }
    }
}
