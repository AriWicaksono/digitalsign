﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class DocumentSign
    {
        [Key]
        public int ID { get; set; }
        public int DocumentID { get; set; }
        public int UserID { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        [Column(TypeName = "decimal(14, 10)")]
        public decimal CoorX { get; set; }
        [Column(TypeName = "decimal(14, 10)")]
        public decimal CoorY { get; set; }
        public int PageNumber { get; set; }
        [Unicode(false)]
        public string? URL { get; set; }
        public bool? IsRequested { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(200)]
        [Unicode(false)]
        public string? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [StringLength(200)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        public bool? IsSigned { get; set; }

        [ForeignKey("DocumentID")]
        [InverseProperty("DocumentSign")]
        public virtual Document Document { get; set; } = null!;
        [ForeignKey("UserID")]
        [InverseProperty("DocumentSign")]
        public virtual Users User { get; set; } = null!;
    }
}
