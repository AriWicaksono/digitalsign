﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    [Index("UserID", Name = "IX_RoleUser", IsUnique = true)]
    public partial class RoleUser
    {
        [Key]
        public int RoleID { get; set; }
        [Key]
        public int UserID { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string CreatedBy { get; set; } = null!;
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DelDate { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string? DelBy { get; set; }
    }
}
