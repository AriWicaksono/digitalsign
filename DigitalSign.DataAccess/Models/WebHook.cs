﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class WebHook
    {
        [Key]
        public int ID { get; set; }
        [StringLength(200)]
        [Unicode(false)]
        public string request_id { get; set; } = null!;
        public bool success { get; set; }
        [StringLength(200)]
        [Unicode(false)]
        public string signer { get; set; } = null!;
        [StringLength(200)]
        [Unicode(false)]
        public string? signer_next { get; set; }
        [Unicode(false)]
        public string? url { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string CreatedBy { get; set; } = null!;
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DelDate { get; set; }
    }
}
