﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class UsersIdentity
    {
        [Key]
        public int ID { get; set; }
        public int UserID { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string FirstName { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string? MiddleName { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? LastName { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string NIK { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string BirthPlace { get; set; } = null!;
        [Column(TypeName = "date")]
        public DateTime BirthDate { get; set; }
        [StringLength(500)]
        [Unicode(false)]
        public string? Address { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string KTPName { get; set; } = null!;
        [Unicode(false)]
        public string KTPFile { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string PhotoName { get; set; } = null!;
        [Unicode(false)]
        public string PhotoFile { get; set; } = null!;
        [Unicode(false)]
        public string? Signature { get; set; }
        [Unicode(false)]
        public string? Certificate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DelDate { get; set; }

        [ForeignKey("UserID")]
        [InverseProperty("UsersIdentity")]
        public virtual Users User { get; set; } = null!;
    }
}
