﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class DocumentParticipant
    {
        [Key]
        public int ID { get; set; }
        public int DocumentID { get; set; }
        public int UserID { get; set; }
        public int Sequence { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string Status { get; set; } = null!;
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }

        [ForeignKey("DocumentID")]
        [InverseProperty("DocumentParticipant")]
        public virtual Document Document { get; set; } = null!;
        [ForeignKey("UserID")]
        [InverseProperty("DocumentParticipant")]
        public virtual Users User { get; set; } = null!;
    }
}
