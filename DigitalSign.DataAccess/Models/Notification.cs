﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class Notification
    {
        [Key]
        public int ID { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string Type { get; set; } = null!;
        [StringLength(100)]
        [Unicode(false)]
        public string Title { get; set; } = null!;
        [StringLength(500)]
        [Unicode(false)]
        public string? Detail { get; set; }
        public int? UserID { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string Icon { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string RedirectTo { get; set; } = null!;
        [Column(TypeName = "datetime")]
        public DateTime? ReadDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string CreatedBy { get; set; } = null!;
        [Column(TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? DeletedBy { get; set; }
    }
}
