﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class NotificationRead
    {
        [Key]
        public int ID { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ReadDate { get; set; }
        public int NotificationID { get; set; }
        public int UserId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DeletedDate { get; set; }
        [StringLength(20)]
        [Unicode(false)]
        public string? DeleteBy { get; set; }

        [ForeignKey("NotificationID")]
        [InverseProperty("NotificationRead")]
        public virtual Notification Notification { get; set; } = null!;
        [ForeignKey("UserId")]
        [InverseProperty("NotificationRead")]
        public virtual Users User { get; set; } = null!;
    }
}
