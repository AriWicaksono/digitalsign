﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class DocumentDiscussion
    {
        [Key]
        public int ID { get; set; }
        public int DocumentID { get; set; }
        public int UserID { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime TImeStamp { get; set; }
        [StringLength(500)]
        [Unicode(false)]
        public string Message { get; set; } = null!;

        [ForeignKey("DocumentID")]
        [InverseProperty("DocumentDiscussion")]
        public virtual Document Document { get; set; } = null!;
        [ForeignKey("UserID")]
        [InverseProperty("DocumentDiscussion")]
        public virtual Users User { get; set; } = null!;
    }
}
