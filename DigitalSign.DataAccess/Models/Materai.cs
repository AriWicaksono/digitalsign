﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class Materai
    {
        [Key]
        public int ID { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string UserID { get; set; } = null!;
        public int Quota { get; set; }
        [Column(TypeName = "decimal(18, 3)")]
        public decimal TotalPrice { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime TrxDate { get; set; }
        [Column(TypeName = "decimal(18, 3)")]
        public decimal TotalPaid { get; set; }
        public int QuotaUsed { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string CreatedBy { get; set; } = null!;
        [StringLength(100)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
    }
}
