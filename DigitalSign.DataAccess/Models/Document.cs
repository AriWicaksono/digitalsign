﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class Document
    {
        public Document()
        {
            DocumentDiscussion = new HashSet<DocumentDiscussion>();
            DocumentHistory = new HashSet<DocumentHistory>();
            DocumentParticipant = new HashSet<DocumentParticipant>();
            DocumentSign = new HashSet<DocumentSign>();
        }

        [Key]
        public int ID { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string Title { get; set; } = null!;
        [StringLength(100)]
        [Unicode(false)]
        public string DocumentName { get; set; } = null!;
        [Unicode(false)]
        public string DocumentFile { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string? Status { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DelDate { get; set; }
        [StringLength(40)]
        [Unicode(false)]
        public string? requestID { get; set; }
        public int? CategoryID { get; set; }
        public int? Owner { get; set; }

        [InverseProperty("Document")]
        public virtual ICollection<DocumentDiscussion> DocumentDiscussion { get; set; }
        [InverseProperty("Document")]
        public virtual ICollection<DocumentHistory> DocumentHistory { get; set; }
        [InverseProperty("Document")]
        public virtual ICollection<DocumentParticipant> DocumentParticipant { get; set; }
        [InverseProperty("Document")]
        public virtual ICollection<DocumentSign> DocumentSign { get; set; }
    }
}
