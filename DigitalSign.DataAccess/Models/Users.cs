﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class Users
    {
        public Users()
        {
            ActivityLog = new HashSet<ActivityLog>();
            DocumentDiscussion = new HashSet<DocumentDiscussion>();
            DocumentHistory = new HashSet<DocumentHistory>();
            DocumentParticipant = new HashSet<DocumentParticipant>();
            DocumentSign = new HashSet<DocumentSign>();
            UsersIdentity = new HashSet<UsersIdentity>();
        }

        [StringLength(40)]
        [Unicode(false)]
        public string? Registration_Id { get; set; }
        [Key]
        public int ID { get; set; }
        [StringLength(30)]
        [Unicode(false)]
        public string Username { get; set; } = null!;
        [StringLength(60)]
        [Unicode(false)]
        public string Password { get; set; } = null!;
        [StringLength(30)]
        [Unicode(false)]
        public string? TilakaName { get; set; }
        [Column(TypeName = "date")]
        public DateTime? DateExpire { get; set; }
        public int RoleID { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string Email { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string PhoneNumber { get; set; } = null!;
        [StringLength(50)]
        [Unicode(false)]
        public string? CompanyName { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Code { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Status { get; set; }
        public bool IsActive { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DelDate { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string? ResetToken { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ResetTokenExp { get; set; }

        [ForeignKey("RoleID")]
        [InverseProperty("Users")]
        public virtual Role Role { get; set; } = null!;
        [InverseProperty("User")]
        public virtual ICollection<ActivityLog> ActivityLog { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<DocumentDiscussion> DocumentDiscussion { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<DocumentHistory> DocumentHistory { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<DocumentParticipant> DocumentParticipant { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<DocumentSign> DocumentSign { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<UsersIdentity> UsersIdentity { get; set; }
    }
}
