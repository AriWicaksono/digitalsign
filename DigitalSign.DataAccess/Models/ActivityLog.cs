﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class ActivityLog
    {
        [Key]
        public int ID { get; set; }
        public int UserID { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string Activity { get; set; } = null!;
        [Unicode(false)]
        public string? Description { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime TimeStamp { get; set; }

        [ForeignKey("UserID")]
        [InverseProperty("ActivityLog")]
        public virtual Users User { get; set; } = null!;
    }
}
