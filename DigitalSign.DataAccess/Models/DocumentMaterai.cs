﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class DocumentMaterai
    {
        [Key]
        public int ID { get; set; }
        public int MateraiID { get; set; }
        [StringLength(255)]
        [Unicode(false)]
        public string DocumentName { get; set; } = null!;
        [StringLength(255)]
        [Unicode(false)]
        public string FileName { get; set; } = null!;
        [StringLength(255)]
        [Unicode(false)]
        public string Location { get; set; } = null!;
        [Column(TypeName = "decimal(14, 10)")]
        public decimal LowerLeftX { get; set; }
        [Column(TypeName = "decimal(14, 10)")]
        public decimal LowerLeftY { get; set; }
        [Column(TypeName = "decimal(14, 10)")]
        public decimal UpperRightX { get; set; }
        [Column(TypeName = "decimal(14, 10)")]
        public decimal UpperRightY { get; set; }
        public int PageNumber { get; set; }
        [StringLength(255)]
        [Unicode(false)]
        public string? Reason { get; set; }
        [StringLength(150)]
        [Unicode(false)]
        public string DocumentType { get; set; } = null!;
        [Unicode(false)]
        public string? StampedDoc { get; set; }
        [StringLength(100)]
        [Unicode(false)]
        public string? SerialNumber { get; set; }
        [Unicode(false)]
        public string? Materai { get; set; }
        public int? RestampCount { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StampDate { get; set; }
        [StringLength(255)]
        [Unicode(false)]
        public string CreatedBy { get; set; } = null!;
        [Column(TypeName = "datetime")]
        public DateTime CreatedDate { get; set; }
        [StringLength(255)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [StringLength(10)]
        [Unicode(false)]
        public string DocumentTypeCode { get; set; } = null!;
        [Unicode(false)]
        public string? FilePath { get; set; }
        public int? Owner { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? DocumentTypeID { get; set; }
    }
}
