﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace DigitalSign.DataAccess.Models
{
    public partial class RoleMenu
    {
        [Key]
        public int ID { get; set; }
        public int RoleID { get; set; }
        public int MenuID { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? CreatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? UpdatedBy { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? UpdatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DelDate { get; set; }

        [ForeignKey("MenuID")]
        [InverseProperty("RoleMenu")]
        public virtual Menu Menu { get; set; } = null!;
        [ForeignKey("RoleID")]
        [InverseProperty("RoleMenu")]
        public virtual Role Role { get; set; } = null!;
    }
}
