﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DigitalSign.DataAccess.Models;

namespace DigitalSign.DataAccess.Context
{
    public partial class DigitalSignContext : DbContext
    {
        public DigitalSignContext()
        {
        }

        public DigitalSignContext(DbContextOptions<DigitalSignContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ActivityLog> ActivityLog { get; set; } = null!;
        public virtual DbSet<Document> Document { get; set; } = null!;
        public virtual DbSet<DocumentCategory> DocumentCategory { get; set; } = null!;
        public virtual DbSet<DocumentDiscussion> DocumentDiscussion { get; set; } = null!;
        public virtual DbSet<DocumentHistory> DocumentHistory { get; set; } = null!;
        public virtual DbSet<DocumentMaterai> DocumentMaterai { get; set; } = null!;
        public virtual DbSet<DocumentParticipant> DocumentParticipant { get; set; } = null!;
        public virtual DbSet<DocumentSign> DocumentSign { get; set; } = null!;
        public virtual DbSet<GlobalConfiguration> GlobalConfiguration { get; set; } = null!;
        public virtual DbSet<Materai> Materai { get; set; } = null!;
        public virtual DbSet<Menu> Menu { get; set; } = null!;
        public virtual DbSet<Notification> Notification { get; set; } = null!;
        public virtual DbSet<Role> Role { get; set; } = null!;
        public virtual DbSet<RoleMenu> RoleMenu { get; set; } = null!;
        public virtual DbSet<RoleUser> RoleUser { get; set; } = null!;
        public virtual DbSet<TilakaApiLog> TilakaApiLog { get; set; } = null!;
        public virtual DbSet<Users> Users { get; set; } = null!;
        public virtual DbSet<UsersIdentity> UsersIdentity { get; set; } = null!;
        public virtual DbSet<WebHook> WebHook { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=34.101.228.175;Initial Catalog=DigitalSign;User Id=sa;Password=Indocyber.100;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ActivityLog>(entity =>
            {
                entity.HasOne(d => d.User)
                    .WithMany(p => p.ActivityLog)
                    .HasForeignKey(d => d.UserID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ActivityLog_Users");
            });

            modelBuilder.Entity<DocumentDiscussion>(entity =>
            {
                entity.HasOne(d => d.Document)
                    .WithMany(p => p.DocumentDiscussion)
                    .HasForeignKey(d => d.DocumentID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocumentDiscussion_Document");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DocumentDiscussion)
                    .HasForeignKey(d => d.UserID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocumentDiscussion_Users");
            });

            modelBuilder.Entity<DocumentHistory>(entity =>
            {
                entity.HasOne(d => d.Document)
                    .WithMany(p => p.DocumentHistory)
                    .HasForeignKey(d => d.DocumentID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocumentHistory_Document");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DocumentHistory)
                    .HasForeignKey(d => d.UserID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocumentHistory_User");
            });

            modelBuilder.Entity<DocumentParticipant>(entity =>
            {
                entity.HasOne(d => d.Document)
                    .WithMany(p => p.DocumentParticipant)
                    .HasForeignKey(d => d.DocumentID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocumentParticipant_Document");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DocumentParticipant)
                    .HasForeignKey(d => d.UserID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocumentParticipant_Users");
            });

            modelBuilder.Entity<DocumentSign>(entity =>
            {
                entity.HasOne(d => d.Document)
                    .WithMany(p => p.DocumentSign)
                    .HasForeignKey(d => d.DocumentID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocumentSign_Document");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DocumentSign)
                    .HasForeignKey(d => d.UserID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DocumentSign_Users");
            });

            modelBuilder.Entity<RoleMenu>(entity =>
            {
                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.RoleMenu)
                    .HasForeignKey(d => d.MenuID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoleMenu_Menu");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleMenu)
                    .HasForeignKey(d => d.RoleID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RoleMenu_Role");
            });

            modelBuilder.Entity<RoleUser>(entity =>
            {
                entity.HasKey(e => new { e.RoleID, e.UserID });
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.RoleID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Users_Role");
            });

            modelBuilder.Entity<UsersIdentity>(entity =>
            {
                entity.HasOne(d => d.User)
                    .WithMany(p => p.UsersIdentity)
                    .HasForeignKey(d => d.UserID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UsersIdentity_Users");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
