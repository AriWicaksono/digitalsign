﻿$(document).ready(function () {
    $(".uploadStart").css("display", "none");
});

$("#DocumentFile").on("change", function (e) {
	var url = baseurl + 'Materai/GetBase64FromFile';
	var file = new FormData();
	var fileUpload = this.files[0];
	$("#FileName").val(fileUpload.name);
    $(".uploadStart").css("display", "flex");
	file.append('file', fileUpload);
	$.ajax({
		url: url,
		method: "POST",
		data: file,
		async: false,
		processData: false,
		contentType: false,
		success: function (result) {
			if (result.isSucceed) {
				const startPdf = () => {
					PDFStart(result.Data);
				}
				startPdf();
			} else {
				alert(result.Message);
			}
		},
		error: function (err) {
			alert("Something wrong!");
		}
	});
    //PDFStart();
});


$("#submitMaterai").click(function (event) {
    event.preventDefault();
    $("#submitMaterai").disableBtn();
    swal({
        title: "Are you sure want to submit materai?",
        text: "",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "CONFIRM",
        cancelButtonText: "CANCEL",
        closeOnConfirm: false,
        closeOnCancel: false
    },
        function (isConfirm) {
            if (isConfirm) {
                swal.close();
                showProgress();
                var dataSubmit = new FormData(document.getElementById("frmMaterai"));
                //for (var i = 0; i < dataSubmit.length; i++) {
                //    if (dataSubmit[i].name === "lower_left_x") {
                //        dataSubmit[i].name = "VisLLX";
                //    }
                //    if (dataSubmit[i].name === "lower_left_y") {
                //        dataSubmit[i].name = "VisLLY";
                //    }
                //    if (dataSubmit[i].name === "upper_right_x") {
                //        dataSubmit[i].name = "VisURX";
                //    }
                //    if (dataSubmit[i].name === "upper_right_y") {
                //        dataSubmit[i].name = "VisURY";
                //    }
                //}
                dataSubmit.append("DocumentTypeCode", $("#DocumentType").data("kendoDropDownList").text())
                dataSubmit.append("Page", document.querySelector('#npages').innerHTML);
                dataSubmit.append("DocumentFile", $('#DocumentFile').get(0).files[0]);
                $.ajax({
                    url: baseurl + "Materai/SubmitStamping",
                    type: "POST",
                    data: dataSubmit,
                    dataType: "json",
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        hideProgress();
                        console.log(result);
                        if (result.isSucceed) {
                            if (result.Data == null) {
                                swal("Success!", result.Message, "success");
                                $('.confirm').click(() => { location.href = baseurl + "Materai/Index"; });
                                setTimeout(function () {
                                    location.href = baseurl + "Materai/Index";
                                }, 4000);
                            } else {
                                swal("Failed!", result.Message, "error");
                                $('.confirm').click(() => { location.href = baseurl + "Materai/Index"; });
                                setTimeout(function () {
                                    location.href = baseurl + "Materai/Index";
                                }, 4000);
                            }
                        }
                        else {
                            if (result.Data == null) {
                                swal("Failed!", result.Message, "error");
                                $('.confirm').click(() => { location.href = baseurl + "Materai/Index"; });
                                setTimeout(function () {
                                    location.href = baseurl + "Materai/Index";
                                }, 4000);
                            } else {
                                swal("Failed!", result.Message, "error");
                                $("#submitMaterai").enableBtn();
                                hideProgress();
                                if (typeof result.Data == 'string') {
                                    showValidateMessage(result.Data);
                                }
                            }
                        }
                    },
                    error: function (err) {
                        hideProgress();
                        sweetAlert(err.Message, "Something went wrong!", "error");
                    }
                });
            }
            else {
                swal("Cancelled", "", "error");
            }
        });
});

function showValidateMessage(validations) {
    for (var validation of validations) {

        let propertyName = validation.PropertyName;
        let errorMessage = validation.ErrorMessage;
        let parent = $(`#${propertyName}`).closest("div");
        parent.append(`<span data-valmsg-for='${propertyName}' style='color: red'>${errorMessage}</span>`)

        //$(`span[data-valmsg-for=${propertyName}]`).text(errorMessage).css("color", "red").css("display", "block");
        //console.log(errorMessage + " " + propertyName);
    }
}

$("#restampMaterai").click(function (event) {
    event.preventDefault();
    $("#restampMaterai").disableBtn();
    swal({
        title: "Are you sure want to restamp materai?",
        text: "",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "CONFIRM",
        cancelButtonText: "CANCEL",
        closeOnConfirm: false,
        closeOnCancel: false
    },
        function (isConfirm) {
            if (isConfirm) {
                swal.close();
                showProgress();
                var idDoc = $("#ID").val();
                $.ajax({
                    url: baseurl + "Materai/RestampMaterai?docId=" + idDoc,
                    type: "POST",
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        hideProgress();
                        console.log(result);
                        if (result.isSucceed) {
                            swal("Success!", result.Message, "success");
                            $('.confirm').click(() => { location.href = baseurl + "Materai/Index"; });
                            setTimeout(function () {
                                location.href = baseurl + "Materai/Index";
                            }, 4000);
                        }
                        else {
                            swal("Failed!", result.Message, "error");
                            $('.confirm').click(() => { location.reload() });
                            setTimeout(function () {
                                location.reload();
                            }, 10000);
                        }
                    },
                    error: function (err) {
                        hideProgress();
                        sweetAlert(err.Message, "Something went wrong!", "error");
                    }
                });
            }
            else {
                swal("Cancelled", "", "error");
            }
        });
});


$("#resubmitMaterai").click(function (event) {
    event.preventDefault();
    $("#resubmitMaterai").disableBtn();
    swal({
        title: "Are you sure want to resubmit materai?",
        text: "",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "CONFIRM",
        cancelButtonText: "CANCEL",
        closeOnConfirm: false,
        closeOnCancel: false
    },
        function (isConfirm) {
            if (isConfirm) {
                swal.close();
                showProgress();
                //var idDoc = $("#ID").val();
                var dataSubmit = new FormData(document.getElementById("frmMaterai"));
                dataSubmit.set("Page", document.querySelector('#npages').innerHTML);
                $.ajax({
                    url: baseurl + "Materai/ResubmitMaterai",
                    type: "POST",
                    data: dataSubmit,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        hideProgress();
                        console.log(result);
                        if (result.isSucceed) {
                            swal("Success!", result.Message, "success");
                            $('.confirm').click(() => { location.href = baseurl + "Materai/Index"; });
                            setTimeout(function () {
                                location.href = baseurl + "Materai/Index";
                            }, 4000);
                        }
                        else {
                            swal("Failed!", result.Message, "error");
                            $('.confirm').click(() => { location.reload() });
                            setTimeout(function () {
                                location.reload();
                            }, 10000);
                        }
                    },
                    error: function (err) {
                        hideProgress();
                        sweetAlert(err.Message, "Something went wrong!", "error");
                    }
                });
            }
            else {
                swal("Cancelled", "", "error");
            }
        });
});