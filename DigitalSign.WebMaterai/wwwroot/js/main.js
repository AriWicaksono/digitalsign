﻿jQuery.fn.extend({
    disableBtn: function () {
        return this.each(function () {
            var $this = $(this).html();
            $(this).html("<i class='fa fa-circle-o-notch fa-spin'></i> Processing...");
            $(this).prop("origin-text", $this);
            $(this).attr("disabled", true);
            $(this).addClass("k-state-disabled");
        });
    },

    enableBtn: function () {
        return this.each(function () {
            if ($(this).prop("origin-text") != null)
                $(this).html($(this).prop("origin-text"));
            $(this).attr("disabled", false);
            $(this).removeClass("k-state-disabled");
        });
    }
});