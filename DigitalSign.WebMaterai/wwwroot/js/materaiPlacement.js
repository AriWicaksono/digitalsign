﻿// A $( document ).ready() block.
$(document).ready(function () {
    console.log("ready!");
});
var instance = null;
const annotationObject = {
    user: "Signature",
    page: 1,
    x: 0,
    y: 0,
    w: 50,
    h: 50
};

var canvasWidth = 0;
var canvasHeight = 0;
var isDrop = false;
var canvas;
let numPage = 1;

var elDrag = null;
//document.getElementById("dragMe").addEventListener("dragstart", (event) => {
//    // console.log(event);
//    if (!isDrop) {
//        elDrag = document.getElementById('objDrag');
//        elDrag.style.width = annotationObject.w + 'px';
//        elDrag.style.height = annotationObject.h + 'px';
//    }
//});

let targetDropZone = document.getElementById(`container`);

targetDropZone.addEventListener("dragover", (event) => {
    // prevent default to allow drop
    event.preventDefault();
});

//targetDropZone.addEventListener("drop", (event) => {
//    // prevent default action (open as link for some elements)
//    event.preventDefault();
//    if (!isDrop) {
//        if (event.target.nodeName == "CANVAS") {
//            event.target.parentElement.appendChild(elDrag);
//            $("#deleteDragMe").show();
//            $("#submitMaterai").show();
//            $("#dragMe").removeClass("btn-info");
//            $("#dragMe").addClass("btn-danger");
//            $("#dragMe").prop("draggable", false);
//            $("#text-action").text("(Delete Me)");
//            isDrop = true;

//            let minimum_size = 50;
//            let docWidth = document.getElementById('document_width').value;
//            let docHeight = document.getElementById('document_height').value;
//            let offSetWidth = event.offsetX + annotationObject.w;
//            let offSetHeight = event.offsetY + annotationObject.h;

//            if (offSetWidth > docWidth) {
//                let newWidth = docWidth - event.offsetX;
//                if (newWidth < minimum_size) {
//                    annotationObject.x = docWidth - minimum_size;
//                    annotationObject.w = minimum_size;
//                } else {
//                    annotationObject.x = event.offsetX;
//                    annotationObject.w = newWidth;
//                }
//            } else {
//                annotationObject.x = event.offsetX;
//            }

//            if (offSetHeight > docHeight) {
//                let newHeight = docHeight - event.offsetY;
//                if (newHeight < minimum_size) {
//                    annotationObject.y = docHeight - minimum_size;
//                    annotationObject.h = minimum_size;
//                } else {
//                    annotationObject.y = event.offsetY;
//                    annotationObject.h = newHeight;
//                }
//            } else {
//                annotationObject.y = event.offsetY;
//            }

//            elDrag.style.width = annotationObject.w + 'px';
//            elDrag.style.height = annotationObject.h + 'px';
//            elDrag.style.left = annotationObject.x + 'px';
//            elDrag.style.top = annotationObject.y + 'px';
//            elDrag.style.display = 'flex';

//            getPositions();
//            //console.log(`Left : ${annotationObject.x}, Top : ${annotationObject.y}, Width : ${annotationObject.w}, Height : ${annotationObject.h}, Page : ${annotationObject.page}`);
//        } else {
//            return
//        }
//    }
//});


const PDFStart = nameRoute => {
    let loadingTask = pdfjsLib.getDocument(nameRoute),
        pdfDoc = null,
        scale = 1;
    canvas = document.querySelector('#cnv');
    let ctx = canvas.getContext('2d');
    const GeneratePDF = numPage => {

        pdfDoc.getPage(numPage).then(page => {

            let viewport = page.getViewport({ scale: scale });
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            let renderContext = {
                canvasContext: ctx,
                viewport: viewport
            }

            document.getElementById('document_width').value = viewport.width;
            document.getElementById('document_height').value = viewport.height;

            page.render(renderContext);
            canvasWidth = viewport.width;
            canvasHeight = viewport.height;
            getPositions();
        });
        document.querySelector('#npages').innerHTML = numPage;
        annotationObject.page = numPage;
    }

    const PrevPage = () => {
        if (numPage === 1) {
            return
        }
        numPage--;
        GeneratePDF(numPage);
    }

    const NextPage = () => {
        if (numPage >= pdfDoc.numPages) {
            return
        }
        numPage++;
        GeneratePDF(numPage);
    }

    document.querySelector('#prev').addEventListener('click', PrevPage)
    document.querySelector('#next').addEventListener('click', NextPage)

    loadingTask.promise.then(pdfDoc_ => {
        pdfDoc = pdfDoc_;
        document.querySelector('#npages').innerHTML = pdfDoc.numPages;
        document.querySelector('#allpages').innerHTML = pdfDoc.numPages;
        GeneratePDF(numPage)
    });
}

function getPositions() {
    var imgsign = new Image();
    imgsign.src = $('#digital_signature_path').val();

    imgsign.onload = function () {
        var signheight = imgsign.height;
        var signwidth = imgsign.width;
        $(canvas).imgAreaSelect({
            // aspectRatio: signwidth + ':' + signheight,
            handles: true,
            show: true,
            onSelectEnd: function (img, selection) {
                var height = parseInt($('input[name="document_height"]').val());
                var width = parseInt($('input[name="document_width"]').val());
                var scale = width / (canvasWidth - 1);

                var VisLLX = selection.x1 * scale,
                    VisLLY = height - (selection.y2 * scale),
                    VisURX = selection.x2 * scale,
                    VisURY = height - (selection.y1 * scale);

                var diff_x = Math.abs(VisLLX - VisURX),
                    diff_y = Math.abs(VisLLY - VisURY)

                if (diff_x < 1 && diff_y < 1) {
                    $(canvas).imgAreaSelect({
                        x1: $('input[name="x1"]').val(),
                        y1: $('input[name="y1"]').val(),
                        x2: $('input[name="x2"]').val(),
                        y2: $('input[name="y2"]').val()
                    });
                } else {
                    $('input[name="x1"]').val(parseInt(selection.x1));
                    $('input[name="x2"]').val(parseInt(selection.x2));
                    $('input[name="y1"]').val(parseInt(selection.y1));
                    $('input[name="y2"]').val(parseInt(selection.y2));
                    $('input[name="VisLLX"]').val(parseInt(VisLLX));
                    $('input[name="VisLLY"]').val(parseInt(VisLLY));
                    $('input[name="VisURX"]').val(parseInt(VisURX));
                    $('input[name="VisURY"]').val(parseInt(VisURY));
                }
            },
            zIndex: -2,
            borderWidth: 4
        });

        var is_visible_sign = $('#is_visible_sign').val();
        if (is_visible_sign == 'True') {
            var wdth = 50;
            var hgth = (signheight * wdth) / signwidth;

            var height = parseInt($('input[name="document_height"]').val());
            var width = parseInt($('input[name="document_width"]').val());
            var scale = width / (canvasWidth - 1);

            var x1 = 315;
            var x2 = 339 + wdth;
            var y1 = canvasHeight - hgth - 537;
            var y2 = canvasHeight - 521;
            var VisLLX = x1 * scale,
                VisLLY = height - (y2 * scale),
                VisURX = x2 * scale,
                VisURY = height - (y1 * scale);

            if ($('#is_detail').val() == 'True') {
                VisLLX = $('input[name=VisLLX]').val();
                VisLLY = $('input[name=VisLLY]').val();
                VisURX = $('input[name=VisURX]').val();
                VisURY = $('input[name=VisURY]').val();

                x1 = VisLLX / scale;
                x2 = VisURX / scale;
                y1 = (height - VisURY) / scale;
                y2 = (height - VisLLY) / scale;
            }
            $('input[name="x1"]').val(parseInt(x1));
            $('input[name="x2"]').val(parseInt(x2));
            $('input[name="y1"]').val(parseInt(y1));
            $('input[name="y2"]').val(parseInt(y2));
            $('input[name="VisLLX"]').val(parseInt(VisLLX));
            $('input[name="VisLLY"]').val(parseInt(VisLLY));
            $('input[name="VisURX"]').val(parseInt(VisURX));
            $('input[name="VisURY"]').val(parseInt(VisURY));

            $(canvas).imgAreaSelect({
                x1: x1,
                y1: y1,
                x2: x2,
                y2: y2
            });
            //}

        }
        var url = imgsign.src;

        $('.imgareaselect-selection').css({ 'background': 'url(' + url + ') center/100% 100% no-repeat' })
    }

    imgsign.src = $('#digital_signature_path').val();
}

$("#deleteDragMe").click(function (event) {
    event.stopPropagation();
    event.preventDefault();
    //instance.delete(annotationObject.user);
    elDrag.style.display = 'none';
    isDrop = false;
    $("#deleteDragMe").hide();
    $("#submitMaterai").hide();
    $("#dragMe").removeClass("btn-danger");
    $("#dragMe").addClass("btn-info");
    $("#dragMe").prop("draggable", true);
    $("#text-action").text("(Drag Me)");
    annotationObject.w = 100;
    annotationObject.h = 100;
});