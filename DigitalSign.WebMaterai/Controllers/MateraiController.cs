﻿using DigitalSign.DataAccess.Models;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Security;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace DigitalSign.WebMaterai.Controllers
{
    public class MateraiController : Controller
    {
        public IActionResult Index()
        {
            if (ClaimHelper.CheckSession && ClaimHelper.IsAdmin)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Title = "Materai";
                return View();
            }
            else
            {
                return RedirectToAction("NotAllowed", "Home");
            }
        }

        public JsonResult Submit(MateraiViewModel viewModel)
        {
            var result = new ProcessResult();
            try
            {
                viewModel.UserID = ClaimHelper.UserName;
                viewModel.TrxDate = DateTime.Now;
                var endpoint = $"{ApiUrl.Master}Materai" + Utilities.Route.Save;
                var json = JsonConvert.SerializeObject(viewModel);

                var response = RestAPIHelper<ProcessResult>.Submit(json, Method.POST, endpoint, Request);
                if (response != null)
                {
                    result = response;
                }
            }
            catch (Exception ex)
            {
                result.ProcessFailed(ex.Message);
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult ListDocument([DataSourceRequest] DataSourceRequest request)
        {
            AjaxViewModel ajaxViewModel = new AjaxViewModel();
            DataSourceResult newDataSourceResult = new DataSourceResult();
            try
            {
                //var json = GridUtilities.ConvertKendoRequestToJson(request);
                var endpoint = $"{ApiUrl.Master}Materai/GetList?currentUser={ClaimHelper.UserName}";
                var result = RestAPIHelper<List<DocumentMateraiNewViewModel>>
                    .Submit("", Method.GET, endpoint, Request);
                foreach (var item in result)
                {
                    item.IsSuperAdmin = ClaimHelper.RoleName == DataConfiguration.Configuration.RoleSuperAdmin;
                }
                newDataSourceResult = result.ToDataSourceResult(request);
            }
            catch (Exception exception)
            {
                if (exception.Message == "Session Timed Out")
                {
                    return Json(new
                    {
                        redirectUrl = Url.Action("Login", "Home"),
                        isRedirect = true
                    });
                }
                ajaxViewModel.SetValues(false, null, string.Format("Failed\\nMessage: {0}", exception.GetBaseException().Message));
            }
            return Json(newDataSourceResult);
        }

        [HttpPost]
        public ActionResult ListLog([DataSourceRequest] DataSourceRequest request, string docId)
        {
            AjaxViewModel ajaxViewModel = new AjaxViewModel();
            DataSourceResult newDataSourceResult = new DataSourceResult();
            try
            {
                //var json = GridUtilities.ConvertKendoRequestToJson(request);
                var endpoint = $"{ApiUrl.Master}Materai/ListLog?docId={docId}";
                var result = RestAPIHelper<List<LogTilakaViewModel>>
                    .Submit("", Method.GET, endpoint, Request);
                newDataSourceResult = result.ToDataSourceResult(request);
            }
            catch (Exception exception)
            {
                if (exception.Message == "Session Timed Out")
                {
                    return Json(new
                    {
                        redirectUrl = Url.Action("Login", "Home"),
                        isRedirect = true
                    });
                }
                ajaxViewModel.SetValues(false, null, string.Format("Failed\\nMessage: {0}", exception.GetBaseException().Message));
            }
            return Json(newDataSourceResult);
        }

        public IActionResult GetListDocumentType()
        {
            try
            {
                var endpoint = $"{ApiUrl.Master}Materai/GetListDocumentType";
                var response = RestAPIHelper<List<DocumentType>>.Submit("", Method.GET, endpoint, Request);
                return Json(response);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IActionResult GetListQuota()
        {
            try
            {
                var endpoint = $"{ApiUrl.Master}Materai/GetListQuota?currentUser={ClaimHelper.UserName}";
                var response = RestAPIHelper<List<MateraiViewModel>>.Submit("", Method.GET, endpoint, Request);
                return Json(response);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IActionResult Upload()
        {
            if (ClaimHelper.CheckSession && ClaimHelper.IsAdmin)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Title = "Stamping Materai";
                var viewModel = new DocumentMateraiViewModel();
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("NotAllowed", "Home");
            }
        }

        [HttpPost]
        public IActionResult GetBase64FromFile(IFormFile file)
        {
            var ajaxVM = new AjaxViewModel();
            try
            {
                byte[] filesContents = FTPHelper.GetFileContentsByIFormFile(file);
                var base64String = "data:application/pdf;base64," + Convert.ToBase64String(filesContents);
                ajaxVM.SetValues(true, base64String, "success");
            }
            catch (Exception ex)
            {
                ajaxVM.SetValues(false, null, "failed");
            }
            return Json(ajaxVM);
        }

        [HttpPost]
        public JsonResult SubmitStamping(DocumentMateraiViewModel viewModel)
        {
            var result = new AjaxViewModel();
            if (string.IsNullOrEmpty(viewModel.DocumentType))
            {
                viewModel.DocumentType = null;
                viewModel.DocumentTypeCode = null;
            }
            if (ModelState.IsValid)
            {
                var endpoint = $"{ApiUrl.Master}Materai/SubmitStamping";

                if (viewModel.DocumentFile != null)
                {

                    byte[] filesContents = FTPHelper.GetFileContentsByIFormFile(viewModel.DocumentFile);

                    //}
                    var model = new DocumentMateraiNewViewModel()
                    {
                        ID = string.Empty,
                        ContentPDF = Convert.ToBase64String(filesContents),
                        FileName = viewModel.DocumentFile.FileName,
                        DocumentName = viewModel.DocumentName,
                        DocumentType = viewModel.DocumentTypeCode,
                        DocumentTypeCode = viewModel.DocumentType,
                        Location = viewModel.Location,
                        Page = viewModel.Page,
                        Reason = viewModel.Reason,
                        VisLLX = viewModel.VisLLX,
                        VisLLY = viewModel.VisLLY,
                        VisURX = viewModel.VisURX,
                        VisURY = viewModel.VisURY,
                        MateraiID = viewModel.MateraiID.HasValue ? viewModel.MateraiID.Value : 0,
                        UserLogin = Utilities.ClaimHelper.UserName
                    };
                    var json = JsonConvert.SerializeObject(model);

                    var response = RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
                    if (response != null)
                    {
                        if (response.isSucceed)
                        {
                            result.SetValues(true, null, response.Message);
                        }
                        else
                        {
                            result.SetValues(true, new { id = 1 }, response.Message);
                        }
                    }
                    else
                    {
                        result.SetValues(true, new { id = 1 }, "Something when wrong");
                    }
                }
                else
                {
                    result.SetValues(false, null, "File Not Found");
                }
                return Json(result);
            }
            var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
            result.SetValues(false, validationMessages, "Please fill required column");
            return Json(result);
        }

        public IActionResult ViewStamp(string id) 
        {
            if (ClaimHelper.CheckSession && ClaimHelper.IsAdmin)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Title = "Stampped Materai";
                var endpoint = $"{ApiUrl.Master}Materai/GetStamppedDoc?docId=" + id;
                var response = RestAPIHelper<DocumentMateraiNewViewModel>.Submit("", Method.GET, endpoint, Request);

                return View(response);
            }
            else
            {
                return RedirectToAction("NotAllowed", "Home");
            }
        }

        public IActionResult Detail(string id)
        {
            if (ClaimHelper.CheckSession && ClaimHelper.IsAdmin)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Title = "Stampped Materai";
                var endpoint = $"{ApiUrl.Master}Materai/GetDetailDoc?docId=" + id;
                var response = RestAPIHelper<DocumentMateraiDetailViewModel>.Submit("", Method.GET, endpoint, Request);

                return View(response);
            }
            else
            {
                return RedirectToAction("NotAllowed", "Home");
            }
        }

        public IActionResult RestampMaterai(string docId) 
        {
            var endpoint = $"{ApiUrl.Master}Materai/RestampMaterai?docId={docId}&currentUser={ClaimHelper.UserName}";
            var response = RestAPIHelper<AjaxViewModel>.Submit("", Method.POST, endpoint, Request);
            return Json(response);
        }

        public IActionResult ResubmitMaterai(DocumentMateraiDetailViewModel viewModel)
        {
            viewModel.UserLogin = ClaimHelper.UserName;
            var json = JsonConvert.SerializeObject(viewModel);
            var endpoint = $"{ApiUrl.Master}Materai/ResubmitMaterai?currentUser={ClaimHelper.UserName}";
            var response = RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
            return Json(response);
        }

        public IActionResult ViewLog(string docId)
        {
            if (ClaimHelper.CheckSession && ClaimHelper.IsAdmin)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Title = "Log Document";
                var endpoint = $"{ApiUrl.Master}Materai/GetDetailDoc?docId=" + docId;
                var response = RestAPIHelper<DocumentMateraiDetailViewModel>.Submit("", Method.GET, endpoint, Request);

                return PartialView("_LogDetail", response);
            }
            else
            {
                return RedirectToAction("NotAllowed", "Home");
            }
        }

    }
}
