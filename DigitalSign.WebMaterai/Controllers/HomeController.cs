﻿using DigitalSign.Utilities;
using DigitalSign.ViewModels.Security;
using DigitalSign.ViewModels;
using DigitalSign.WebMaterai.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using System.Diagnostics;
using System.Security.Policy;
using DigitalSign.ViewModels.Master.Document;

namespace DigitalSign.WebMaterai.Controllers
{
	public class HomeController : Controller
	{
		string url = $"{ApiUrl.Security}";
		private readonly ILogger<HomeController> _logger;

		public HomeController(ILogger<HomeController> logger)
		{
			_logger = logger;
		}

		public IActionResult Index()
		{
			if (ClaimHelper.CheckSession && ClaimHelper.IsAdmin)
			{
				ViewBag.Username = ClaimHelper.UserName;
				return View();
			}
			else
			{
                return RedirectToAction("NotAllowed");
            }
		}

		public IActionResult BuyQuota() {
			var model = new MateraiViewModel();
			return PartialView("_BuyQuota", model);
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		[HttpGet]
		public IActionResult Login()
		{
			if (ClaimHelper.CheckSession)
			{
				return RedirectToAction("Index", "Home");
			}
			else
			{
				return View();
			}
		}
		public IActionResult Login(LoginRequestViewModel model)
		{
			var endpoint = url + "LoginMaterai";
			var result = new AjaxViewModel();
			if (ModelState.IsValid)
			{
				try
				{
					string json = JsonConvert.SerializeObject(model);
					var content = RestAPIHelper<AjaxViewModel<LoginResponseViewModel>>.Submit(json, Method.POST, endpoint);

					if (content.isSucceed)
					{
						if (!String.IsNullOrEmpty(HttpContext.Request.Cookies["token"]))
						{
							HttpContext.Response.Cookies.Delete("token");
							HttpContext.Response.Cookies.Append("token", content.Data.Token);
						}
						else
						{
							HttpContext.Response.Cookies.Append("token", content.Data.Token);
						}
					}

					result.SetValues(content.isSucceed, content.Data, content.Message);
				}
				catch (Exception)
				{
					result.SetValues(false, null, "Something Error");
				}

			}
			else
			{
				var validationMessages = ValidationHelper.GetValidationMessage(ModelState);
				result.SetValues(false, validationMessages, "");
			}

			return Json(result);
		}

		[Route("Logout")]
		public IActionResult Logout()
		{
			HttpContext.Response.Cookies.Delete("token");
			return RedirectToAction("Login");
		}

		public IActionResult NotAllowed() 
		{
			ViewBag.UserName = ClaimHelper.UserName;
			return View();
		}

	}
}