﻿using DigitalSign.Provider.Master;
using DigitalSign.Provider.Security;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace DigitalSign.API.Security.Controllers
{
    [Route("Api/Security/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersProvider usersProvider;
        private readonly ISecurityProvider securityProvider;
        private readonly IMasterUserProvider mUserProvider;
        private readonly ILogger<UsersController> _logger;
        private readonly IGlobalSettingProvider _global;

        public UsersController(IUsersProvider usersProvider, ISecurityProvider securityProvider, IMasterUserProvider mUserProvider, ILogger<UsersController> logger, IGlobalSettingProvider global)
        {
            this.usersProvider = usersProvider;
            this.securityProvider = securityProvider;
            this.mUserProvider = mUserProvider;
            _logger = logger;
            _global = global;
        }

        [HttpPost]
        [Route("[action]")]
        [AllowAnonymous]
        public ActionResult Register(RegisterInsertViewModel registerModel)
        {
            var contentBody = new
            {
                request_id = registerModel.Registration_Id,
                nik = registerModel.NIK
            };
            var data = usersProvider.Register(registerModel);
            return Ok(data);
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult CheckAccount(string nik)
        {
            var data = usersProvider.CheckAccount(nik);
            return Ok(data);
        }

        [HttpPost]
        [Route("[action]")]
        public ActionResult RegisterTilaka(RegisterTilakaViewModel registerModel)
        {
            ResponseRegistrasi res = new ResponseRegistrasi();
            string[] dataSuccess = { "8aec06d8-78ba-4136-9ecb-497a98d63529", "dummy@mail.com" };
            string[] dataFailed = { "dummy666" };
            if (registerModel.name == "veryd")
            {
                res.Success = true;
                res.Message = "Data Diterima";
                res.Data = dataSuccess;
            }
            else
            {
                res.Success = false;
                res.Message = "Error Response from Call eKyc API";
                res.Data = dataFailed;
            }
            return Ok(res);
        }

        [HttpPost("[action]")]
        [AllowAnonymous]
        public async Task<ActionResult> Activation(ActivationEmailViewModel viewModel)
        {
            var jsonViewModel = new AjaxViewModel();
            try
            {
                //var endpoint = urlSecurity + $"VerifyEmail?nik={viewModel.NIK}&code=" + viewModel.Code;
                var content = securityProvider.ActivationAccount(viewModel.NIK, viewModel.Code);
                if (!content.isSucceed)
                {
                    throw new Exception(message: content.message);
                }

                var token = await Tilaka.GetTilakaToken();
                var uuid = await Tilaka.GenerateUUID(token);
                var start = DateTime.Now;

                RegisterTilakaViewModel register = JsonConvert.DeserializeObject<RegisterTilakaViewModel>(content.value);
                register.registration_id = string.IsNullOrEmpty(register.registration_id) ? uuid : register.registration_id;
                var json = JsonConvert.SerializeObject(register);
                _logger.LogInformation("Register Object Request: {@requestBody}", json);
                var model = Utilities.RestAPIHelper<ResponseRegistrasi>.Submit(json, Method.POST, Tilaka.registerForKycCheck, token);
                _logger.LogInformation("Register Object Response: {@responseBody}", model);
                if (model.Success)
                {
                    var jsonChangeRegId = new ChangeRegIdViewModel();
                    jsonChangeRegId.NIK = register.nik;
                    jsonChangeRegId.RegistrationId = register.registration_id;

                    //string urlSave = urlMasterUser + "/ChangeRegistrationId";
                    var changeUuid = mUserProvider.ChangeRegistrationID(jsonChangeRegId);
                    jsonViewModel.SetValues(true, new { id = register.registration_id }, "Tolong selesaikan liveness untuk mengaktifkan akun");
                }
                else
                {
                    jsonViewModel.SetValues(false, model.Data, model.Message);
                }

                var logApi = new LogTilakaViewModel()
                {
                    RequestStart = start,
                    ApiName = "RegisterForKycCheck",
                    RequestBody = json,
                    EndPoint = Tilaka.registerForKycCheck,
                    Method = "POST",
                    RequestBy = viewModel.NIK,
                    ResponseBody = JsonConvert.SerializeObject(model),
                    RequestEnd = DateTime.Now
                };
                _global.LogTilaka(logApi);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                jsonViewModel.SetValues(false, null, e.Message);
                //throw;
            }
            return Ok(jsonViewModel);
        }
    }
}
