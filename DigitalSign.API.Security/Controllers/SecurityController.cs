﻿using DigitalSign.Provider.Master;
using DigitalSign.Provider.Security;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace DigitalSign.API.Security.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        private readonly ISecurityProvider securityProvider;
        private readonly IDocumentSignProvider _provider;
        private readonly ILogger<SecurityController> _logger;

        public SecurityController(ISecurityProvider securityProvider, IDocumentSignProvider provider, ILogger<SecurityController> logger)
        {
            this.securityProvider = securityProvider;
            _provider = provider;
            _logger = logger;
        }

        [HttpGet("[action]")]
        public IActionResult GetMenu(int? parentMenuID)
        {
            IEnumerable<MenuViewModel> menus = null;
            menus = securityProvider.GetMenu(parentMenuID, "Admin");

            return Ok(menus);
        }

        [HttpPost]
        [Route("[action]")]
        [AllowAnonymous]
        public ActionResult Login([FromBody] LoginRequestViewModel model)
        {
            try
            {
                AjaxViewModel<object> result = new AjaxViewModel<object>();
                _logger.LogInformation("Object Request Body: {@requestBody}", model);
                var IsAuthenticated = securityProvider.IsAuthenticated(model);
                if (IsAuthenticated)
                {
                    LoginResponseViewModel obj = new LoginResponseViewModel();
                    obj.Data = securityProvider.GetUser(model);
                    obj.Token = securityProvider.CreateToken(obj.Data);
                    obj.IsSuccess = true;

                    result.isSucceed = true;
                    result.Message = "Success";
                    result.Data = obj;
                    _logger.LogInformation("Object Response Body: {@responseBody}", result);
                    return Ok(result);
                }
                else
                {
                    result.isSucceed = false;
                    result.Message = "Login Gagal! Username atau Password salah!";
                    _logger.LogInformation("Object: {@responseBody}", result);
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                {
                    _logger.LogError($"Error on method Login/Security{ex.Message}");
                }
                else
                {
                    _logger.LogError($"Error on method Login/Security{ex.InnerException.Message}");
                }
                throw;
            }
        }

        [HttpPost]
        [Route("[action]")]
        [AllowAnonymous]
        public ActionResult LoginMaterai([FromBody] LoginRequestViewModel model)
        {
            AjaxViewModel<object> result = new AjaxViewModel<object>();
            try
            {
                _logger.LogInformation("Object Request Body: {@requestBody}", model);
                var IsAuthenticated = securityProvider.IsAuthenticated(model);
                if (IsAuthenticated)
                {
                    LoginResponseViewModel obj = new LoginResponseViewModel();
                    obj.Data = securityProvider.GetUserRole(model);
                    if (obj.Data.RoleID == 0)
                    {
                        result.isSucceed = false;
                        result.Message = "Login Gagal! Role belum diatur!";
                        return Ok(result);
                    }
                    obj.Token = securityProvider.CreateToken(obj.Data);
                    obj.IsSuccess = true;

                    result.isSucceed = true;
                    result.Message = "Success";
                    result.Data = obj;
                    _logger.LogInformation("Object Response Body: {@responseBody}", result);
                    return Ok(result);
                }
                else
                {
                    result.isSucceed = false;
                    result.Message = "Login Gagal! Username atau Password salah!";
                    _logger.LogInformation("Object: {@responseBody}", result);
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                {
                    _logger.LogError($"Error on method Login/Security{ex.Message}");
                    result.Message = "Login Gagal!" + ex.Message;
                }
                else
                {
                    _logger.LogError($"Error on method Login/Security{ex.InnerException.Message}");
                    result.Message = "Login Gagal!" + ex.InnerException.Message;
                }
                result.isSucceed = false;
                _logger.LogInformation("Object: {@responseBody}", result);
                return BadRequest(result);
                //throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult SendEmail(string nik)
        {
            var data = securityProvider.VerifySendEmail(nik);
            return Ok(data);
        }


        [HttpGet]
        [Route("[action]")]
        public ActionResult VerifyEmail(string nik, string code)
        {
            var data = securityProvider.ActivationAccount(nik, code);
            return Ok(data);
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult WhoAmI()
        {
            return Ok(securityProvider.WhoAmI());
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> SubmitMaterai(DocumentMateraiViewModel model)
        {
            try
            {
                _logger.LogInformation("Object: {@requestBody}", model);
                var data = _provider.SubmitMaterai(model);//simpen materai
                if (data.isSucceed)//send ke tilaka
                {
                    _logger.LogInformation("Object: {@responseBody}", data);
                    var token = await Tilaka.GetTilakaToken();
                    var contentBody = new RequestStampMaterai()
                    {
                        content_pdf = model.ContentPDF.Replace("data:application/pdf;base64,", ""),
                        file_name = model.FileName,
                        location = model.Location,
                        nama_doc = model.DocumentType,
                        reason = model.Reason,
                        signature_page = model.Page.ToString(),
                        visLLX = model.VisLLX,
                        visLLY = model.VisLLY,
                        visURX = model.VisURX,
                        visURY = model.VisURY
                    };
                    _logger.LogInformation("Object: {@requestBody}", contentBody);
                    _logger.LogInformation("Object: {@tokenTilaka}", token);
                    var responeTilaka = RestAPIHelper<ResponseMaterai>.Submit(JsonConvert.SerializeObject(contentBody), RestSharp.Method.POST, Tilaka.stamp_doc, token);
                    var responseRestampTilaka = new ResponseMaterai();
                    _logger.LogInformation("Object: {@responseBody}", responeTilaka);
                    if (!responeTilaka.success)
                    {
                        do
                        {
                            var dataUpdateRestamp = _provider.SaveMaterai(responeTilaka.data, Convert.ToInt32(data.value), true, model.UserLogin);
                            if (dataUpdateRestamp.isSucceed)
                            {
                                var contentBodyRestamp = new RequestRestampMaterai()
                                {
                                    content_pdf = model.ContentPDF.Replace("data:application/pdf;base64,", ""),
                                    file_name = model.FileName,
                                    location = model.Location,
                                    nama_doc = model.DocumentType,
                                    reason = model.Reason,
                                    signature_page = model.Page.ToString(),
                                    visLLX = model.VisLLX,
                                    visLLY = model.VisLLY,
                                    visURX = model.VisURX,
                                    visURY = model.VisURY,
                                    materai_img = responeTilaka.data.materai_img,
                                    serial_number = responeTilaka.data.serial_number
                                };
                                _logger.LogInformation("Object: {@requestBody}", contentBodyRestamp);
                                responseRestampTilaka = RestAPIHelper<ResponseMaterai>.Submit(JsonConvert.SerializeObject(contentBodyRestamp), RestSharp.Method.POST, Tilaka.restamp_doc, token);
                                _logger.LogInformation("Object: {@responseBody}", responseRestampTilaka);
                            }
                            else
                            {
                                responseRestampTilaka.success = true;
                            }
                        } while (!responseRestampTilaka.success);
                        //}
                        //else { 
                        //    data.message = responeTilaka.message;
                        //    data.isSucceed= false;
                        //}
                    }
                    //else 
                    //{
                    var dataUpdate = _provider.SaveMaterai(responeTilaka.data, Convert.ToInt32(data.value), false, model.UserLogin);
                    data.isSucceed = responseRestampTilaka.success && dataUpdate.isSucceed;
                    data.message = responseRestampTilaka.success ? responseRestampTilaka.message : dataUpdate.message;
                    //}
                }
                return Ok(data);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error on method Security/SubmitMaterai, {ex.Message}");
                throw;
            }
        }

		[HttpGet]
		[Route("[action]")]
        [AllowAnonymous]
        public ActionResult ForgotPassword(string email, string url) 
        { 
            var data = securityProvider.ForgotPasswordSendMail(email, url);
            return Ok(data);
        }

		[HttpGet]
		[Route("[action]")]
        [AllowAnonymous]
        public ActionResult ConfirmResetPassword(string code, string email, DateTime timeClick)
        {
            var data = securityProvider.ConfirmResetPassword(code, email, timeClick);
            return Ok(data);
        }

		[HttpPost]
		[Route("[action]")]
        [AllowAnonymous]
        public ActionResult ResetPassword([FromBody] ResetPasswordViewModel viewModel)
        { 
            var data = securityProvider.ResetPassword(viewModel.Email, viewModel.Password);
            return Ok(data);
        }
    }
}
