using DigitalSign.DataAccess.Context;
using DigitalSign.Provider.Base;
using DigitalSign.Provider.Master;
using DigitalSign.Provider.Security;
using DigitalSign.Utilities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Sinks.Elasticsearch;
using Swashbuckle.AspNetCore.Filters;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

//Add Serilog with Elasticsearch configuration
builder.Host.UseSerilog((context, configuration) =>
{
    if (context.Configuration["ElasticActive"].ToString() == "true")
    {
        configuration.Enrich.FromLogContext()
            .Enrich.WithMachineName()
            .WriteTo.Console()
            .WriteTo.Elasticsearch(
                new ElasticsearchSinkOptions(node: new Uri(context.Configuration["ElasticConfiguration:Uri"]))
                {
                    IndexFormat = $"{context.Configuration["ApplicationName"]}-logs-{context.HostingEnvironment.EnvironmentName?.ToLower().Replace(oldValue: ".", newValue: "-")}-{DateTime.UtcNow:yyyy-MM}",
                    ModifyConnectionSettings = x => x.BasicAuthentication(context.Configuration["ElasticConfiguration:Username"], context.Configuration["ElasticConfiguration:Password"]),
                    NumberOfShards = 2,
                    NumberOfReplicas = 1,
                    AutoRegisterTemplate = true,
                    OverwriteTemplate = true,
                    TemplateName = "serilog-security",
                    AutoRegisterTemplateVersion = AutoRegisterTemplateVersion.ESv7,
                    TypeName = null,
                    BatchAction = ElasticOpType.Create
                })
            .Enrich.WithProperty(name: "Environment", context.HostingEnvironment.EnvironmentName)
            .ReadFrom.Configuration(context.Configuration);
    }
    else
    {
        configuration.ReadFrom.Configuration(context.Configuration)
        .WriteTo.File(string.Format(context.Configuration["LogFilePath"], DateTime.Today.ToString("yyyy-MM-dd")))
        .Enrich.FromLogContext()
        .Enrich.WithMachineName()
        .Enrich.WithEnvironmentName()
        .WriteTo.Console();
    }
});

//Add servicce DB Context
builder.Services.AddDbContext<DigitalSignContext>(options =>
{
    //options.UseSqlServer("Data Source=34.101.228.175;Initial Catalog=DigitalSign;User Id=sa;Password=Indocyber.100;");
    options.UseSqlServer(DataConfiguration.Configuration.ConnectionStrings.DefaultConnection);
});

//Add service Provider
builder.Services.AddScoped<ISecurityProvider, SecurityProvider>();
builder.Services.AddScoped<IUsersProvider, UsersProvider>();
builder.Services.AddScoped<IDocumentSignProvider, DocumentSignProvider>();
builder.Services.AddScoped<IMasterUserProvider, MasterUserProvider>();
builder.Services.AddScoped<IGlobalSettingProvider, GlobalSettingProvider>();
builder.Services.AddScoped<ActivityLogProvider>();

//Add HttpContextAccessor
builder.Services.AddHttpContextAccessor();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();


// Update SwaggerGen to add Bearer Authorization
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "DigitalSign.Api.Security", Version = "v1" });

    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Description = "JWT Authorization header using the Bearer scheme."

    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            new string[] {}
        }
    });
});

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration.GetSection("Token:Key").Value)),
            ValidateIssuer = false,
            ValidateAudience = false
        };
    });

//Add Json Option to prevent camel case
builder.Services.AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.PropertyNamingPolicy = null;
    });


var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
