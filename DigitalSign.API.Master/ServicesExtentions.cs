﻿using DigitalSign.Provider.Base;
using DigitalSign.Provider.Master;

namespace DigitalSign.API.Master
{
    public static class ServiceExtentions
    {
        public static void ConfigureAddScope(this IServiceCollection services)
        {
            services.AddScoped<IMasterMenuProvider, MasterMenuProvider>();
            services.AddScoped<IMasterRoleProvider, MasterRoleProvider>();
            services.AddScoped<IMasterRoleMenuProvider, MasterRoleMenuProvider>();
            services.AddScoped<IMasterDocumentProvider, MasterDocumentProvider>();
            services.AddScoped<IMasterUserProvider, MasterUserProvider>();
            services.AddScoped<ActivityLogProvider>();
            services.AddScoped<IDocumentSignProvider, DocumentSignProvider>();
            services.AddScoped<IDocumentDiscussionProvider, DocumentDiscussionProvider>();
            services.AddScoped<INotificationProvider, NotificationProvider>();
            services.AddScoped<IMateraiService, MateraiProvider>();
            services.AddScoped<IGlobalSettingProvider, GlobalSettingProvider>();
            services.AddScoped<IDocumentCategoryProvider, DocumentCategoryProvider>();
        }
    }
}
