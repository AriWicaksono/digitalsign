﻿using DigitalSign.Provider.Master;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Master.Role;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DigitalSign.API.Master.Controllers
{
    [Authorize]
    [Route("api/master/[controller]")]
    [ApiController]
    public class DocumentCategoryController : ControllerBase
    {
        private readonly IDocumentCategoryProvider _provider;
        public DocumentCategoryController(IDocumentCategoryProvider provider)
        {
            _provider = provider;
        }

        [HttpGet("[action]")]
        public IActionResult GetAll()
        {
            var data = _provider.GetList();
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetById(int id)
        {
            var data = _provider.GetDocumentCategoryById(id);
            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult AddEditDocumentCategory([FromBody]DocumentCategoryViewModel model, string currentUser)
        {
            var data = _provider.AddEditDocumentCategory(model, currentUser);

            return Ok(data);
        }

        [HttpDelete("[action]")]
        public IActionResult Delete(int id, string currentUser)
        {
            var data = _provider.DeleteDocumentCategory(id, currentUser);
            return Ok(data);
        }
    }
}
