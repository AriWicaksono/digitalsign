﻿using DigitalSign.Provider.Master;
using DigitalSign.ViewModels.Master.User;
using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalSign.API.Master.Controllers
{
	[Authorize]
	[Route("api/master/[controller]")]
    [ApiController]

    public class MasterUserController : Controller
    {
        private readonly IMasterUserProvider provider;
        public MasterUserController(IMasterUserProvider provider)
        {
            this.provider = provider;
        }

        [HttpGet("[action]")]
        public IActionResult GetAll()
        {
            var data = provider.GetAll();
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetForDropdownList()
        {
            var data = provider.GetForDropdownList();
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetById(int id)
        {
            var data = provider.GetById(id);
            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult Save(MasterUserViewModel model)
        {
            var data = provider.Save(model, "Dev");
            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult ChangeRegistrationId(ChangeRegIdViewModel model)
        {
            var data = provider.ChangeRegistrationID(model);
            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult Delete(int id)
        {
            var data = provider.Delete(id);
            return Ok(data);
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult UpdateTilakaName(string requestId, string tilakaName)
        {
            var data = provider.UpdateTilakaName(requestId, tilakaName);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetListRole()
        {
            var data = provider.GetListRole();

            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult UpdateAccountInfo(AccountInfoUserViewModel model)
        {
            var data = provider.UpdateAccountInfo(model);

            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult ChangePassword(ChangePasswordUserViewModel model)
        {
            var data = provider.ChangePassword(model);

            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult UploadCertificate(UploadCertificateViewModel model)
        {
            var data = provider.UploadCertificate(model);

            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult CreateSign(CreateSignViewModel model)
        {
            var data = provider.CreateSign(model);

            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult ResetSignature(int id)
        {
            var data = provider.ResetSignature(id);

            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult RegisterAdmin([FromBody]RegisterAdminInsertViewModel model, string currentUser) 
        {
            var data = provider.RegisterAdmin(model, currentUser);
            return Ok(data);
        }
    }
}
