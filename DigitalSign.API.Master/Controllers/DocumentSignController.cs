﻿using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Master;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using System.Net;

namespace DigitalSign.API.Master.Controllers
{
    [Route("api/master/[controller]")]
    [ApiController]
    [Authorize]
    public class DocumentSignController : Controller
    {
        private readonly IDocumentSignProvider provider;
        private readonly IMasterDocumentProvider masterProvider;
        private readonly IGlobalSettingProvider globalProvider;
        private readonly ILogger<DocumentSignController> _logger;
        public DocumentSignController(IDocumentSignProvider provider, IMasterDocumentProvider masterProvider, IGlobalSettingProvider globalProvider, ILogger<DocumentSignController> logger)
        {
            this.provider = provider;
            this.masterProvider = masterProvider;
            this.globalProvider = globalProvider;
            _logger = logger;
        }

        [HttpGet("[action]")]
        public IActionResult GetData(int DocId, int UserId)
        {
            var data = provider.GetData(DocId, UserId);
            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult AddDocumentSign(DocumentSignViewModel model)
        {
            var data = provider.AddDocumentSign(model);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult DeleteDocumentSign(int id)
        {
            var data = provider.DeleteDocumentSign(id);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetDataRequest(int DocId)
        {
            var data = provider.GetDataRequest(DocId);
            if (data != null)
            {
                string FtpAddress = masterProvider.GetGlobalConfigurationByKeyName("FtpAddress");
                string ftpUsername = masterProvider.GetGlobalConfigurationByKeyName("ftpUsername");
                string ftpPassword = masterProvider.GetGlobalConfigurationByKeyName("ftpPassword");
                foreach (var item in data.list_pdf)
                {
                    item.file = string.Format("{0}{1}", "data:application/pdf;base64,", FTPHelper.GetStringBase64(item.file, ftpUsername, ftpPassword, FtpAddress));
                }
            }
            return Ok(data);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> RequestSign(int idDoc, string currentUser)
        {
            var result = new AjaxViewModel();
            //int? logId = null;
			var logApi = new LogTilakaViewModel()
			{
				RequestStart = DateTime.Now,
				ApiName = "RequestSignIn",
				EndPoint = Tilaka.signing_requestsign,
				Method = "POST",
				RequestBy = currentUser,
				ResponseStatus = ViewModels.Security.ResponseStatus.Started,
                ReffID = idDoc
			};
			var logId = globalProvider.LogTilaka(logApi, null);
			try
            {
                var resultParticipant = masterProvider.GetAllParticipant(idDoc);
                var sequenceCurrentUser = resultParticipant.SingleOrDefault(x => x.Username == currentUser).Sequence;

                if (sequenceCurrentUser > 1)
                {
                    var prevUser = resultParticipant.SingleOrDefault(x => x.Sequence == sequenceCurrentUser - 1);
                    if (prevUser.Status != "none")
                    {
                        var resultNext = masterProvider.GetNextParticipantUrl(idDoc, prevUser.Username, currentUser);
                        if (resultNext != null)
                        {
                            var resultUpdateRequestID = masterProvider.UpdateRequestID(idDoc, resultNext.request_id, currentUser, resultNext.url);
                            result.SetValues(true, resultNext.url, "success");
                        }
                        else
                        {
                            result.SetValues(false, null, "Next Signer not found");
                        }
                    }
                    else
                    {
                        result.SetValues(false, null, "Document harus di tanda tangani secara berurutan, mohon menunggu giliran anda...");
                    }
                }
                else
                {
                    var content = provider.GetDataRequest(idDoc);
                    if (content != null)
                    {
                        string FtpAddress = masterProvider.GetGlobalConfigurationByKeyName("FtpAddress");
                        string ftpUsername = masterProvider.GetGlobalConfigurationByKeyName("ftpUsername");
                        string ftpPassword = masterProvider.GetGlobalConfigurationByKeyName("ftpPassword");
                        foreach (var item in content.list_pdf)
                        {
                            item.file = string.Format("{0}{1}", "data:application/pdf;base64,", FTPHelper.GetStringBase64(item.file, ftpUsername, ftpPassword, FtpAddress));
                        }
                    }

                    var token = await Tilaka.GetTilakaToken();

                    if (String.IsNullOrEmpty(content.request_id))
                    {
                        content.request_id = await Tilaka.GenerateUUID(token);
                        string json = JsonConvert.SerializeObject(content);
						logApi.RequestBody = json;
                        logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Processing;
						globalProvider.LogTilaka(logApi, logId);
						var responeTilaka = RestAPIHelper<ResponeRequestSign>.Submit(json, Method.POST, Tilaka.signing_requestsign, token);

                        if (responeTilaka.success)
                        {
                            var resultUpdateRequestID = masterProvider.UpdateRequestID(idDoc, content.request_id, currentUser, responeTilaka.auth_urls[0].user_identifier);
                            logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Success;
                        }
                        else
                        {
                            logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Failed;
                        }
                        result.SetValues(responeTilaka.success, responeTilaka.auth_urls != null ? responeTilaka.auth_urls[0].url : null, responeTilaka.message);
                        logApi.ResponseBody = JsonConvert.SerializeObject(responeTilaka);
                        logApi.RequestEnd = DateTime.Now;
                        globalProvider.LogTilaka(logApi, logId);
                    }
                    else
                    {
                        var getUrlExist = masterProvider.UrlWebViewFailed(idDoc, currentUser);

                        if (string.IsNullOrEmpty(getUrlExist))
                        {
                            result.SetValues(true, getUrlExist, "");
                        }
                        else
                        {
                            content.request_id = await Tilaka.GenerateUUID(token);
                            string json = JsonConvert.SerializeObject(content);
							logApi.RequestBody = json;
							logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Processing;
							globalProvider.LogTilaka(logApi, logId);
							var responeTilaka = RestAPIHelper<ResponeRequestSign>.Submit(json, Method.POST, Tilaka.signing_requestsign, token);

                            if (responeTilaka.success)
                            {
                                var resultUpdateRequestID = masterProvider.UpdateRequestID(idDoc, content.request_id, responeTilaka.auth_urls[0].user_identifier, responeTilaka.auth_urls[0].url);
								logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Success;
							}
							else
							{
								logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Failed;
							}
                            result.SetValues(responeTilaka.success, responeTilaka.auth_urls != null ? responeTilaka.auth_urls[0].url : null, responeTilaka.message);
							logApi.ResponseBody = JsonConvert.SerializeObject(responeTilaka);
							logApi.RequestEnd = DateTime.Now;
							globalProvider.LogTilaka(logApi, logId);
						}
                    }
                }
            }
            catch (Exception ex)
            {
                logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Error;
                logApi.ErrorMessage = ex.Message;
                globalProvider.LogTilaka(logApi, logId);
                result.SetValues(false, null, ex.Message);
            }
            return Ok(result);
        }

    }
}
