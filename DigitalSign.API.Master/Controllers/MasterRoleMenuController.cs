﻿using DigitalSign.Provider.Master;
using DigitalSign.ViewModels.Master.RoleMenu;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalSign.API.Master.Controllers
{
    [Authorize]
    [Route("api/master/[controller]")]
    [ApiController]
    public class MasterRoleMenuController : ControllerBase
    {
        private readonly IMasterRoleMenuProvider provider;
        public MasterRoleMenuController(IMasterRoleMenuProvider provider)
        {
            this.provider = provider;
        }

        [HttpGet("[action]")]
        public IActionResult GetAll()
        {
            var data = provider.GetAll();

            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetById(int roleMenuID)
        {
            var data = provider.GetById(roleMenuID);

            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult Save(MasterRoleMenuViewModel model)
        {
            var data = provider.Save(model, "create");

            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult Delete(int id)
        {
            var data = provider.Delete(id);

            return Ok(data);
        }
        [HttpGet("[action]")]
        public IActionResult GetListRole()
        {
            var data = provider.GetListRole();

            return Ok(data);
        }
        [HttpGet("[action]")]
        public IActionResult GetListMenu()
        {
            var data = provider.GetListMenu();

            return Ok(data);
        }
    }
}
