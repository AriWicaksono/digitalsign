﻿using DigitalSign.Provider.Master;
using DigitalSign.ViewModels.Master.Role;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalSign.API.Master.Controllers
{
    [Route("api/master/[controller]")]
    [ApiController]
    [Authorize]
    public class MasterRoleController : Controller
    {
        private readonly IMasterRoleProvider provider;
        public MasterRoleController(IMasterRoleProvider provider)
        {
            this.provider = provider;
        }

        [HttpGet("[action]")]
        public IActionResult GetAll()
        {
            var data = provider.GetAll();
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetById(int roleID)
        {
            var data = provider.GetById(roleID);
            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult Save(MasterRoleViewModel model)
        {
            var data = provider.Save(model, "Dev");

            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult Delete(int roleID)
        {
            var data = provider.Delete(roleID);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetAllRoleAdmin() {
            var data = provider.GetAll().Where(m => m.IsAdmin && m.IsActive);
            return Ok(data);
        }
    }
}
