﻿using DigitalSign.Provider.Master;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Master.User;
using DigitalSign.ViewModels.Security;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace DigitalSign.API.Master.Controllers
{
	[Authorize]
	[Route("api/master/[controller]")]
	[ApiController]
	public class MateraiController : ControllerBase
	{
		private readonly IMateraiService _materai;
		private readonly IGlobalSettingProvider _global;
		private readonly ILogger<MateraiController> _logger;

		public MateraiController(IMateraiService materai, IGlobalSettingProvider global, ILogger<MateraiController> logger)
		{
			_materai = materai;
			_global = global;
			_logger = logger;
		}

		[HttpPost("[action]")]
		public IActionResult Save(MateraiViewModel model)
		{
			var data = _materai.Save(model, model.UserID);
			return Ok(data);
		}

		[HttpGet]
		[Route("GetList")]
		public IActionResult GetList(string currentUser)
		{
			try
			{
				var data = _materai.GetList(currentUser);
				//DataSourceRequest req = GridUtilities.ConvertToKendoFromCustomRequest(request);
				//var vm = data.ToDataSourceResult(req);
				foreach (var item in data)
				{
					item.ID = EncryptionHelper.EncryptUrlParam(item.ID); 
				}
				return Ok(data);
			}
			catch (Exception exception)
			{
				//log.LogInformation(exception.ToString());
				return NotFound(exception.Message);
			}
		}

        [HttpGet("[action]")]
        public IActionResult ListLog(string docId)
        {
            try
            {
				var id = Convert.ToInt32(EncryptionHelper.DecryptUrlParam(docId));
                var data = _materai.GetListLog(id);
                //DataSourceRequest req = GridUtilities.ConvertToKendoFromCustomRequest(request);
                //var vm = data.ToDataSourceResult(req);
                foreach (var item in data)
                {
                    item.ID = EncryptionHelper.EncryptUrlParam(item.ID);
                }
                return Ok(data);
            }
            catch (Exception exception)
            {
                //log.LogInformation(exception.ToString());
                return NotFound(exception.Message);
            }
        }

        [HttpGet("[action]")]
		public async Task<IActionResult> GetListDocumentType()
		{
			try
			{
				//var token = await Tilaka.GetTilakaToken();
				var responseTilaka = await ListDocumentType();
				return Ok(responseTilaka.data);
			}
			catch (Exception ex)
			{
				_logger.LogError("Error on method GetListDocumentType " + ex.Message);
				throw;
			}
		}

		[HttpGet("[action]")]
		public async Task<IActionResult> GetListQuota(string currentUser)
		{
			try
			{
				var data = _materai.GetListQuota(currentUser);
				return Ok(data);
			}
			catch (Exception ex)
			{
				_logger.LogError("Error on method GetListDocumentType " + ex.Message);
				throw;
			}
		}

		[HttpPost("[action]")]
		public async Task<IActionResult> SubmitStamping(DocumentMateraiNewViewModel model)
		{
			//_logger.LogInformation("Object: {@requestBody}", model);
			//_logger.LogInformation("String: {@requestBody}", displayName);
			var ajaxVm = new AjaxViewModel();
			var logApi = new LogTilakaViewModel()
			{
				RequestStart = DateTime.Now,
				ApiName = "Stamping",
				EndPoint = Tilaka.stamp_doc,
				Method = "POST",
				RequestBy = model.UserLogin,
				RequestEnd = DateTime.Now,
				ResponseStatus = ViewModels.Security.ResponseStatus.Started
			};
			var logId = _global.LogTilaka(logApi, null);
			try
			{
				DateTime start = DateTime.Now;
				
				string extension = Path.GetExtension(model.FileName);
				var date = DateTime.Now;
				string fileName = $"{Path.GetFileNameWithoutExtension(model.FileName)}_{date.ToString("hhmmfff")}{extension}";
				string filePath = $"{date.ToString("yyyy/MMMM/dd")}/Materai/{model.DocumentType}";
				if (fileName.Length >= 50)
				{
					ajaxVm.SetValues(false, null, "File Name is too long");
					return Ok(ajaxVm);
				}
				string FtpAddress = _materai.GetGlobalConfigurationByKeyName("FtpAddress");
				string ftpUsername = _materai.GetGlobalConfigurationByKeyName("ftpUsername");
				string ftpPassword = _materai.GetGlobalConfigurationByKeyName("ftpPassword");
				FTPHelper.CreateFolder(filePath, ftpUsername, ftpPassword, FtpAddress);
				var filePathFix = $"{filePath}/{fileName}";
				FTPHelper.UploadFile(Convert.FromBase64String(model.ContentPDF), filePathFix, ftpUsername, ftpPassword, FtpAddress);
				model.FileName = fileName;
				model.FilePath = filePathFix;

				_logger.LogInformation("Object: {@requestBody}", model);
				var data = _materai.SubmitStamping(model);//simpen materai
				if (data.isSucceed)//send ke tilaka
				{
					_logger.LogInformation("Object: {@responseBody}", data);
					var token = await Tilaka.GetTilakaToken();
					var contentBody = new RequestStampMaterai()
					{
						content_pdf = model.ContentPDF.Replace("data:application/pdf;base64,", ""),
						file_name = model.FileName,
						location = model.Location,
						nama_doc = model.DocumentTypeCode,
						reason = model.Reason,
						signature_page = model.Page.ToString(),
						visLLX = model.VisLLX,
						visLLY = model.VisLLY,
						visURX = model.VisURX,
						visURY = model.VisURY
					};
					var jbody = JsonConvert.SerializeObject(contentBody);
					_logger.LogInformation("Object: {@requestBody}", contentBody);
					_logger.LogInformation("Object: {@tokenTilaka}", token);
					logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Processing;
					logApi.ReffID = Convert.ToInt32(data.Data);
					logApi.RequestBody = jbody;
					_global.LogTilaka(logApi, logId);
					var responeTilaka = RestAPIHelper<ResponseMaterai>.Submit(jbody, RestSharp.Method.POST, Tilaka.stamp_doc, token);
					logApi.ResponseBody = JsonConvert.SerializeObject(responeTilaka);
					//var responseRestampTilaka = new ResponseMaterai();
					var isValid = false;
					_logger.LogInformation("Object: {@responseBody}", responeTilaka);
					if (!responeTilaka.success)
					{
						logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Failed;
						if (responeTilaka.data != null)
						{
							var dataUpdateRestamp = _materai.SaveMaterai(responeTilaka.data, Convert.ToInt32(data.Data), model.UserLogin);
							data.SetValues(false, null, responeTilaka.message);
						}
						else {
							data.SetValues(false, null, responeTilaka.message);
						}
						//do
						//{
						//    var startRestamp = DateTime.Now;
						//    if (dataUpdateRestamp.isSucceed)
						//    {
						//        var contentBodyRestamp = new RequestRestampMaterai()
						//        {
						//            content_pdf = model.ContentPDF.Replace("data:application/pdf;base64,", ""),
						//            file_name = model.FileName,
						//            location = model.Location,
						//            nama_doc = model.DocumentTypeCode,
						//            reason = model.Reason,
						//            signature_page = model.Page.ToString(),
						//            visLLX = model.VisLLX,
						//            visLLY = model.VisLLY,
						//            visURX = model.VisURX,
						//            visURY = model.VisURY,
						//            materai_img = responeTilaka.data.materai_img,
						//            serial_number = responeTilaka.data.serial_number
						//        };
						//        var jbodyRestamp = JsonConvert.SerializeObject(contentBodyRestamp);
						//        _logger.LogInformation("Object: {@requestBody}", contentBodyRestamp);
						//        responseRestampTilaka = RestAPIHelper<ResponseMaterai>.Submit(jbodyRestamp, RestSharp.Method.POST, Tilaka.restamp_doc, token);
						//        var logApiRestamp = new LogTilakaViewModel()
						//        {
						//            RequestStart = startRestamp,
						//            ApiName = "Restamping",
						//            RequestBody = jbodyRestamp,
						//            EndPoint = Tilaka.restamp_doc,
						//            Method = "POST",
						//            RequestBy = model.UserLogin,
						//            ResponseBody = JsonConvert.SerializeObject(responseRestampTilaka),
						//            RequestEnd = DateTime.Now
						//        };
						//        logsApi.Add(logApiRestamp);
						//        _logger.LogInformation("Object: {@responseBody}", responseRestampTilaka);
						//        isValid = responseRestampTilaka.success;
						//    }
						//    else
						//    {
						//        isValid = true;
						//    }
						//} while (!isValid);

						//if (responseRestampTilaka.success)
						//{
						//    var filePathStamp = $"DigitalSign/{model.UserLogin}_stamppedDoc_{timeStamp}{extension}";
						//    FTPHelper.UploadFile(Convert.FromBase64String(responseRestampTilaka.data.result_doc), filePathStamp, ftpUsername, ftpPassword, FtpAddress);
						//    var hasilStamp = new MateraiResult()
						//    {
						//        serial_number = responseRestampTilaka.data.serial_number,
						//        materai_img = responseRestampTilaka.data.materai_img,
						//        result_doc = filePathStamp
						//    };
						//    var dataUpdate = _materai.SaveMaterai(hasilStamp, Convert.ToInt32(data.Data), false, model.UserLogin);
						//    data.SetValues(true, responseRestampTilaka.data.result_doc, responseRestampTilaka.message);
						//}
						//else
						//{
						//    data.SetValues(false, null, responseRestampTilaka.message);
						//}

					}
					else
					{
						logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Success;
						var filePathStamp = $"{filePath}/{model.UserLogin}_stamppedDoc_{date.ToString("hhmmfff")}{extension}";
						FTPHelper.UploadFile(Convert.FromBase64String(responeTilaka.data.result_doc), filePathStamp, ftpUsername, ftpPassword, FtpAddress);
						var hasilStamp = new MateraiResult()
						{
							serial_number = responeTilaka.data.serial_number,
							materai_img = responeTilaka.data.materai_img,
							result_doc = filePathStamp
						};
						var dataUpdate = _materai.SaveMaterai(hasilStamp, Convert.ToInt32(data.Data), false, model.UserLogin);
						//_materai.UpdateQuota(model.MateraiID, model.UserLogin);
						data.SetValues(true, responeTilaka.data.result_doc, responeTilaka.message);
					}
					_global.LogTilaka(logApi, logId);
					//}
				}
				return Ok(data);

			}
			catch (Exception e)
			{
				_logger.LogError(e, "Save");
				logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Error;
				logApi.ErrorMessage = e.Message;
				_global.LogTilaka(logApi, logId);
				ajaxVm.SetValues(false, logApi.ReffID == null ? "Error Saving!" : null, e.Message);
				return Ok(ajaxVm);
			}
		}

		[HttpGet("[action]")]
		public IActionResult GetStamppedDoc(string docId)
		{
			var id = Convert.ToInt32(EncryptionHelper.DecryptUrlParam(docId));
			var getStamp = _materai.GetDocumentStampped(id);
			string FtpAddress = _materai.GetGlobalConfigurationByKeyName("FtpAddress");
			string ftpUsername = _materai.GetGlobalConfigurationByKeyName("ftpUsername");
			string ftpPassword = _materai.GetGlobalConfigurationByKeyName("ftpPassword");
			getStamp.ID = EncryptionHelper.EncryptUrlParam(getStamp.ID);
			//getStamp.ContentPDF = string.Format("{0}{1}", "data:application/pdf;base64,", FTPHelper.GetStringBase64(getStamp.StampedDoc, ftpUsername, ftpPassword, FtpAddress));
			getStamp.ContentPDF = FTPHelper.GetStringBase64(getStamp.StampedDoc, ftpUsername, ftpPassword, FtpAddress);
            return Ok(getStamp);
		}

		[HttpGet("[action]")]
		public IActionResult GetDetailDoc(string docId)
		{
			var id = Convert.ToInt32(EncryptionHelper.DecryptUrlParam(docId));
			var getStamp = _materai.GetDocument(id);
			string FtpAddress = _materai.GetGlobalConfigurationByKeyName("FtpAddress");
			string ftpUsername = _materai.GetGlobalConfigurationByKeyName("ftpUsername");
			string ftpPassword = _materai.GetGlobalConfigurationByKeyName("ftpPassword");
			getStamp.ID = EncryptionHelper.EncryptUrlParam(getStamp.ID);
			if (!string.IsNullOrEmpty(getStamp.FilePath))
			{
				getStamp.ContentPDF = FTPHelper.GetStringBase64(getStamp.FilePath, ftpUsername, ftpPassword, FtpAddress);
				//getStamp.ContentPDF = string.Format("{0}{1}", "data:application/pdf;base64,", FTPHelper.GetStringBase64(getStamp.FilePath, ftpUsername, ftpPassword, FtpAddress));
			}
			else
			{
				getStamp.ContentPDF = FTPHelper.GetStringBase64(getStamp.FileName, ftpUsername, ftpPassword, FtpAddress);
				//getStamp.ContentPDF = string.Format("{0}{1}", "data:application/pdf;base64,", FTPHelper.GetStringBase64(getStamp.FileName, ftpUsername, ftpPassword, FtpAddress));
			}
			return Ok(getStamp);
		}

        [HttpPost("[action]")]
		public async Task<IActionResult> RestampMaterai(string docId, string currentUser)
		{
			var ajaxVm = new AjaxViewModel();
			var id = Convert.ToInt32(EncryptionHelper.DecryptUrlParam(docId));
			var logApiRestamp = new LogTilakaViewModel()
			{
				RequestStart = DateTime.Now,
				ApiName = "Restamping",
				EndPoint = Tilaka.restamp_doc,
				Method = "POST",
				RequestBy = currentUser,
				RequestEnd = DateTime.Now,
				ResponseStatus = ViewModels.Security.ResponseStatus.Started,
				ReffID = id,
			};
			var logId = _global.LogTilaka(logApiRestamp, null);
			try
			{
				var start = DateTime.Now;
				var model = _materai.GetDocument(id);
				string FtpAddress = _materai.GetGlobalConfigurationByKeyName("FtpAddress");
				string ftpUsername = _materai.GetGlobalConfigurationByKeyName("ftpUsername");
				string ftpPassword = _materai.GetGlobalConfigurationByKeyName("ftpPassword");
				if (!string.IsNullOrEmpty(model.FilePath))
				{
					model.ContentPDF = FTPHelper.GetStringBase64(model.FilePath, ftpUsername, ftpPassword, FtpAddress);
				}
				else
				{
					model.ContentPDF = FTPHelper.GetStringBase64(model.FileName, ftpUsername, ftpPassword, FtpAddress);
				}

				var token = await Tilaka.GetTilakaToken();

				var contentBodyRestamp = new RequestRestampMaterai()
				{
					content_pdf = model.ContentPDF.Replace("data:application/pdf;base64,", ""),
					file_name = model.FileName,
					location = model.Location,
					nama_doc = model.DocumentTypeCode,
					reason = model.Reason,
					signature_page = model.Page.ToString(),
					visLLX = model.VisLLX,
					visLLY = model.VisLLY,
					visURX = model.VisURX,
					visURY = model.VisURY,
					materai_img = model.Materai,
					serial_number = model.SerialNumber
				};

				var jbodyRestamp = JsonConvert.SerializeObject(contentBodyRestamp);
				logApiRestamp.ResponseStatus = ViewModels.Security.ResponseStatus.Processing;
				logApiRestamp.RequestBody = jbodyRestamp;
				_global.LogTilaka(logApiRestamp, logId);
				_logger.LogInformation("Object: {@requestBody}", contentBodyRestamp);
				var responseRestampTilaka = RestAPIHelper<ResponseMaterai>.Submit(jbodyRestamp, RestSharp.Method.POST, Tilaka.restamp_doc, token);
				_logger.LogInformation("Object: {@responseBody}", responseRestampTilaka);

				logApiRestamp.ResponseBody = JsonConvert.SerializeObject(responseRestampTilaka);
				if (responseRestampTilaka.success)
				{
					logApiRestamp.ResponseStatus = ViewModels.Security.ResponseStatus.Success;
					string extension = Path.GetExtension(model.FileName);
					string timeStamp = DateTime.Now.ToString("hhmmfff");
					var filePathStamp = $"{model.UserLogin}_stamppedDoc_{timeStamp}{extension}";

					// modified by martine 
					var folder = Path.GetDirectoryName(model.FilePath);

					if (!string.IsNullOrEmpty(model.FilePath))
					{
						filePathStamp = $"{folder}/{filePathStamp}";
					}
					else
					{
						var date = DateTime.Now;
						string filePath = $"{date.ToString("yyyy/MMMM/dd")}/Materai/{model.DocumentType}";
						filePathStamp = $"{filePath}/{filePathStamp}";
					}
					FTPHelper.UploadFile(Convert.FromBase64String(responseRestampTilaka.data.result_doc), filePathStamp, ftpUsername, ftpPassword, FtpAddress);
					var hasilStamp = new MateraiResult()
					{
						serial_number = responseRestampTilaka.data.serial_number,
						materai_img = responseRestampTilaka.data.materai_img,
						result_doc = filePathStamp
					};
					var dataUpdate = _materai.SaveMaterai(hasilStamp, Convert.ToInt32(model.ID), currentUser);
					ajaxVm.SetValues(true, null, responseRestampTilaka.message);
				}
				else
				{
					logApiRestamp.ResponseStatus = ViewModels.Security.ResponseStatus.Failed;
					ajaxVm.SetValues(false, null, responseRestampTilaka.message);
				}
				_global.LogTilaka(logApiRestamp, logId);

			}
			catch (Exception ex)
			{
				logApiRestamp.ResponseStatus = ViewModels.Security.ResponseStatus.Error;
				logApiRestamp.ErrorMessage = ex.Message;
				_global.LogTilaka(logApiRestamp, logId);
				_logger.LogError(ex.Message);
				ajaxVm.SetValues(false, null, ex.Message);

			}

			return Ok(ajaxVm);
		}

        [HttpPost("[action]")]
        public async Task<IActionResult> ResubmitMaterai(string currentUser, [FromBody] DocumentMateraiDetailViewModel viewModel)
        {
            var ajaxVm = new AjaxViewModel();
            var id = Convert.ToInt32(EncryptionHelper.DecryptUrlParam(viewModel.ID));
			viewModel.ID = id.ToString();
            var logApi = new LogTilakaViewModel()
            {
                RequestStart = DateTime.Now,
                ApiName = "Stamping",
                EndPoint = Tilaka.stamp_doc,
                Method = "POST",
                RequestBy = currentUser,
                RequestEnd = DateTime.Now,
                ResponseStatus = ViewModels.Security.ResponseStatus.Started,
                ReffID = id,
            };
            var logId = _global.LogTilaka(logApi, null);
            try
            {
                var start = DateTime.Now;
				var updateDocumentMateraiLocation = _materai.UpdateStampLocation(viewModel, currentUser);
                var model = _materai.GetDocument(id);
                string FtpAddress = _materai.GetGlobalConfigurationByKeyName("FtpAddress");
                string ftpUsername = _materai.GetGlobalConfigurationByKeyName("ftpUsername");
                string ftpPassword = _materai.GetGlobalConfigurationByKeyName("ftpPassword");
                if (!string.IsNullOrEmpty(model.FilePath))
                {
                    model.ContentPDF = FTPHelper.GetStringBase64(model.FilePath, ftpUsername, ftpPassword, FtpAddress);
                }
                else
                {
                    model.ContentPDF = FTPHelper.GetStringBase64(model.FileName, ftpUsername, ftpPassword, FtpAddress);
                }

                var token = await Tilaka.GetTilakaToken();

                var contentBody = new RequestStampMaterai()
                {
                    content_pdf = model.ContentPDF.Replace("data:application/pdf;base64,", ""),
                    file_name = model.FileName,
                    location = model.Location,
                    nama_doc = model.DocumentTypeCode,
                    reason = model.Reason,
					signature_page = model.Page.ToString(),
					visLLX = model.VisLLX,
					visLLY = model.VisLLY,
					visURX = model.VisURX,
					visURY = model.VisURY
				};

                var jbody = JsonConvert.SerializeObject(contentBody);
                logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Processing;
                logApi.RequestBody = jbody;
                logId = _global.LogTilaka(logApi, logId);
                _logger.LogInformation("Object: {@requestBody}", contentBody);
                var responseTilaka = RestAPIHelper<ResponseMaterai>.Submit(jbody, RestSharp.Method.POST, Tilaka.stamp_doc, token);
                _logger.LogInformation("Object: {@responseBody}", responseTilaka);

                logApi.ResponseBody = JsonConvert.SerializeObject(responseTilaka);
                if (responseTilaka.success)
                {
                    logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Success;
                    string extension = Path.GetExtension(model.FileName);
                    string timeStamp = DateTime.Now.ToString("hhmmfff");
                    var filePathStamp = $"{model.UserLogin}_stamppedDoc_{timeStamp}{extension}";

                    // modified by martine 
                    var folder = Path.GetDirectoryName(model.FilePath);

                    if (!string.IsNullOrEmpty(model.FilePath))
                    {
                        filePathStamp = $"{folder}/{filePathStamp}";
                    }
                    else
                    {
                        var date = DateTime.Now;
                        string filePath = $"{date.ToString("yyyy/MMMM/dd")}/Materai/{model.DocumentType}";
                        filePathStamp = $"{filePath}/{filePathStamp}";
                    }
                    FTPHelper.UploadFile(Convert.FromBase64String(responseTilaka.data.result_doc), filePathStamp, ftpUsername, ftpPassword, FtpAddress);
                    var hasilStamp = new MateraiResult()
                    {
                        serial_number = responseTilaka.data.serial_number,
                        materai_img = responseTilaka.data.materai_img,
                        result_doc = filePathStamp
                    };
                    var dataUpdate = _materai.SaveMaterai(hasilStamp, Convert.ToInt32(model.ID), currentUser);
                    ajaxVm.SetValues(true, null, responseTilaka.message);
                }
                else
                {
                    logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Failed;
                    ajaxVm.SetValues(false, null, responseTilaka.message);
                }
                logId = _global.LogTilaka(logApi, logId);

            }
            catch (Exception ex)
            {
                logApi.ResponseStatus = ViewModels.Security.ResponseStatus.Error;
                logApi.ErrorMessage = ex.Message;
                _global.LogTilaka(logApi, logId);
                _logger.LogError(ex.Message);
                ajaxVm.SetValues(false, null, ex.Message);

            }

            return Ok(ajaxVm);
        }

		private async Task<ResponeDocumentType> ListDocumentType() 
		{
            try
            {
                var token = await Tilaka.GetTilakaToken();
                var responseTilaka = RestAPIHelper<ResponeDocumentType>.Submit("", Method.GET, Tilaka.document_type, token);
				return responseTilaka;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error on method ListDocumentType " + ex.Message);
                throw;
            }
        }
    }
}
