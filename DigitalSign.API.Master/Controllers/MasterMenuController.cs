﻿using DigitalSign.Provider.Master;
using DigitalSign.ViewModels.Master.Menu;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalSign.API.Master.Controllers
{
    [Authorize]
    [Route("api/master/[controller]")]
    [ApiController]
    public class MasterMenuController : ControllerBase
    {
        private readonly IMasterMenuProvider provider;

        public MasterMenuController(IMasterMenuProvider provider)
        {
            this.provider = provider;
        }

        [HttpGet("[action]")]
        public IActionResult GetAll()
        {
            var data = provider.GetAll();

            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetById(int id)
        {
            var data = provider.GetById(id);

            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult Save(MasterMenuViewModel model)
        {
            var data = provider.Save(model, "Dev");

            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult Delete(int id)
        {
            var data = provider.Delete(id);

            return Ok(data);
        }

        [HttpGet]
        [Route("GetParentLookUp/{roleID}")]
        public IActionResult GetParentLookUp(int roleID)
        {
            try
            {
                var data = provider.GetParentLookUp(roleID);
                return Ok(data);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
