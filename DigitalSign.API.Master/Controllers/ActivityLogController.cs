﻿using DigitalSign.Provider.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalSign.API.Master.Controllers
{
    [Route("api/master/[controller]")]
    [ApiController]
    [Authorize]
    public class ActivityLogController : Controller
	{
        private readonly ActivityLogProvider provider;
        public ActivityLogController(ActivityLogProvider provider)
        {
            this.provider = provider;
        }

        [HttpGet("[action]")]
        public IActionResult GetActivity(int UserID)
        {
            var data = provider.GetActivity(UserID);
            return Ok(data);
        }
    }
}
