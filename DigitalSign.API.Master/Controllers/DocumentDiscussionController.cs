﻿using DigitalSign.Provider.Master;
using DigitalSign.ViewModels.Master.Document;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalSign.API.Master.Controllers
{
    [Route("api/master/[controller]")]
    [ApiController]
    [Authorize]
    public class DocumentDiscussionController : Controller
    {
        private readonly IDocumentDiscussionProvider provider;
        public DocumentDiscussionController(IDocumentDiscussionProvider provider)
        {
            this.provider = provider;
        }

        [HttpGet("[action]")]
        public IActionResult GetDiscussion(int DocumentID)
        {
            var data = provider.GetDiscussion(DocumentID);
            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult AddDiscussion(DocumentDiscussionViewModel model)
        {
            var data = provider.AddDiscussion(model);

            return Ok(data);
        }
    }
}
