﻿using DigitalSign.Provider.Master;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace DigitalSign.API.Master.Controllers
{
    [Route("api/master/[controller]")]
    [ApiController]
    [Authorize]
    public class MasterDocumentController : Controller
    {
        private readonly IMasterDocumentProvider provider;
        private readonly IDocumentSignProvider _docProvider;
        private readonly ILogger<MasterDocumentController> _logger;
        public MasterDocumentController(IMasterDocumentProvider provider, IDocumentSignProvider docProvider, ILogger<MasterDocumentController> logger)
        {
            this.provider = provider;
            _docProvider = docProvider;
            _logger = logger;
        }

        [HttpGet("[action]")]
        public IActionResult GetAll(string role, int userID)
        {
            var data = provider.GetAll(role, userID);
            return Ok(data);
        }

        //[HttpGet("[action]")]
        //public IActionResult GetById(int id)
        //{
        //    var data = provider.GetById(id);
        //    return Ok(data);
        //}

        //[HttpPost("[action]")]
        //public IActionResult Save(List<MasterDocumentViewModel> model, string displayName)
        //{
        //    var data = provider.Save(model, displayName);

        //    return Ok(data);
        //}

        [HttpPost("[action]")]
        public IActionResult Delete(int id)
        {
            var data = provider.Delete(id);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult UpdateRequestID(int DocumentID, string RequestID)
        {
            var data = provider.UpdateRequestID(DocumentID, RequestID);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult UpdateRequestIDNew(int documentID, string requestID, string userSign, string url)
        {
            var data = provider.UpdateRequestID(documentID, requestID, userSign, url);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetByRequestID(string RequestID)
        {
            var data = provider.GetByRequestID(RequestID);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetAllParticipant(int DocumentID)
        {
            var data = provider.GetAllParticipant(DocumentID);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult UpdateStatusSignDocument(int DocumentID, int UserID, string Status)
        {
            var data = provider.UpdateStatusSignDocument(DocumentID, UserID, Status);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult SendDocument(int documentID)
        {
            var data = provider.SendDocument(documentID);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult CountStatusDocument(int userId, string roleName)
        {
            var data = provider.CountStatusDocument(userId, roleName);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult Search(int userId, string roleName, string keyword)
        {
            var data = provider.Search(userId, roleName, keyword);
            return Ok(data);
        }

        [AllowAnonymous]
        [Route("/DocumentSigning/[action]")]
        [HttpPost]
        public IActionResult NextSigner(CallbackSigningViewModel model)
        {
            /*
            Disini rencana nya request body yang dikirim dari tilaka akan disimpan di log
            Pastikan juga request_id nya masuk agar bisa jadi indexing ketika pencarian log nya
             */
            return Ok();
        }

        [HttpGet("[action]")]
        public IActionResult GetNextParticipantUrl(int docId, string lastSigner, string nextSigner) 
        {
            var data = provider.GetNextParticipantUrl(docId, lastSigner, nextSigner);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetDocumentForMaterai(int docId)
        {
            var data = provider.GetDocumentForMaterai(docId);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> SendDocumentBa(int documentID)
        {
            var result = new AjaxViewModel();
            try
            {
                var data = _docProvider.GetDataRequest(documentID);
                if (data != null)
                {
                    string FtpAddress = provider.GetGlobalConfigurationByKeyName("FtpAddress");
                    string ftpUsername = provider.GetGlobalConfigurationByKeyName("ftpUsername");
                    string ftpPassword = provider.GetGlobalConfigurationByKeyName("ftpPassword");
                    foreach (var item in data.list_pdf)
                    {
                        item.file = string.Format("{0}{1}", "data:application/pdf;base64,", FTPHelper.GetStringBase64(item.file, ftpUsername, ftpPassword, FtpAddress));
                    }
                }
                var token = await Tilaka.GetTilakaToken();
                data.request_id = await Tilaka.GenerateUUID(token);
                string json = JsonConvert.SerializeObject(data);
                var responeTilaka = RestAPIHelper<ResponeRequestSign>.Submit(json, Method.POST, Tilaka.signing_requestsign, token);

                if (responeTilaka.success)
                {
                    provider.UpdateRequestID(documentID, data.request_id);
                    //provider.SendDocument(documentID);
                }

                result.SetValues(responeTilaka.success, responeTilaka.auth_urls != null ? responeTilaka.auth_urls[0].url : null, responeTilaka.message);
            }
            catch (Exception ex)
            {
                result.SetValues(false, null, ex.Message);
            }
            return Ok(result);
        }

        [HttpGet("[action]")]
        public IActionResult GetWebView(int docId, string currentSigner)
        { 
            var data = provider.UrlWebViewFailed(docId, currentSigner);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult GetById(int id)
        {

            string FtpAddress = provider.GetGlobalConfigurationByKeyName("FtpAddress");
            string ftpUsername = provider.GetGlobalConfigurationByKeyName("ftpUsername");
            string ftpPassword = provider.GetGlobalConfigurationByKeyName("ftpPassword");

            var data = provider.GetById(id);
            data.DocumentFile = FTPHelper.GetStringBase64(data.DocumentFile, ftpUsername, ftpPassword, FtpAddress);
            //data.DocumentFile = string.Format("{0}{1}", "data:application/pdf;base64,", FTPHelper.GetStringBase64(data.DocumentFile, ftpUsername, ftpPassword, FtpAddress));
            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult Save(List<MasterDocumentViewModel> model, string displayName)
        {
            _logger.LogInformation("Object: {@requestBody}", model);
            //_logger.LogInformation("String: {@requestBody}", displayName);
            try
            {
                foreach (var item in model)
                {
                    var date = DateTime.Now;
					//tahun/month/tanggal/digitalSign/{documentCategory}/{filename_uniqueID}
					//add ftp config by reza
					string guid = Guid.NewGuid().ToString();
                    string extension = Path.GetExtension(item.DocumentName);
                    //string fileName = $"{guid}{extension}";
                    string fileName = $"{item.DocumentName}_{date.ToString("hhmmfff")}{extension}";
                    string filePath = $"{date.ToString("yyyy/MMMM/dd")}/DigitalSign/{item.CategoryName}";
                    string FtpAddress = provider.GetGlobalConfigurationByKeyName("FtpAddress");
                    string ftpUsername = provider.GetGlobalConfigurationByKeyName("ftpUsername");
                    string ftpPassword = provider.GetGlobalConfigurationByKeyName("ftpPassword");
                    FTPHelper.CreateFolder(filePath, ftpUsername, ftpPassword, FtpAddress);
                    filePath = $"{filePath}/{fileName}";
                    FTPHelper.UploadFile(Convert.FromBase64String(item.DocumentFile), filePath, ftpUsername, ftpPassword, FtpAddress);
                    item.DocumentFile = filePath;
                }

                var data = provider.Save(model, displayName);
                _logger.LogInformation("Object: {@respondBody}", data);
                return Ok(data);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Save");
                //throw;
                return BadRequest(e.Message);
            }
        }
    }
}
