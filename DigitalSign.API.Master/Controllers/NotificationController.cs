﻿using DigitalSign.Provider.Master;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Master.Notification;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DigitalSign.API.Master.Controllers
{
    [Authorize]
    [Route("api/master/[controller]")]
    [ApiController]
    public class NotificationController : Controller
    {
        private readonly INotificationProvider provider;
        private readonly ILogger<NotificationController> logger;
        public NotificationController(INotificationProvider provider, ILogger<NotificationController> logger)
        {
            this.provider = provider;
            this.logger = logger;
        }

        [HttpGet("[action]")]
        public IActionResult GetNotif(int userID)
        {
            var data = provider.ShowNotification(userID);
            return Ok(data);
        }

        [HttpPost("[action]")]
        public IActionResult AddNotification(NotificationViewModel model)
        {
            var data = provider.CreateNotif(model);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult ReadNotification(int notifID, int userID)
        {
            var data = provider.ReadNotification(notifID, userID);
            return Ok(data);
        }

        [HttpGet("[action]")]
        public IActionResult DeleteNotification(int notifID, string username)
        {
            var data = provider.DeleteNotification(notifID, username);
            return Ok(data);
        }

        [HttpPost("[action]")]
        [AllowAnonymous]
        public IActionResult WebHook([FromBody] CallbackSigningViewModel viewModel)
        {
            try
            {
                logger.LogInformation("Webhook Request: {@requestBody}", viewModel);
                var data = provider.WebHookProcess(viewModel);
                logger.LogInformation("Webhook Response: {@responseBody}", data);
                return Ok(data);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                throw ex;
            }
        }
    }
}
