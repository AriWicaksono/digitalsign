﻿using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Role;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using Route = DigitalSign.Utilities.Route;

namespace DigitalSign.Web.Controllers
{
    public class RoleController : Controller
    {
        string url = $"{ApiUrl.Master}MasterRole";
        string Message = String.Empty;

        public IActionResult Index()
        {
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;
                return View();
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        [HttpPost]
        public JsonResult RoleList([DataSourceRequest] DataSourceRequest request)
        {
            var endpoint = url + Route.GetAll;
            var viewModel = new List<MasterRoleViewModel>();
            viewModel = RestAPIHelper<List<MasterRoleViewModel>>.Submit("", Method.GET, endpoint, Request);
            var filter = viewModel.ToDataSourceResult(request);
            return Json(filter);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new MasterRoleViewModel();
            return PartialView("CreateEdit", model);
        }

        [HttpPost]
        public ActionResult Create(MasterRoleViewModel model)
        {
            var jsonViewModel = new AjaxViewModel();
            if (ModelState.IsValid)
            {
                try
                {
                    model.CreatedBy = ClaimHelper.UserName;
                    string json = JsonConvert.SerializeObject(model);
                    //var endpoint = String.Format("{0}{1}", url, Route.Validate);

                    var endpoint = String.Format("{0}{1}", url, Route.Save);
                    var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request, ref Message);
                    if (content != null)
                    {
                        jsonViewModel.SetValues(true, null, "Saved");
                    }
                    else
                    {
                        jsonViewModel.SetValues(false, null, "Save Failed");
                    }
                }
                catch (Exception)
                {
                    jsonViewModel.SetValues(false, null, "Failed Message : " + Message);
                }
                return Json(jsonViewModel);
            }
            var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
            jsonViewModel.SetValues(false, validationMessages, "");
            return Json(jsonViewModel);

        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var jsonViewModel = new AjaxViewModel();
            MasterRoleViewModel model = new MasterRoleViewModel();
            try
            {
                var endpoint = String.Format("{0}{1}?roleID={2}", url, Route.GetByID, id);
                model = Utilities.RestAPIHelper<MasterRoleViewModel>.Submit("", Method.GET, endpoint, Request, ref Message);
            }
            catch (Exception)
            {
                jsonViewModel.SetValues(false, null, "Failed Message : " + Message);
            }
            return PartialView("CreateEdit", model);
        }

        [HttpPost]
        public ActionResult Edit(MasterRoleViewModel model)
        {
            var jsonViewModel = new AjaxViewModel();
            if (ModelState.IsValid)
            {
                try
                {
                    model.CreatedBy = ClaimHelper.UserName;
                    model.Description = string.IsNullOrEmpty(model.Description) ? null : model.Description;
                    string json = JsonConvert.SerializeObject(model);
                    //var endpoint = String.Format("{0}{1}", url, Route.Validate);
                    var endpoint = url + Route.Save;
                    var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request, ref Message);
                    if (content != null)
                    {
                        jsonViewModel.SetValues(true, null, "Saved");
                    }
                    else
                    {
                        jsonViewModel.SetValues(false, null, "Update Failed");
                    }
                }
                catch (Exception)
                {
                    jsonViewModel.SetValues(false, null, "Failed Message : " + Message);
                }
                return Json(jsonViewModel);
            }
            var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
            jsonViewModel.SetValues(false, validationMessages, "");
            return Json(jsonViewModel);
        }

        public ActionResult Delete(int id)
        {
            var ajaxViewModel = new AjaxViewModel();
            try
            {
                string json = JsonConvert.SerializeObject(id);
                var endpoint = String.Format("{0}{1}?roleID={2}", url, Route.Delete, id);
                var delete = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.POST, endpoint, Request, ref Message);
                ajaxViewModel.SetValues(delete.isSucceed, null, delete.Message);
            }
            catch (Exception)
            {
                ajaxViewModel.SetValues(false, null, "Failed Message : " + Message);
            }
            return Json(ajaxViewModel);
        }
    }
}
