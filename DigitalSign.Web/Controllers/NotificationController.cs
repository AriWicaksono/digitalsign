﻿using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace DigitalSign.Web.Controllers
{
	public class NotificationController : Controller
	{
		string url = $"{ApiUrl.Master}Notification/";

		public IActionResult GetAll()
		{
			if (ClaimHelper.CheckSession)
			{
				ViewBag.Username = ClaimHelper.UserName;
				ViewBag.Rolename = ClaimHelper.RoleName;
				var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
				ViewBag.Notification = notification;
				int counter = notification.Where(a => a.ReadDate == null).Count();
				ViewBag.HitungCounter = counter;
				return View("NotificationAll", notification);
			}
			else
			{
				return RedirectToAction("SignIn", "Auth");
			}
		}

		public async Task<JsonResult> ReadNotification(int notifID)
		{
			int userID = ClaimHelper.LoginID;
			var jsonViewModel = new AjaxViewModel();
			var endpoint = url + $"ReadNotification?notifID={notifID}&userID={userID}";
			var result = Utilities.RestAPIHelper<ProcessResult>.Submit("", Method.GET, endpoint, Request);
			jsonViewModel.SetValues(result.isSucceed, result.value, result.message);
			return Json(jsonViewModel);
		}

		public async Task<JsonResult> DeleteNotification(int notifID)
		{
			string username = ClaimHelper.UserName;
			var jsonViewModel = new AjaxViewModel();
			var endpoint = url + $"DeleteNotification?notifID={notifID}&username={username}";
			var result = Utilities.RestAPIHelper<ProcessResult>.Submit("", Method.GET, endpoint, Request);
			jsonViewModel.SetValues(result.isSucceed, result.value, result.message);
			return Json(jsonViewModel);
		}

		public IActionResult WebHook(string request_id, bool success, string signer, string? signer_next, string? url)
		{
			var webHook = new CallbackSigningViewModel()
			{
				request_id = request_id,
				success	= success,
				signer= signer,
				signer_next = signer_next,
				url = url
			};
			var json = JsonConvert.SerializeObject(webHook);
			var endpoint = $"{ApiUrl.Master}Notification/WebHook";
			var result = Utilities.RestAPIHelper<ProcessResult>.Submit(json, Method.POST, endpoint);
			return Ok(result);
		}

	}
}
