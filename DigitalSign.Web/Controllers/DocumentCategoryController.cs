﻿using DigitalSign.Utilities;
using DigitalSign.ViewModels.Master.Role;
using DigitalSign.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using System.Security.Policy;
using DigitalSign.ViewModels.Master.Document;

namespace DigitalSign.Web.Controllers
{
    public class DocumentCategoryController : Controller
    {
        string _url = $"{ApiUrl.Master}DocumentCategory";
        public IActionResult Index()
        {
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;
                return View();
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        [HttpPost]
        public JsonResult GetList([DataSourceRequest] DataSourceRequest request)
        {
            var endpoint = $"{_url}/GetAll";
            var viewModel = RestAPIHelper<List<DocumentCategoryViewModel>>.Submit("", Method.GET, endpoint, Request);
            var filter = viewModel.ToDataSourceResult(request);
            return Json(filter);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = new DocumentCategoryViewModel();
            return PartialView("CreateEdit", model);
        }

        [HttpPost]
        public ActionResult CreateEdit(DocumentCategoryViewModel model)
        {
            var jsonViewModel = new AjaxViewModel();
            if (ModelState.IsValid)
            {
                try
                {
                    var currentUser = ClaimHelper.UserName;
                    string json = JsonConvert.SerializeObject(model);

                    var endpoint = $"{_url}/AddEditDocumentCategory?currentUser={currentUser}";
                    var content = Utilities.RestAPIHelper<ProcessResult>.Submit(json, Method.POST, endpoint, Request);
                    if (content != null)
                    {
                        jsonViewModel.SetValues(content.isSucceed, null, content.message);
                    }
                    else
                    {
                        jsonViewModel.SetValues(false, null, "Save Failed");
                    }
                }
                catch (Exception ex)
                {
                    jsonViewModel.SetValues(false, null, "Failed Message : " + ex.Message);
                }
                return Json(jsonViewModel);
            }
            var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
            jsonViewModel.SetValues(false, validationMessages, "");
            return Json(jsonViewModel);

        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var jsonViewModel = new AjaxViewModel();
            var model = new DocumentCategoryViewModel();
            try
            {
                var endpoint = $"{_url}/GetById?id={id}";
                model = Utilities.RestAPIHelper<DocumentCategoryViewModel>.Submit("", Method.GET, endpoint, Request);
            }
            catch (Exception ex)
            {
                jsonViewModel.SetValues(false, null, "Failed Message : " + ex.Message);
            }
            return PartialView("CreateEdit", model);
        }

        public ActionResult Delete(int id)
        {
            var ajaxViewModel = new AjaxViewModel();
            try
            {
                string json = JsonConvert.SerializeObject(id);
                var endpoint = $"{_url}/Delete?id={id}&currentUser={ClaimHelper.UserName}";
                var delete = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.DELETE, endpoint, Request);
                ajaxViewModel.SetValues(delete.isSucceed, null, delete.Message);
            }
            catch (Exception ex)
            {
                ajaxViewModel.SetValues(false, null, "Failed Message : " + ex.Message);
            }
            return Json(ajaxViewModel);
        }
    }
}
