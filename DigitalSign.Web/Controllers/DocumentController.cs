﻿using DigitalSign.DataAccess.Models;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Master.RoleMenu;
using DigitalSign.ViewModels.Master.User;
using DigitalSign.ViewModels.Security;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Security.Policy;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;
using Route = DigitalSign.Utilities.Route;

namespace DigitalSign.Web.Controllers
{
    public class DocumentController : Controller
    {
        string url = $"{ApiUrl.Master}MasterDocument";
        private readonly ILogger _logger;

        public DocumentController(ILogger<BerandaController> logger)
        {
            _logger = logger;
        }
        public IActionResult Index(string keyword = null)
        {
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;

                var endpointUser = String.Format("{0}{1}?id={2}", $"{ApiUrl.Master}MasterUser", Route.GetByID, ClaimHelper.LoginID);
                var dataUser = Utilities.RestAPIHelper<MasterUserViewModel>.Submit("", Method.GET, endpointUser, Request);
                ViewBag.Status = dataUser.Status;
                ViewBag.Signature = dataUser.Signature;
                ViewBag.Keyword = keyword;
                return View();
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        public IActionResult MateraiPlacement(int docId, string keyword = null)
        {
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;

                var endpointUser = String.Format("{0}{1}?id={2}", $"{ApiUrl.Master}MasterUser", Route.GetByID, ClaimHelper.LoginID);
                var dataUser = Utilities.RestAPIHelper<MasterUserViewModel>.Submit("", Method.GET, endpointUser, Request);
                ViewBag.Status = dataUser.Status;
                ViewBag.Signature = dataUser.Signature;
                ViewBag.Keyword = keyword;

                var endpoint = url + $"/GetDocumentForMaterai?docId={docId}";
                var viewModel = RestAPIHelper<DocumentMateraiViewModel>.Submit("", Method.GET, endpoint, Request);

                return View("MateraiPlacement", viewModel);
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }


        [HttpPost]
        public JsonResult List([DataSourceRequest] DataSourceRequest request, string keyword = null)
        {
            string endpoint = String.Empty;
            if (keyword.HasValue())
            {
                endpoint = url + "/Search" + "?roleName=" + ClaimHelper.RoleName + "&userId=" + ClaimHelper.LoginID + "&keyword=" + keyword;
            }
            else
            {
                endpoint = url + Route.GetAll + "?role=" + ClaimHelper.RoleName + "&userID=" + ClaimHelper.LoginID;
            }

            var viewModel = RestAPIHelper<List<MasterDocumentViewModel>>.Submit("", Method.GET, endpoint, Request);
            var filter = viewModel.ToDataSourceResult(request);
            return Json(filter);
        }

        public IActionResult ListDocumentCategory() {
			var endpoint = $"{ApiUrl.Master}DocumentCategory/GetAll";
			var viewModel = RestAPIHelper<List<DocumentCategoryViewModel>>.Submit("", Method.GET, endpoint, Request);
            return Json(viewModel);
		}

        [HttpGet]
        public ActionResult Detail(int id)
        {
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                ViewBag.UserID = ClaimHelper.LoginID;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;

                var endpoint = url + Route.GetByID + $"?id={id}";
                var viewModel = RestAPIHelper<MasterDocumentViewModel>.Submit("", Method.GET, endpoint, Request);

                return View("Detail", viewModel);
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        [HttpGet]
        public ActionResult Upload()
        {
            var model = new MasterDocumentViewModel();
            model.ListIdUser = (List<DropdownList>)GetIdUserAll();
            return PartialView("Upload", model);
        }

        //[HttpPost]
        //public JsonResult UploadFile(DocumentUploadViewModel viewModel)
        //{
        //	var result = new AjaxViewModel();
        //	if (ModelState.IsValid)
        //	{
        //		var listParticipant = JsonConvert.DeserializeObject<List<DocumentParticipantViewModel>>(viewModel.ListParticipant);
        //		var listHistory = new List<DocumentHistoryViewModel>();
        //		var docHistory = new DocumentHistoryViewModel();
        //		docHistory.UserId = Utilities.ClaimHelper.LoginID;
        //		docHistory.Activity = "Created";
        //		docHistory.TimeStamp = DateTime.Now;
        //		listHistory.Add(docHistory);

        //		var endpoint = $"{ApiUrl.Master}MasterDocument" + Route.Save + "?displayName=" + Utilities.ClaimHelper.UserName;
        //		var docModel = new List<MasterDocumentViewModel>();

        //		for (var i = 0; i < viewModel.DocumentFileNew.Count(); i++)
        //		{
        //			docModel.Add(new MasterDocumentViewModel
        //			{
        //				Title = viewModel.Title,
        //				DocumentName = viewModel.DocumentFileNew[i].FileName,
        //				DocumentFile = Utilities.Base64Helper.ConvertToBase64(viewModel.DocumentFileNew[i]),
        //				Status = null,
        //				DocumentParticipant = listParticipant.Where(x => x.DocumentID == i).ToList(),
        //				DocumentHistory = listHistory,
        //			});

        //		}

        //		var json = JsonConvert.SerializeObject(docModel);

        //		var response = RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
        //		if (response != null)
        //		{
        //			if (response.isSucceed)
        //			{
        //				result.SetValues(true, null, response.Message);
        //			}
        //			else
        //			{
        //				result.SetValues(true, new { id = 1 }, response.Message);
        //			}
        //		}
        //		else
        //		{
        //			result.SetValues(true, new { id = 1 }, "Something when wrong");
        //		}
        //		return Json(result);
        //	}
        //	var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
        //	result.SetValues(false, validationMessages, "");
        //	return Json(result);
        //}

        [HttpPost]
        public JsonResult UploadFile(DocumentUploadViewModel viewModel)
        {
            var result = new AjaxViewModel();
            if (ModelState.IsValid)
            {
                var listParticipant = JsonConvert.DeserializeObject<List<DocumentParticipantViewModel>>(viewModel.ListParticipant);
                var listHistory = new List<DocumentHistoryViewModel>();
                var docHistory = new DocumentHistoryViewModel();
                docHistory.UserId = Utilities.ClaimHelper.LoginID;
                docHistory.Activity = "Created";
                docHistory.TimeStamp = DateTime.Now;
                listHistory.Add(docHistory);

                var endpoint = $"{ApiUrl.Master}MasterDocument" + Route.Save + "?displayName=" + Utilities.ClaimHelper.UserName;

                //add ftp config by reza
                //string guid = Guid.NewGuid().ToString();
                //string extension = Path.GetExtension(viewModel.DocumentFile.FileName);
                //byte[] filesContents = FTPHelper.GetFileContentsByIFormFile(viewModel.DocumentFile);
                //string fileName = $"{guid}{extension}";
                //string filePath = Path.Combine("DigitalSign", fileName);
                //FTPHelper.UploadFile(filesContents, filePath, "k2admin", "Indocyber.100", "ftp://34.101.119.19/");

                //var docModel = new MasterDocumentViewModel();
                //docModel.Title = viewModel.Title;
                //docModel.DocumentName = fileName;
                ////docModel.DocumentFile = Utilities.Base64Helper.ConvertToBase64(viewModel.DocumentFile);
                //docModel.DocumentFile = filePath;
                //docModel.Status = null;
                //docModel.DocumentParticipant = listParticipant;
                //docModel.DocumentHistory = listHistory;
                var docModel = new List<MasterDocumentViewModel>();

                for (var i = 0; i < viewModel.DocumentFileNew.Count(); i++)
                {
                    byte[] filesContents = FTPHelper.GetFileContentsByIFormFile(viewModel.DocumentFileNew[i]);
                    docModel.Add(new MasterDocumentViewModel
                    {
                        Title = viewModel.Title,
                        DocumentName = viewModel.DocumentFileNew[i].FileName,
                        DocumentFile = Convert.ToBase64String(filesContents),
                        Status = null,
                        DocumentParticipant = listParticipant.Where(x => x.DocumentID == i).ToList(),
                        DocumentHistory = listHistory,
                        Category = viewModel.Category,
                        CategoryName = viewModel.CategoryName,
                    });
                }

                var json = JsonConvert.SerializeObject(docModel);

                var response = RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
                if (response != null)
                {
                    if (response.isSucceed)
                    {
                        result.SetValues(true, null, response.Message);
                    }
                    else
                    {
                        result.SetValues(true, new { id = 1 }, response.Message);
                    }
                }
                else
                {
                    result.SetValues(true, new { id = 1 }, "Something went wrong");
                }
                return Json(result);
            }
            var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
            result.SetValues(false, validationMessages, "");
            return Json(result);
        }

        public IEnumerable<DropdownList> GetIdUserAll()
        {
            var endpoint = $"{ApiUrl.Master}MasterUser/GetForDropdownList";
            var viewModel = RestAPIHelper<List<MasterUserViewModel>>.Submit("", Method.GET, endpoint, Request);
            List<DropdownList> list = new List<DropdownList>();

            foreach (var item in viewModel)
            {
                list.Add(new DropdownList
                {
                    Text = item.Fullname + " - " + item.Email,
                    Value = item.ID.ToString()
                });
            }

            return list.ToList();
        }

        [HttpGet]
        public ActionResult SignConfig(int idUser, int idDoc)
        {
            ViewBag.idUser = idUser;
            ViewBag.idDoc = idDoc;
            return PartialView("SignConfig");
        }

        [HttpGet]
        public ActionResult SignaturePlacement(int documentId)
        {
            var endpoint = url + Route.GetByID + $"?id={documentId}";
            var viewModel = RestAPIHelper<MasterDocumentViewModel>.Submit("", Method.GET, endpoint, Request);
            ViewBag.DocumentFile = viewModel.DocumentFile;
            return PartialView("SignaturePlacement");
        }

        [HttpPost]
        public JsonResult ListSignConfig([DataSourceRequest] DataSourceRequest request, int DocId, int UserId)
        {
            var endpoint = $"{ApiUrl.Master}DocumentSign/GetData?DocId=" + DocId + "&UserId=" + UserId;
            var viewModel = RestAPIHelper<List<DocumentSignViewModel>>.Submit("", Method.GET, endpoint, Request);
            var filter = viewModel.ToDataSourceResult(request);
            return Json(filter);
        }

        [HttpPost]
        public IActionResult AddDocumentSign(DocumentSignViewModel model)
        {
            var result = new AjaxViewModel();
            string Message = String.Empty;
            if (ModelState.IsValid)
            {
                try
                {
                    string json = JsonConvert.SerializeObject(model);
                    var endpoint = $"{ApiUrl.Master}DocumentSign/AddDocumentSign";
                    var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request, ref Message);
                    result.SetValues(content.isSucceed, null, content.Message);
                }
                catch (Exception ex)
                {
                    result.SetValues(false, null, "Failed Message : " + ex.Message);
                }
            }
            else
            {
                var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
                result.SetValues(false, validationMessages, "Failed To Add Signature Participant");
            }

            return Json(result);
        }

        [HttpPost]
        public IActionResult DeleteDocumentSign(int id)
        {
            var result = new AjaxViewModel();
            string Message = String.Empty;

            try
            {
                var endpoint = $"{ApiUrl.Master}DocumentSign/DeleteDocumentSign?id=" + id;
                var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, endpoint, Request, ref Message);
                result.SetValues(content.isSucceed, null, content.Message);
            }
            catch (Exception ex)
            {
                result.SetValues(false, null, "Failed Message : " + ex.Message);
            }

            return Json(result);
        }

        [HttpPost]
        public IActionResult RequestSign(int idDoc)
        {
            var result = new AjaxViewModel();
            try
            {
                var endpoint = $"{ApiUrl.Master}DocumentSign/RequestSign?idDoc=" + idDoc + "&currentUser=" + ClaimHelper.UserName;
                result = RestAPIHelper<AjaxViewModel>.Submit("", Method.POST, endpoint, Request);
            }
            catch (Exception ex)
            {
                result.SetValues(false, null, ex.Message);
            }
            return Json(result);
        }

        [HttpPost]
        public async Task<IActionResult> RequestSignAsyncLama(int idDoc)
        {
            var result = new AjaxViewModel();

            /*
            Logic yang akan digunakan setelah log di pasang di project ini
            jika sequence current user != 1
            get prevUser
            maka tambah logic
	            jika prevUser status sign nya not none
	            maka baca url dari log berdasarkan request_id yang tersimpan di tabel document dengan idDocument yang mau di ttd
	
	            jika prevUser status sign nya none
	            maka keluarkan error message bahwa document harus di ttd secara berurutan
	
            jika sequence current user == 1
            maka tambah logic
	            jika request_id di tabel document masih null
	            maka lakukan request ke tilaka, update request_id di tabel document jika request berhasil
	
	            jika request_id di tabel document tidak null
	            maka baca log berdasarkan request_id yang tersimpan di tabel document dengan idDocument yang mau di ttd 

             
             */

            try
            {
                // get participant document
                var endpointParticipant = $"{ApiUrl.Master}MasterDocument/GetAllParticipant?DocumentID=" + idDoc;
                var resultParticipant = Utilities.RestAPIHelper<List<DocumentParticipantViewModel>>.Submit("", Method.GET, endpointParticipant, Request);
                var sequenceCurrentUser = resultParticipant.SingleOrDefault(x => x.UserID == ClaimHelper.LoginID).Sequence;
                // end get participant document

                if (sequenceCurrentUser > 1)
                {
                    var prevUser = resultParticipant.SingleOrDefault(x => x.Sequence == sequenceCurrentUser - 1);
                    //var statusPrevUser = prevUser.Status;
                    if (prevUser.Status != "none")
                    {

                        var endpointNext = $"{ApiUrl.Master}MasterDocument/GetNextParticipantUrl?docId=" + idDoc + "&lastSigner=" + prevUser.Username + "&nextSigner=" + ClaimHelper.UserName;
                        var resultNext = Utilities.RestAPIHelper<WebHook>.Submit("", Method.GET, endpointNext, Request);
                        if (resultNext != null)
                        {
                            var endpointUpdateRequestID = $"{ApiUrl.Master}MasterDocument/UpdateRequestIDNew?documentID=" + idDoc + "&requestID=" + resultNext.request_id + "&userSign=" + ClaimHelper.UserName + "&url=" + resultNext.url;
                            var resultUpdateRequestID = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, endpointUpdateRequestID, Request);
                            result.SetValues(true, resultNext.url, "success");
                        }
                        else
                        {
                            result.SetValues(false, null, "Next Signer not found");
                        }
                    }
                    else
                    {
                        result.SetValues(false, null, "Document harus di tanda tangani secara berurutan, mohon menunggu giliran anda...");
                    }
                }
                else
                {
                    var endpoint = $"{ApiUrl.Master}DocumentSign/GetDataRequest?DocId=" + idDoc;
                    var content = Utilities.RestAPIHelper<RequestSignViewModel>.Submit("", Method.GET, endpoint, Request);
                    var token = await Tilaka.GetTilakaToken();

                    if (String.IsNullOrEmpty(content.request_id))
                    {
                        content.request_id = await Tilaka.GenerateUUID(token);
                        string json = JsonConvert.SerializeObject(content);
                        var responeTilaka = RestAPIHelper<ResponeRequestSign>.Submit(json, Method.POST, Tilaka.signing_requestsign, token);

                        if (responeTilaka.success)
                        {
                            //var endpointUpdateRequestID = $"{ApiUrl.Master}MasterDocument/UpdateRequestID?DocumentID=" + idDoc + "&RequestID=" + content.request_id;
                            //var resultUpdateRequestID = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, endpointUpdateRequestID, Request);
                            var endpointUpdateRequestID = $"{ApiUrl.Master}MasterDocument/UpdateRequestIDNew?documentID=" + idDoc + "&requestID=" + content.request_id + "&userSign=" + responeTilaka.auth_urls[0].user_identifier + "&url=" + responeTilaka.auth_urls[0].url;
                            var resultUpdateRequestID = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, endpointUpdateRequestID, Request);
                        }

                        result.SetValues(responeTilaka.success, responeTilaka.auth_urls != null ? responeTilaka.auth_urls[0].url : null, responeTilaka.message);
                    }
                    else
                    {
                        /*
                        baca log,
                        sesuai request_id nya, ambil data url nya
                        result.SetValues(true, {data dari log}.auth_urls[0].url, "");

                        **untuk sementara disamakan dulu dengan yang atas**
                        
                         */
                        var endpointUrl = $"{ApiUrl.Master}MasterDocument/GetWebView?docId=" + idDoc + "&currentSigner=" + ClaimHelper.UserName;
                        var getUrlExist = Utilities.RestAPIHelper<string>.Submit("", Method.GET, endpointUrl, Request);
                        //content.request_id = await Tilaka.GenerateUUID(token);
                        //string json = JsonConvert.SerializeObject(content);
                        //var responeTilaka = RestAPIHelper<ResponeRequestSign>.Submit(json, Method.POST, Tilaka.signing_requestsign, token);

                        //if (responeTilaka.success)
                        //{
                        //                      //var endpointUpdateRequestID = $"{ApiUrl.Master}MasterDocument/UpdateRequestID?DocumentID=" + idDoc + "&RequestID=" + content.request_id;
                        //                      //var resultUpdateRequestID = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, endpointUpdateRequestID, Request);
                        //                      var endpointUpdateRequestID = $"{ApiUrl.Master}MasterDocument/UpdateRequestIDNew?documentID=" + idDoc + "&requestID=" + content.request_id + "&userSign=" + responeTilaka.auth_urls[0].user_identifier + "&url=" + responeTilaka.auth_urls[0].url;
                        //                      var resultUpdateRequestID = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, endpointUpdateRequestID, Request);
                        //                  }
                        if (string.IsNullOrEmpty(getUrlExist))
                        {
                            result.SetValues(true, getUrlExist, "");
                        }
                        else
                        {
                            content.request_id = await Tilaka.GenerateUUID(token);
                            string json = JsonConvert.SerializeObject(content);
                            var responeTilaka = RestAPIHelper<ResponeRequestSign>.Submit(json, Method.POST, Tilaka.signing_requestsign, token);

                            if (responeTilaka.success)
                            {
                                //var endpointUpdateRequestID = $"{ApiUrl.Master}MasterDocument/UpdateRequestID?DocumentID=" + idDoc + "&RequestID=" + content.request_id;
                                //var resultUpdateRequestID = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, endpointUpdateRequestID, Request);
                                var endpointUpdateRequestID = $"{ApiUrl.Master}MasterDocument/UpdateRequestIDNew?documentID=" + idDoc + "&requestID=" + content.request_id + "&userSign=" + responeTilaka.auth_urls[0].user_identifier + "&url=" + responeTilaka.auth_urls[0].url;
                                var resultUpdateRequestID = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, endpointUpdateRequestID, Request);
                            }

                            result.SetValues(responeTilaka.success, responeTilaka.auth_urls != null ? responeTilaka.auth_urls[0].url : null, responeTilaka.message);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                result.SetValues(false, null, "Failed Message : " + e.Message);
            }

            return Json(result);
        }

        public IActionResult SignDocument(string url)
        {
            ViewBag.url = url;
            return PartialView("SignDocument");
        }

        public IActionResult VerifySignDocument()
        {
            _logger.LogInformation("About page hit /VerifLinkAccount at {DT}", DateTime.Now.ToLongTimeString());
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;

                return View("VerifySignDocument");
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        public async Task<IActionResult> UpdateStatus(string request_id, string tilaka_name, string status)
        {
            _logger.LogInformation("About page hit /UpdateStatus at {DT}", DateTime.Now.ToLongTimeString());
            var jsonViewModel = new AjaxViewModel();
            if (request_id == null || tilaka_name == null)
            {
                _logger.LogError(LogEvents.GetItemNotFound, $"Request Id = ({request_id}) & Tilaka Name = ({tilaka_name}) & Status = ({status})");
                jsonViewModel.SetValues(false, null, "Wrong URL");
                return Json(jsonViewModel);
            }

            var urldataDoc = $"{ApiUrl.Master}MasterDocument/GetByRequestID?RequestID={request_id}";
            var dataDoc = Utilities.RestAPIHelper<MasterDocumentViewModel>.Submit("", Method.GET, urldataDoc, Request);

            var urlUpdateStatusSign = $"{ApiUrl.Master}MasterDocument/UpdateStatusSignDocument?DocumentID=" + dataDoc.ID + "&UserID=" + ClaimHelper.LoginID + "&Status=" + status;
            var result = Utilities.RestAPIHelper<ProcessResult>.Submit("", Method.GET, urlUpdateStatusSign, Request);
            jsonViewModel.SetValues(result.isSucceed, result.value, result.message);
            if (jsonViewModel.isSucceed)
            {
                _logger.LogInformation(LogEvents.UpdateItem, $"Succes Update Request Id = ({request_id}) & Tilaka Name = ({tilaka_name}) & Status = ({status})");
            }
            else
            {
                _logger.LogError(LogEvents.UpdateItem, $"Failed Update Request Id = ({request_id}) & Tilaka Name = ({tilaka_name}) & Status = ({status}), message = {result.message}");
            }
            return Json(jsonViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> GetDocument(string request_id)
        {
            var PDFFile = await Tilaka.DownloadSignedPDF(request_id);
            var Base64 = PDFFile.signed_pdf[0].pdf;
            var filename = PDFFile.signed_pdf[0].pdf_name;
            return File(Convert.FromBase64String(Base64), "application/pdf", filename.Split('.')[0] + " (signed).pdf");
        }

        [HttpGet]
        public IActionResult SendDocument(int documentId)
        {
            var result = new AjaxViewModel();

            var urlSendDocument = $"{ApiUrl.Master}MasterDocument/SendDocument?documentID={documentId}";
            var resultSend = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, urlSendDocument, Request);
            result.SetValues(resultSend.isSucceed, "", resultSend.Message);
            return Json(result);
        }

        public async Task<IActionResult> GetListDocumentType()
        {
            try
            {
                var token = await Tilaka.GetTilakaToken();
                var responseTilaka = RestAPIHelper<ResponeDocumentType>.Submit("", Method.GET, Tilaka.document_type, token);
                return Json(responseTilaka.data);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error on method GetListDocumentType " + ex.Message);
                throw;
            }
        }

        [HttpPost]
        public IActionResult SubmitMaterai(DocumentMateraiViewModel viewModel)
        {
            var result = new AjaxViewModel();
            try
            {
                viewModel.UserLogin = ClaimHelper.UserName;
                string Message = string.Empty;
                string json = JsonConvert.SerializeObject(viewModel);
                var endpoint = $"{ApiUrl.Security}SubmitMaterai";
                var content = Utilities.RestAPIHelper<ProcessResult>.Submit(json, Method.POST, endpoint, Request, ref Message);
                result.SetValues(content.isSucceed, null, content.message);
            }
            catch (Exception ex)
            {
                result.SetValues(false, null, ex.Message);
                //throw;
            }

            return Json(result);
        }
    }
}
