﻿using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Role;
using DigitalSign.ViewModels.Master.User;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using Route = DigitalSign.Utilities.Route;
using DropDownList = DigitalSign.ViewModels.Master.DropDownList;
using Microsoft.AspNetCore.Authorization;
using DigitalSign.ViewModels.Security;
using System;

namespace DigitalSign.Web.Controllers
{
    public class UserController : Controller
    {
        string url = $"{ApiUrl.Master}MasterUser";
        string Message = String.Empty;
        public IActionResult Index()
        {
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;
                return View();
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        [HttpPost]
        public JsonResult UserList([DataSourceRequest] DataSourceRequest request)
        {
            var endpoint = url + Route.GetAll;
            var viewModel = new List<MasterUserViewModel>();
            viewModel = RestAPIHelper<List<MasterUserViewModel>>.Submit("", Method.GET, endpoint, Request);
            var filter = viewModel.ToDataSourceResult(request);
            return Json(filter);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var jsonViewModel = new AjaxViewModel();
            MasterUserViewModel model = new MasterUserViewModel();
            try
            {
                var endpoint = String.Format("{0}{1}?id={2}", url, Route.GetByID, id);
                model = Utilities.RestAPIHelper<MasterUserViewModel>.Submit("", Method.GET, endpoint, Request, ref Message);
                model.ListRole = GetRole();
            }
            catch (Exception)
            {
                jsonViewModel.SetValues(false, null, "Failed Message : " + Message);
                return Json(jsonViewModel);
            }
            return PartialView("CreateEdit", model);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;
                var jsonViewModel = new AjaxViewModel();
                RegisterAdminViewModel model = new RegisterAdminViewModel();
                try
                {
                    //model.ListRole = GetRoleAdmin();
                }
                catch (Exception ex)
                {
                    jsonViewModel.SetValues(false, null, "Failed Message : " + ex.Message);
                    return Json(jsonViewModel);
                }
                return View("CreateAdmin", model);
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        [HttpPost]
        public ActionResult Edit(MasterUserViewModel model)
        {
            var result = new AjaxViewModel();
            try
            {
                string json = JsonConvert.SerializeObject(model);
                var endpoint = url + Route.Save;
                var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request, ref Message);
                result.SetValues(content.isSucceed, null, content.Message);
            }
            catch (Exception)
            {
                result.SetValues(false, null, "Failed Message : " + Message);
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult Create(RegisterAdminViewModel viewModel)
        {
            var jsonViewModel = new AjaxViewModel();
            if (ModelState.IsValid)
            {
                try
                {
                    var register = new RegisterAdminInsertViewModel();
                    register.RoleID = viewModel.RoleID;
                    register.Username = viewModel.Username;
                    register.Password = viewModel.Password;
                    register.Email = viewModel.Email;
                    register.PhoneNumber = viewModel.PhoneNumber;
                    register.CompanyName = viewModel.CompanyName;
                    register.FirstName = viewModel.FirstName;
                    register.MidleName = viewModel.MidleName;
                    register.LastName = viewModel.LastName;
                    register.Code = viewModel.Code;
                    register.NIK = viewModel.NIK;
                    register.BirthPlace = viewModel.BirthPlace;
                    register.BirthDate = viewModel.BirthDate.Value;
                    register.Address = viewModel.Address;
                    register.KTPFile = Utilities.Base64Helper.ConvertToBase64(viewModel.KTPFile);
                    register.KTPName = viewModel.KTPFile.FileName;
                    register.PhotoFile = Utilities.Base64Helper.ConvertToBase64(viewModel.PhotoFile);
                    register.PhotoName = viewModel.PhotoFile.FileName;
                    var endpoint = url + "/RegisterAdmin?currentUser=" + ClaimHelper.UserName;
                    var json = JsonConvert.SerializeObject(register);

                    jsonViewModel = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
                    
                }
                catch (Exception e)
                {
                    jsonViewModel.SetValues(false, null, e.Message);
                    //throw;
                }
                return Json(jsonViewModel);
            }
            var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
            jsonViewModel.SetValues(false, validationMessages, "Terdapat error saat!");
            return Json(jsonViewModel);
        }

        [HttpPost]
        public ActionResult ResetSignature(int id)
        {
            var result = new AjaxViewModel();
            try
            {
                string json = JsonConvert.SerializeObject(id);
                var endpoint = url + "/ResetSignature?id=" + id;
                var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.GET, endpoint, Request, ref Message);
                result.SetValues(content.isSucceed, null, content.Message);
            }
            catch (Exception)
            {
                result.SetValues(false, null, "Failed Message : " + Message);
            }
            return Json(result);
        }

        public List<DropDownList> GetRole()
        {
            string urlMasterUser = $"{ApiUrl.Master}MasterRole";
            var endpoint = urlMasterUser + Utilities.Route.GetAll;
            var viewModel = new List<MasterRoleViewModel>();
            viewModel = RestAPIHelper<List<MasterRoleViewModel>>.Submit("", Method.GET, endpoint, Request);

            List<DropDownList> list = new List<DropDownList>();
            var listNew = viewModel.Where(x => x.Name != "" && x.IsActive);
            foreach (var item in listNew)
            {
                list.Add(new DropDownList
                {
                    Text = item.Name,
                    Value = item.ID.ToString()

                });
            }
            return list.ToList();
        }

        public IActionResult GetRoleAdmin()
        {
            string urlMasterUser = $"{ApiUrl.Master}MasterRole";
            var endpoint = urlMasterUser + "/GetAllRoleAdmin";
            var viewModel = RestAPIHelper<List<MasterRoleViewModel>>.Submit("", Method.GET, endpoint, Request);

            return Json(viewModel);
        }
    }
}
