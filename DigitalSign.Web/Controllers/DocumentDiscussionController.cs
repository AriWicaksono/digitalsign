﻿using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace DigitalSign.Web.Controllers
{
    public class DocumentDiscussionController : Controller
    {
        string url = $"{ApiUrl.Master}DocumentDiscussion";

        [HttpGet]
        public JsonResult GetDiscussion(int DocumentID)
        {
            var endpoint = url + "/GetDiscussion" + $"?DocumentID={DocumentID}";
            var viewModel = RestAPIHelper<List<DocumentDiscussionViewModel>>.Submit("", Method.GET, endpoint, Request);
            return Json(viewModel.OrderBy(m => m.TimeStamp));
        }

        [HttpPost]
        public JsonResult AddDiscussion(DocumentDiscussionViewModel model)
        {
            var result = new AjaxViewModel();
            try
            {
                model.UserID = ClaimHelper.LoginID;
                string json = JsonConvert.SerializeObject(model);
                var endpoint = $"{url}/AddDiscussion";
                var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request);
                if (content.isSucceed)
                {
                    var getDiscussion = GetDiscussion(model.DocumentID);
                    result.SetValues(content.isSucceed, getDiscussion, content.Message);
                }
            }
            catch (Exception ex)
            {
                result.SetValues(false, null, "Failed Message : " + ex.Message);
            }

            return Json(result);
        }
    }
}
