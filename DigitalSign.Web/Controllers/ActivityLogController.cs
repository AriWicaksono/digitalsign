﻿using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using RestSharp;

namespace DigitalSign.Web.Controllers
{
	public class ActivityLogController : Controller
	{
		string url = $"{ApiUrl.Master}ActivityLog";
		public IActionResult Index()
		{
			if (ClaimHelper.CheckSession)
			{
				ViewBag.Username = ClaimHelper.UserName;
				ViewBag.Rolename = ClaimHelper.RoleName;
				ViewBag.LoginID = ClaimHelper.LoginID;
				var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
				ViewBag.Notification = notification;
				int counter = notification.Where(a => a.ReadDate == null).Count();
				ViewBag.HitungCounter = counter;
				return View();
			}
			else
			{
				return RedirectToAction("SignIn", "Auth");
			}
		}

		[HttpPost]
		public JsonResult Read([DataSourceRequest] DataSourceRequest request)
		{
			var endpoint = url + "/GetActivity?UserID="+ClaimHelper.LoginID;
			var viewModel = RestAPIHelper<List<ActivityLogViewModel>>.Submit("", Method.GET, endpoint, Request);
			var filter = viewModel.ToDataSourceResult(request);
			return Json(filter);
		}
	}
}
