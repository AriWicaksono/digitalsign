﻿using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.User;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace DigitalSign.Web.Controllers
{
    public class ProfileController : Controller
    {
        string url = $"{ApiUrl.Master}MasterUser";
        string Message = String.Empty;

        public IActionResult Index()
        {
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                ViewBag.LoginID = ClaimHelper.LoginID;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;

                var jsonViewModel = new AjaxViewModel();
                var endpoint = String.Format("{0}{1}?id={2}", url, Utilities.Route.GetByID, ViewBag.LoginID);
                var model = Utilities.RestAPIHelper<MasterUserViewModel>.Submit("", Method.GET, endpoint, Request, ref Message);

                return View(model);
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        [HttpPost]
        public IActionResult UpdateAccountInfo(AccountInfoUserViewModel model)
        {
            var result = new AjaxViewModel();
            if (ModelState.IsValid)
            {
                try
                {
                    model.ID = ClaimHelper.LoginID;
					if (model.Photo!=null)
					{
                        model.PhotoFile = Utilities.Base64Helper.ConvertToBase64(model.Photo);
                        model.Photo = null;
					}
                    string json = JsonConvert.SerializeObject(model);
                    var endpoint = url + "/UpdateAccountInfo";
                    var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request, ref Message);
                    result.SetValues(content.isSucceed, null, content.Message);
                }
                catch (Exception)
                {
                    result.SetValues(false, null, "Failed Message : " + Message);
                }
            }
            else
            {
                var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
                result.SetValues(false, validationMessages, "Failed To Update Account Info");
            }

            return Json(result);
        }

        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordUserViewModel model)
        {
            var result = new AjaxViewModel();
            if (ModelState.IsValid)
            {
                try
                {
                    model.ID = ClaimHelper.LoginID;
                    string json = JsonConvert.SerializeObject(model);
                    var endpoint = url + "/ChangePassword";
                    var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request, ref Message);
                    result.SetValues(content.isSucceed, null, content.Message);
                }
                catch (Exception)
                {
                    result.SetValues(false, null, "Failed Message : " + Message);
                }
            }
            else
            {
                var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
                result.SetValues(false, validationMessages, "Failed To Change Password");
            }

            return Json(result);
        }

        [HttpGet]
        public IActionResult UploadSertificate()
        {
            var endpoint = String.Format("{0}{1}?id={2}", url, Utilities.Route.GetByID, ClaimHelper.LoginID);
            var model = Utilities.RestAPIHelper<MasterUserViewModel>.Submit("", Method.GET, endpoint, Request, ref Message);
            return PartialView("UploadSertificate", model);
        }

        [HttpPost]
        public IActionResult UploadSertificate(IFormFile SertificateFile)
        {
            var model = new UploadCertificateViewModel();
            model.ID = ClaimHelper.LoginID;
            model.Username = ClaimHelper.UserName;
            model.Certificate = Utilities.Base64Helper.ConvertToBase64(SertificateFile);
            var result = new AjaxViewModel();
            
            try
            {
                string json = JsonConvert.SerializeObject(model);
                var endpoint = url + "/UploadCertificate";
                var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request, ref Message);
                result.SetValues(content.isSucceed, null, content.Message);
            }
            catch (Exception)
            {
                result.SetValues(false, null, "Failed Message : " + Message);
            }
            

            return Json(result);
        }

        [HttpGet]
        public IActionResult CreateSign()
        {
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                ViewBag.LoginID = ClaimHelper.LoginID;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;

                var model = new MasterUserViewModel();
                return View("FormCreateSign", model);
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }   
        }

        [HttpPost]
        public IActionResult CreateSign(string sign)
        {
            var result = new AjaxViewModel();
            if (sign != "")
            {
                var model = new CreateSignViewModel();
                model.ID = ClaimHelper.LoginID;
                model.Username = ClaimHelper.UserName;
                model.Signature = sign;

                try
                {
                    string json = JsonConvert.SerializeObject(model);
                    var endpoint = url + "/CreateSign";
                    var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(json, Method.POST, endpoint, Request, ref Message);
                    result.SetValues(content.isSucceed, null, content.Message);
                }
                catch (Exception)
                {
                    result.SetValues(false, null, "Failed Message : " + Message);
                }
            }
            else
            {
                result.SetValues(false, null, "Failed To Save Signature");
            }
            
            return Json(result);
        }
    }
}
