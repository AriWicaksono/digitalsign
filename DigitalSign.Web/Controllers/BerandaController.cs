﻿using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Master.User;
using DigitalSign.ViewModels.Security;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Net.Http.Headers;
using Route = DigitalSign.Utilities.Route;

namespace DigitalSign.Web.Controllers
{

    public class BerandaController : Controller
    {
        string url = $"{ApiUrl.Security}Users";
        string urlMasterUser = $"{ApiUrl.Master}MasterUser";
        string urlMasterDocument = $"{ApiUrl.Master}MasterDocument";
        string urlSecurity = $"{ApiUrl.Security}";
        private readonly ILogger _logger;

        public BerandaController(ILogger<BerandaController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;

                var endpointCount = urlMasterDocument + "/CountStatusDocument" + "?roleName=" + ClaimHelper.RoleName + "&userId=" + ClaimHelper.LoginID;
                ViewBag.HasilCount = RestAPIHelper<Dictionary<string, int>>.Submit("", Method.GET, endpointCount, Request);

                if (ClaimHelper.RoleName != RoleUserRequester.Signer)
                {
                    return View();
                }
                else
                {
                    var endpoint = String.Format("{0}{1}?id={2}", urlMasterUser, Route.GetByID, ClaimHelper.LoginID);
                    var model = Utilities.RestAPIHelper<MasterUserViewModel>.Submit("", Method.GET, endpoint, Request);
                    return View("IndexUser", model);
                }
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        [HttpPost]
        public JsonResult ListDocumentUser([DataSourceRequest] DataSourceRequest request, string Status = null)
        {
            var endpoint = urlMasterDocument + Route.GetAll + "?role=" + ClaimHelper.RoleName + "&userID=" + ClaimHelper.LoginID;
            var viewModel = RestAPIHelper<List<MasterDocumentViewModel>>.Submit("", Method.GET, endpoint, Request);
            var newViewModel = new List<MasterDocumentViewModel>();

            if (Status.HasValue())
			{
                newViewModel = viewModel.Where(x => x.Status == Status).ToList();
			}
			else
			{
                newViewModel = viewModel;
            }

            var filter = newViewModel.ToDataSourceResult(request);
            return Json(filter);
        }

        public IActionResult AccountLiveness(string url)
        {
            ViewBag.Url = url;
            return PartialView("Liveness");
        }

        [HttpPost]
        public JsonResult Activation(ActivationEmailViewModel viewModel)
        {
            var jsonViewModel = new AjaxViewModel();
            try
            {
                var endpoint = url + $"/Activation";
                var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit(JsonConvert.SerializeObject(viewModel), Method.POST, endpoint, Request);
                jsonViewModel = content;
            }
            catch (Exception e)
            {
                jsonViewModel.SetValues(false, null, e.Message);
                //throw;
            }
            return Json(jsonViewModel);
        }

        [HttpPost]
        public async Task<JsonResult> ActivationLama (ActivationEmailViewModel viewModel)
        {
            var jsonViewModel = new AjaxViewModel();
            try
            {
                var endpoint = urlSecurity + $"VerifyEmail?nik={viewModel.NIK}&code=" + viewModel.Code;
                var content = Utilities.RestAPIHelper<ProcessResult>.Submit("", Method.GET, endpoint);
                if (!content.isSucceed)
                {
                    throw new Exception(message: content.message);
                }

                var token = await Tilaka.GetTilakaToken();
                var uuid = await Tilaka.GenerateUUID(token);

                RegisterTilakaViewModel register = JsonConvert.DeserializeObject<RegisterTilakaViewModel>(content.value);
                register.registration_id = uuid;
                var json = JsonConvert.SerializeObject(register);

                var model = Utilities.RestAPIHelper<ResponseRegistrasi>.Submit(json, Method.POST, Tilaka.registerForKycCheck, token);
                if (model.Success)
                {
                    var jsonChangeRegId = new ChangeRegIdViewModel();
                    jsonChangeRegId.NIK = register.nik;
                    jsonChangeRegId.RegistrationId = register.registration_id;

                    string urlSave = urlMasterUser + "/ChangeRegistrationId";
                    var changeUuid = Utilities.RestAPIHelper<ProcessResult>.Submit(JsonConvert.SerializeObject(jsonChangeRegId), Method.POST, urlSave, Request);
                    jsonViewModel.SetValues(true, new { id = register.registration_id }, "Tolong selesaikan liveness untuk mengaktifkan akun");
                }
                else
                {
                    jsonViewModel.SetValues(false, model.Data, model.Message);
                }
            }
            catch (Exception e)
            {
                jsonViewModel.SetValues(false, null, e.Message);
                //throw;
            }
            return Json(jsonViewModel);
        }

        [HttpGet]
        public async Task<JsonResult> SendEmail(string nik)
        {
            var jsonViewModel = new AjaxViewModel();
            var urlSendEmail = urlSecurity + "SendEmail?nik=" + nik;
            var result = Utilities.RestAPIHelper<ProcessResult>.Submit("", Method.GET, urlSendEmail);
            jsonViewModel.SetValues(result.isSucceed, result.value, result.message);
            return Json(jsonViewModel);
        }

        [HttpGet]
		[Route("Register")]
        public IActionResult Register()
        {
            return View("Register");
        }

        public async Task<IActionResult> LinkAccount()
        {
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;
                var endpointUser = String.Format("{0}{1}?id={2}", $"{ApiUrl.Master}MasterUser", Route.GetByID, ClaimHelper.LoginID);
                var dataUser = Utilities.RestAPIHelper<MasterUserViewModel>.Submit("", Method.GET, endpointUser, Request);
                string UUID = string.Empty;
                var token = await Tilaka.GetTilakaToken();
                if (dataUser == null)
                {

                    //UUID = await Tilaka.GenerateUUID(token);
                    return RedirectToAction("Index", "Beranda");
                }
                else
                {
                    UUID = dataUser.RegistrationID?? await Tilaka.GenerateUUID(token);
                    var jbody = new {
                        request_id = UUID,
                        nik = dataUser.NIK
                    };

                    var model = RestAPIHelper<ResponseCheckAkunDS>.Submit(JsonConvert.SerializeObject(jbody), Method.POST, Tilaka.check_exist_account, token);
                    if (model.status)
                    {
                        ViewBag.URLLinkAccount = String.Format("{0}?request_id={1}&setting={2}", Tilaka.linked_account, UUID, 1);
                        return View("PenautanAkun");
                    }
                    else {
                        return RedirectToAction("Index", "Beranda");
                    }

                }
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        public IActionResult VerifLinkAccount()
        {
            _logger.LogInformation("About page hit /VerifLinkAccount at {DT}", DateTime.Now.ToLongTimeString());
            if (ClaimHelper.CheckSession)
            {
                ViewBag.Username = ClaimHelper.UserName;
                ViewBag.Rolename = ClaimHelper.RoleName;
                var notification = ClaimHelper.GetNotification(ClaimHelper.LoginID);
                ViewBag.Notification = notification;
                int counter = notification.Where(a => a.ReadDate == null).Count();
                ViewBag.HitungCounter = counter;

                return View("VerifLinkAccount");
            }
            else
            {
                return RedirectToAction("SignIn", "Auth");
            }
        }

        public async Task<JsonResult> UpdateTilakaName(string request_id, string tilaka_name)
        {
            _logger.LogInformation("About page hit /UpdateTilakaName at {DT}", DateTime.Now.ToLongTimeString());
            var jsonViewModel = new AjaxViewModel();
            if (request_id == null || tilaka_name == null)
            {
                _logger.LogError(LogEvents.GetItemNotFound, $"Request Id = ({request_id}) & Tilaka Name = ({tilaka_name})");
                jsonViewModel.SetValues(false, null, "Wrong URL");
                return Json(jsonViewModel);
            }
            var urlSendEmail = urlMasterUser + $"/UpdateTilakaName?requestId={request_id}&tilakaName={tilaka_name}";
            var result = Utilities.RestAPIHelper<ProcessResult>.Submit("", Method.GET, urlSendEmail, Request);
            jsonViewModel.SetValues(result.isSucceed, result.value, result.message);
            if (jsonViewModel.isSucceed)
            {
                _logger.LogInformation(LogEvents.UpdateItem, $"Succes Update Request Id = ({request_id}) & Tilaka Name = ({tilaka_name})");
            }
            else
            {
                _logger.LogError(LogEvents.UpdateItem, $"Failed Update Request Id = ({request_id}) & Tilaka Name = ({tilaka_name}), message = {result.message}");
            }
            return Json(jsonViewModel);
        }

        [HttpPost]
        public async Task<JsonResult> SubmitRegister(RegisterViewModel viewModel)
        {
            var jsonViewModel = new AjaxViewModel();
            if (ModelState.IsValid)
            {
                var jsonAjaxViewModel = new AjaxViewModel();
                try
                {
                    #region cek db

                    //cek data di DB by NIK
                    //var endpoint = url + $"/CheckAccount?nik={viewModel.NIK}";
                    //var dBChecck = RestAPIHelper<ProcessResult>.Submit("", Method.GET, endpoint);
                    //endpoint register db
                    var urlRegisterDB = String.Format("{0}/Register", url);

                    //if (!dBChecck.isSucceed)
                    //{
                    //    throw new Exception(message: dBChecck.message);
                    //}
                    //else
                    //{
                    //    if (dBChecck.message == ("NIK tersedia"))
                    //    {
                    //        jsonAjaxViewModel.SetValues(false, null, "Data sudah terdaftar");
                    //        jsonViewModel.SetValues(true, jsonAjaxViewModel, "");
                    //    }
                    //    else
                    //    {
                            Random rdm = new Random();
                            var randomCode = rdm.Next();
                            viewModel.Code = randomCode.ToString();
                            var urlSendEmail = urlSecurity + "SendEmail?nik=" + viewModel.NIK;
                            var token = await Tilaka.GetTilakaToken();
                            var genUuid = await Tilaka.GenerateUUID(token);
                            var insert = await InsertRegisterDB(viewModel, genUuid, urlRegisterDB);
                            if (insert)
                            {
                                var result = Utilities.RestAPIHelper<ProcessResult>.Submit("", Method.GET, urlSendEmail);
                                jsonAjaxViewModel.SetValues(true, null, result.message);
                            }
                            else
                            {
                                jsonAjaxViewModel.SetValues(false, null, "Data Gagal Ditambahkan");
                            }
                            jsonViewModel.SetValues(true, jsonAjaxViewModel, "");
                        //}
                    //}

                    #endregion
                }
                catch (Exception e)
                {
                    jsonAjaxViewModel.SetValues(false, null, e.Message);
                    jsonViewModel.SetValues(true, jsonAjaxViewModel, "");
                    //throw;
                }
                return Json(jsonViewModel);
            }
            var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
            jsonViewModel.SetValues(false, validationMessages, "");
            return Json(jsonViewModel);
        }

        private async Task<bool> InsertRegisterDB(RegisterViewModel viewModel, string uuid, string enpoint)
        {
            var register = new RegisterInsertViewModel();
            register.Registration_Id = uuid;
            register.Username = viewModel.Username;
            register.Password = viewModel.Password;
            register.Email = viewModel.Email;
            register.PhoneNumber = viewModel.PhoneNumber;
            register.CompanyName = viewModel.CompanyName;
            register.FirstName = viewModel.FirstName;
            register.MidleName = viewModel.MidleName;
            register.LastName = viewModel.LastName;
            register.Code = viewModel.Code;
            register.NIK = viewModel.NIK;
            register.BirthPlace = viewModel.BirthPlace;
            register.BirthDate = viewModel.BirthDate.Value;
            register.Address = viewModel.Address;
            register.KTPFile = Utilities.Base64Helper.ConvertToBase64(viewModel.KTPFile);
            register.KTPName = viewModel.KTPFile.FileName;
            register.PhotoFile = Utilities.Base64Helper.ConvertToBase64(viewModel.PhotoFile);
            register.PhotoName = viewModel.PhotoFile.FileName;

            var json = JsonConvert.SerializeObject(register);

            var result = Utilities.RestAPIHelper<ProcessResult>.Submit(json, Method.POST, enpoint);

            return result.isSucceed;
        }

    }
}
