﻿using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Newtonsoft.Json;
using RestSharp;

namespace DigitalSign.Web.Controllers
{
    public class AuthController : Controller
    {
        string url = $"{ApiUrl.Security}";

        [HttpGet]
        [Route("admin")]
        public IActionResult Index()
        {
            if (ClaimHelper.CheckSession)
            {
                return RedirectToAction("Index", "Beranda");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public IActionResult Index(LoginRequestViewModel model)
        {
            var endpoint = url + "Login";
            var result = new AjaxViewModel();
            if (ModelState.IsValid)
            {
                try
                {
                    string json = JsonConvert.SerializeObject(model);
                    var content = Utilities.RestAPIHelper<AjaxViewModel<LoginResponseViewModel>>.Submit(json, Method.POST, endpoint);

                    if (content.isSucceed)
                    {
                        if (!String.IsNullOrEmpty(HttpContext.Request.Cookies["token"]))
                        {
                            HttpContext.Response.Cookies.Delete("token");
                            HttpContext.Response.Cookies.Append("token", content.Data.Token);
                        }
                        else
                        {
                            HttpContext.Response.Cookies.Append("token", content.Data.Token);
                        }
                    }

                    result.SetValues(content.isSucceed, content.Data, content.Message);
                }
                catch (Exception)
                {
                    result.SetValues(false, null, "Something Error");
                }

            }
            else
            {
                var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
                result.SetValues(false, validationMessages, "");
            }

            return Json(result);
        }

        [Route("Logout")]
        public IActionResult Logout()
        {
            HttpContext.Response.Cookies.Delete("token");
            return RedirectToAction("SignIn");
        }

        public IActionResult SignIn()
        {
            if (ClaimHelper.CheckSession)
            {
                return RedirectToAction("Index", "Beranda");
            }
            else
            {
                return View("SignIn");
            }
        }

        [Route("ForgotPassword")]
        public IActionResult ForgotPassword()
        {
            return PartialView("_ForgotPassword");
        }

        [HttpPost]
        [Route("ForgotPassword")]
        public IActionResult ForgotPassword(string email)
        {
            var ajaxVM = new AjaxViewModel();
            var abc = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";

            //string url = u.Action("About", "Home", null);
            var urlEmail = abc + Url.Action("ResetPassword", "Auth", new { key = ""});
            var endpoint = $"{url}ForgotPassword?email={email}&url={urlEmail}";
            var content = Utilities.RestAPIHelper<ProcessResult>.Submit("", Method.GET, endpoint);
            if (content != null)
            {
                ajaxVM.SetValues(content.isSucceed, content.value, content.message);
            }
            else {
                ajaxVM.SetFailedWithEmptyList("");
            }
            return Json(ajaxVM);
        }

        [Route("ResetPassword")]
		public IActionResult ResetPassword(string code, string email)
		{
            var timeClick = DateTime.Now;
            var endpoint = $"{url}ConfirmResetPassword?code={code}&email={email}&timeClick={timeClick}";
            var content = Utilities.RestAPIHelper<AjaxViewModel>.Submit("", Method.GET, endpoint);
            if (content != null)
            {
                if (content.isSucceed)
                {
                    ViewBag.IsSuccess = 1;
                    var abc = JsonConvert.DeserializeObject<ResetPasswordViewModel>(content.Data.ToString());
                    return View("ForgotPassword", abc);
                } 
                else
                {
                    ViewBag.IsSuccess = 0;
                    ViewBag.Message = content.Message;
                    return View("ForgotPassword");
                }
            }
            else
            {
                ViewBag.IsSuccess = 0;
                ViewBag.Message = "Something Wrong!";
                return View("ForgotPassword");
            }
		}

        [HttpPost("ResetPassword")]
        public IActionResult ResetPassword(ResetPasswordViewModel viewModel)
        {
            var result = new AjaxViewModel();
            if (viewModel.Password != viewModel.ConfirmPassword)
            {
                ModelState.AddModelError(nameof(viewModel.ConfirmPassword), "Password and Confirm Password is not match!");
            }

            if (ModelState.IsValid)
            {
                var endpoint = $"{url}ResetPassword";
                var json = JsonConvert.SerializeObject(viewModel);
                var content = Utilities.RestAPIHelper<ProcessResult>.Submit(json, Method.POST, endpoint);
                if (content != null)
                {
                    result.SetValues(content.isSucceed, content.value, content.message);
                }
                else
                {
                    result.SetFailedWithEmptyList("");
                }
            }
            else
            {
                var validationMessages = Utilities.ValidationHelper.GetValidationMessage(ModelState);
                result.SetValues(false, validationMessages, "Validation error");
            }
            return Json(result);
        }
    }
}
