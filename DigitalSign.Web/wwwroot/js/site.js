﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.


// Write your JavaScript code.
//const baseurl = 'https://localhost:7222/';
//const baseurl = 'https://starbridgesv2.indocyber.co.id:8987/';
const baseurl = 'https://starbridges.indocyber.co.id:455/';
window.mainMenuUrl = baseurl + "Beranda/Index";
window.loginUrl = baseurl + "Auth/Index";

$("#Form-Login").submit(function (e) {
    e.preventDefault();
    showProgress();
    let modelLogin = $("#Form-Login").serialize();
    $.ajax({
        type: "POST",
        data: modelLogin,
        url: window.loginUrl,
        success: function (result) {
            hideProgress();
            if (result.isSucceed) {
                window.location.href = mainMenuUrl;

            }
            else {
                swal("Failed!", result.Message, "error");
                showValidationMessage(result.Data);
            }
        },
        error: function (err) {
            swal("Something went wrong!", err.Message, "error");
        }
    });
});

$("#Form-SignIn").submit(function (e) {
    e.preventDefault();
    showProgress();
    let modelLogin = $("#Form-SignIn").serialize();
    $.ajax({
        type: "POST",
        data: modelLogin,
        url: window.loginUrl,
        success: function (result) {
            hideProgress();
            if (result.isSucceed) {
                window.location.href = mainMenuUrl;
            }
            else {
                swal("Failed!", result.Message, "error");
                showValidationMessage(result.Data);
            }
        },
        error: function (err) {
            swal("Something went wrong!", err.Message, "error");
        }
    });
});

for (let i = 0; i < $(".dropdown").length; i++) {
    let targetId = $(".dropdown").eq(i).children().prop("id");
    $(".dropdown").eq(i).click(function (e) {
        $(`.dropdown-menu[aria-labelledby='${targetId}']`).toggle();
    })
}

function showValidationMessage(datas) {
    for (var data of datas) {
        let propertyName = data.PropertyName;
        let message = data.ErrorMessage;
        $(`span[data-for='${propertyName}']`).html(message);
    }
}