﻿$('#KTPFile').change(function (e) {
	showFoto('ktp-contain', 'KTPFile');
});

$('#PhotoFile').change(function (e) {
	showFoto('photo-contain', 'PhotoFile');
});

$('#back').click(function (e) {
	window.location.href = urlIndex;
});

$('#save').click(function(e) {
	e.preventDefault();
	showProgress();
	$('#save').disableBtn();
	var fdata = new FormData();

	var KTPFile = $('#KTPFile')[0].files[0];
	var Photofile = $('#PhotoFile')[0].files[0];
	const username = $('#Username').val();
	const email = $('#Email').val();
	const phoneNumber = $('#PhoneNumber').val();
	const password = $('#Password').val();
	const confirmPassword = $('#ConfirmPassword').val();
	const companyName = $('#CompanyName').val();
	const firstName = $('#FirstName').val();
	const middleName = $('#MidleName').val();
	const lastName = $('#LastName').val();
	const birthPlace = $('#BirthPlace').val();
	const NIK = $('#NIK').val();
	const address = $('#Address').val();
	const birthDate = $('#BirthDate').val();
	const roleId = $('#RoleID').data("kendoDropDownList").value();

	fdata.append("Username", username);
	fdata.append("Password", password);
	fdata.append("ConfirmPassword", confirmPassword);
	fdata.append("Email", email);
	fdata.append("PhoneNumber", phoneNumber);
	fdata.append("CompanyName", companyName);
	fdata.append("FirstName", firstName);
	fdata.append("MidleName", middleName);
	fdata.append("LastName", lastName);
	fdata.append("NIK", NIK);
	fdata.append("BirthPlace", birthPlace);
	fdata.append("BirthDate", birthDate);
	fdata.append("address", address);
	fdata.append("KTPFile", KTPFile);
	fdata.append("PhotoFile", Photofile);
	fdata.append("RoleID", roleId);

	console.log(fdata);

	$.ajax({
		url: actionUrl,
		method: "POST",
		data: fdata,
		dataType: "json",
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		success: function (res) {
			console.log(res);
			hideProgress();
			if (res.isSucceed) {
				swal("Success!", res.Message, "success");
				$('.confirm').click(() => { location.href = urlIndex });
			} else {
				if (res.Data != null) {
					showValidateMessage(res.Data);
				}
				$('#save').enableBtn();
				swal("Failed!", res.Message, "error");
			}
		},
		error: function () { }
	});
})

function showFoto(container, input) {
	const foto = window.URL.createObjectURL($(`#${input}`)[0].files[0]);
	const elm = `<img src="${foto}" alt="image" height="100" />`;
	$(`#${container}`).html(elm);
}

function showValidateMessage(validations) {
	let validate = "";
	for (var validation of validations) {

		let propertyName = validation.PropertyName;
		let errorMessage = validation.ErrorMessage;

		let parent = $(`#${propertyName}`).closest("div");
		parent.append(`<span data-valmsg-for='${propertyName}' style='color: red'>${errorMessage}</span>`)
		//$(`span[data-valmsg-for=${propertyName}]`).text(errorMessage).css("color", "red").css("display","block");
		//console.log(errorMessage + " " + propertyName);
	}
	return validate;
}

jQuery.fn.extend({
	disableBtn: function () {
		return this.each(function () {
			var $this = $(this).html();
			$(this).html("<i class='fa fa-circle-o-notch fa-spin'></i> Processing...");
			$(this).prop("origin-text", $this);
			$(this).attr("disabled", true);
			$(this).addClass("k-state-disabled");
		});
	},

	enableBtn: function () {
		return this.each(function () {
			if ($(this).prop("origin-text") != null)
				$(this).html($(this).prop("origin-text"));
			$(this).attr("disabled", false);
			$(this).removeClass("k-state-disabled");
		});
	}
});