﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Master.Notification;
using DigitalSign.ViewModels.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Master
{
    public interface IMasterDocumentProvider : IBaseMasterProvider<MasterDocumentViewModel>
    {
        IQueryable<MasterDocumentViewModel> GetAll(string role, int userID);
        ProcessResult Save(List<MasterDocumentViewModel> model, string displayName);
        ProcessResult UpdateRequestID(int DocumentID, string RequestID);
        ProcessResult UpdateRequestID(int documentID, string requestID, string userSign, string url);
        IQueryable<DocumentParticipantViewModel> GetAllParticipant(int DocumentID);
        ProcessResult UpdateStatusSignDocument(int DocumentID, int UserID, string Status);
        ProcessResult SendDocument(int documentID);
        MasterDocumentViewModel GetByRequestID(string RequestID);
        Dictionary<string, int> CountStatusDocument(int userId, string roleName);
        IQueryable<MasterDocumentViewModel> Search(int userId, string roleName, string keyword);
        DocumentMateraiViewModel GetDocumentForMaterai(int docId);
        string GetGlobalConfigurationByKeyName(string keyName);
        WebHook GetNextParticipantUrl(int docId, string lastSigner, string nextSigner);
        string UrlWebViewFailed(int docId, string currentSigner);
    }
    public class MasterDocumentProvider : IMasterDocumentProvider
    {
        DigitalSignContext context;
        INotificationProvider notificationProvider;

        public MasterDocumentProvider(DigitalSignContext context, INotificationProvider notificationProvider)
        {
            this.context = context;
            this.notificationProvider = notificationProvider;
        }

        public IQueryable<MasterDocumentViewModel> GetAll()
        {
            try
            {
                var query = from document in context.Document
                            where !document.DelDate.HasValue
                            orderby document.CreatedDate descending
                            select new MasterDocumentViewModel
                            {
                                ID = document.ID,
                                Title = document.Title,
                                DocumentName = document.DocumentName,
                                Status = document.Status,
                                CreatedBy = document.CreatedBy,
                                CreatedDate = document.CreatedDate
                            };
                return query;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable<MasterDocumentViewModel> GetAll(string role, int userID)
        {
            try
            {
                if (role == RoleUserRequester.SuperAdmin)
                {
                    var query = from document in context.Document
                                where !document.DelDate.HasValue
                                orderby document.CreatedDate descending
                                select new MasterDocumentViewModel
                                {
                                    ID = document.ID,
                                    Title = document.Title,
                                    DocumentName = document.DocumentName,
                                    Status = document.Status,
                                    CreatedBy = document.CreatedBy,
                                    CreatedDate = document.CreatedDate
                                };
                    return query;
                }
                else if (role == RoleUserRequester.Signer)
                {
                    var query = from document in context.Document
                                join docPart in context.DocumentParticipant on document.ID equals docPart.DocumentID
                                where !document.DelDate.HasValue && docPart.UserID == userID && document.Status != null
                                orderby document.CreatedDate descending
                                select new MasterDocumentViewModel
                                {
                                    ID = document.ID,
                                    Title = document.Title,
                                    DocumentName = document.DocumentName,
                                    Status = document.Status,
                                    CreatedBy = document.CreatedBy,
                                    CreatedDate = document.CreatedDate
                                };
                    return query;
                }
                else 
                {
                    var getUserRole = context.Users.Where(m => m.ID == userID).FirstOrDefault();
                    var query = from document in context.Document
                                where !document.DelDate.HasValue
                                //&& document.Owner == getUserRole.Username
                                orderby document.CreatedDate descending
                                select new MasterDocumentViewModel
                                {
                                    ID = document.ID,
                                    Title = document.Title,
                                    DocumentName = document.DocumentName,
                                    Status = document.Status,
                                    CreatedBy = document.CreatedBy,
                                    CreatedDate = document.CreatedDate
                                };
                    return query;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MasterDocumentViewModel GetById(int id)
        {
            try
            {
                var query = (from document in context.Document
                             where (document.ID == id)
                             select new MasterDocumentViewModel
                             {
                                 ID = document.ID,
                                 Title = document.Title,
                                 DocumentName = document.DocumentName,
                                 DocumentFile = document.DocumentFile,
                                 Status = document.Status,
                                 CreatedDate = document.CreatedDate,
                                 CreatedBy = document.CreatedBy,
                                 RequestID = document.requestID == null ? null : document.requestID
                             }).SingleOrDefault();

                var participants = (from docpar in context.DocumentParticipant
                                    where docpar.DocumentID == id
                                    select new DocumentParticipantViewModel
                                    {
                                        ID = docpar.ID,
                                        UserID = docpar.UserID,
                                        Username = docpar.User.Username,
                                        Sequence = docpar.Sequence,
                                        Status = docpar.Status
                                    }).ToList();

                var discussion = (from docdis in context.DocumentDiscussion
                                  join user in context.UsersIdentity on docdis.UserID equals user.UserID
                                  where docdis.DocumentID == id
                                  orderby docdis.ID descending
                                  select new DocumentDiscussionViewModel
                                  {
                                      ID = docdis.ID,
                                      UserID = docdis.UserID,
                                      TimeStamp = docdis.TImeStamp,
                                      Message = docdis.Message,
                                      Poto = user.PhotoFile,
                                      FullName = $"{user.FirstName} {user.MiddleName} {user.LastName}".Replace("  ", " ")
                                  }).ToList();

                var history = (from dochistory in context.DocumentHistory
                               where dochistory.DocumentID == id
                               select new DocumentHistoryViewModel
                               {
                                   ID = dochistory.ID,
                                   Username = dochistory.User.Username,
                                   Activity = dochistory.Activity,
                                   TimeStamp = dochistory.TimeStamp
                               }).ToList();

                query.DocumentParticipant = participants;
                query.DocumentDiscussion = discussion;
                query.DocumentHistory = history;

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProcessResult Save(MasterDocumentViewModel model, string displayName)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                context.Database.BeginTransaction();

                var document = new Document();
                document.Title = model.Title;
                document.DocumentName = model.DocumentName;
                document.DocumentFile = model.DocumentFile;
                document.Status = model.Status;
                EntityHelper.SetAuditFieldsForInsert(document, displayName);
                context.Document.Add(document);

                context.SaveChanges();
                var id = context.Document.OrderBy(a => a.ID).Last().ID;

                foreach (var item in model.DocumentParticipant)
                {
                    context.DocumentParticipant.Add(new DocumentParticipant()
                    {
                        DocumentID = id,
                        Sequence = item.Sequence,
                        UserID = item.UserID,
                        Status = "none"
                    });
                };

                foreach (var item in model.DocumentHistory)
                {
                    context.DocumentHistory.Add(new DocumentHistory()
                    {
                        Activity = item.Activity,
                        DocumentID = id,
                        UserID = item.UserId,
                        TimeStamp = item.TimeStamp
                    });
                };
                context.SaveChanges();
                context.Database.CommitTransaction();
                result.InsertSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
                context.Database.RollbackTransaction();
            }
            return result;
        }

        public ProcessResult Save(List<MasterDocumentViewModel> models, string displayName)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                context.Database.BeginTransaction();

                foreach (var model in models)
                {
                    var document = new Document();
                    document.Title = model.Title;
                    document.DocumentName = model.DocumentName;
                    document.DocumentFile = model.DocumentFile;
                    document.Status = model.Status;
                    document.CategoryID = Convert.ToInt32(model.Category);
                    EntityHelper.SetAuditFieldsForInsert(document, displayName);
                    context.Document.Add(document);

                    context.SaveChanges();
                    var id = context.Document.OrderBy(a => a.ID).Last().ID;

                    foreach (var item in model.DocumentParticipant)
                    {
                        context.DocumentParticipant.Add(new DocumentParticipant()
                        {
                            DocumentID = id,
                            Sequence = item.Sequence,
                            UserID = item.UserID,
                            Status = "none"
                        });
                    };

                    foreach (var item in model.DocumentHistory)
                    {
                        context.DocumentHistory.Add(new DocumentHistory()
                        {
                            Activity = item.Activity,
                            DocumentID = id,
                            UserID = item.UserId,
                            TimeStamp = item.TimeStamp
                        });
                    };
                    context.SaveChanges();
                }

                context.Database.CommitTransaction();
                result.InsertSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
                context.Database.RollbackTransaction();
            }
            return result;
        }
        public ProcessResult Delete(int id)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var document = context.Document.SingleOrDefault(m => m.ID == id);
                document.DelDate = DateTime.Now;
                context.SaveChanges();
                result.ProcessSucceed("Data has been deleted !!!");
            }
            catch (Exception ex)
            {
                result.ProcessFailed(ex.Message);
                throw;
            }
            return result;
        }

        public ProcessResult UpdateRequestID(int documentID, string requestID)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var oldDoc = context.Document.SingleOrDefault(x => x.ID == documentID);
                oldDoc.requestID = requestID;
                context.SaveChanges();
                result.UpdateSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public ProcessResult UpdateRequestID(int documentID, string requestID, string userSign, string url)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var oldDoc = context.Document.SingleOrDefault(x => x.ID == documentID);
                oldDoc.requestID = requestID;
                var getUserSign = context.Users.SingleOrDefault(m => m.Username == userSign);
                var getDocumentSigner = context.DocumentSign.SingleOrDefault(x => x.DocumentID == documentID && x.UserID == getUserSign.ID);
                if (getDocumentSigner != null)
                {
                    getDocumentSigner.URL = url;
                    getDocumentSigner.IsRequested = true;
                    getDocumentSigner.IsSigned = false;
                    EntityHelper.SetAuditFieldsForUpdate(getDocumentSigner, userSign);
                }
                context.SaveChanges();
                result.UpdateSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public WebHook GetNextParticipantUrl(int docId, string lastSigner, string nextSigner)
        {
            var getDocument = context.Document.SingleOrDefault(m => m.ID == docId);
            if (getDocument != null)
            {
                if (getDocument.requestID != null)
                {
                    var getWebHook = context.WebHook.SingleOrDefault(m => m.request_id == getDocument.requestID && m.signer == lastSigner && m.signer_next == nextSigner);
                    return getWebHook;
                }
            }
            return null;
        }

        public string UrlWebViewFailed(int docId, string currentSigner)
        {
            var getUser = context.Users.SingleOrDefault(m => m.Username == currentSigner);
            if (getUser != null)
            {
                var getDocumentSign = context.DocumentSign.SingleOrDefault(m => m.DocumentID == docId && m.UserID == getUser.ID);
                if (getDocumentSign != null)
                {
                    return getDocumentSign.URL;
                }
            }
            return null;
        }
        public ProcessResult UpdateRequestID(int documentID, string requestId, string userSign, string url, string currentUser)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                context.Database.BeginTransaction();
                if (DocSignIsSet(documentID))
                {
                    var oldDocument = context.Document.SingleOrDefault(x => x.ID == documentID);
                    oldDocument.requestID = requestId;
                    oldDocument.Status = "Terkirim";

                    var getUserSign = context.Users.SingleOrDefault(m => m.Username == userSign);
                    var getDocumentSigner = context.DocumentSign.SingleOrDefault(x => x.ID == documentID && x.UserID == getUserSign.ID);
                    if (getDocumentSigner != null)
                    {
                        getDocumentSigner.URL = url;
                        getDocumentSigner.IsRequested = true;
                        getDocumentSigner.IsSigned = false;
                    }
                    var getAllPart = context.DocumentParticipant.Where(x => x.DocumentID == documentID);

                    foreach (var item in getAllPart)
                    {
                        var notifModel = new Notification
                        {
                            Type = "Document",
                            Title = "New Document",
                            Detail = $"New Document ready to signing ({oldDocument.Title} - {oldDocument.DocumentName})",
                            UserID = item.UserID,
                            RedirectTo = $"Document/Detail?id={oldDocument.ID}",
                            Icon = "file",
                            CreatedDate = DateTime.Now,
                            CreatedBy = "System"
                        };

                        context.Notification.Add(notifModel);
                    }

                    result.message = "Document Telah Terkirim";
                    result.isSucceed = true;
                    context.SaveChanges();
                    context.Database.CommitTransaction();
                }
                else
                {
                    result.message = "Tanda Tangan Participant Belum Di Set Secara Lengkap";
                    result.isSucceed = false;
                }

            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
                context.Database.RollbackTransaction();
            }

            return result;
        }

        public IQueryable<DocumentParticipantViewModel> GetAllParticipant(int DocumentID)
        {
            var participants = (from docpar in context.DocumentParticipant
                                where docpar.DocumentID == DocumentID
                                select new DocumentParticipantViewModel
                                {
                                    ID = docpar.ID,
                                    UserID = docpar.UserID,
                                    Username = docpar.User.Username,
                                    Sequence = docpar.Sequence,
                                    Status = docpar.Status
                                });
            return participants;
        }

        public ProcessResult UpdateStatusSignDocument(int DocumentID, int UserID, string Status)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                context.Database.BeginTransaction();
                var targetStatusPart = context.DocumentParticipant.SingleOrDefault(x => x.DocumentID == DocumentID && x.UserID == UserID);
                targetStatusPart.Status = Status;
                context.SaveChanges();

                var DocPart = context.DocumentParticipant.Where(x => x.DocumentID == DocumentID);
                int countDocPart = DocPart.Count();
                int countSuksesSign = DocPart.Where(x => x.Status == "Sukses").Count();

                if (countDocPart == countSuksesSign)
                {
                    var targetDoc = context.Document.SingleOrDefault(x => x.ID == DocumentID);
                    targetDoc.Status = "Complete";
                }
                else if (countDocPart > countSuksesSign)
                {
                    var targetDoc = context.Document.SingleOrDefault(x => x.ID == DocumentID);
                    targetDoc.Status = "Ongoing";
                }

                context.SaveChanges();
                context.Database.CommitTransaction();
                result.UpdateSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
                context.Database.RollbackTransaction();
            }
            return result;
        }

        public MasterDocumentViewModel GetByRequestID(string RequestID)
        {
            try
            {
                var query = (from a in context.Document
                             where a.requestID == RequestID
                             select new MasterDocumentViewModel
                             {
                                 ID = a.ID,
                                 Title = a.Title,
                                 DocumentName = a.DocumentName,
                                 DocumentFile = a.DocumentFile,
                                 Status = a.Status,
                                 CreatedDate = a.CreatedDate,
                                 CreatedBy = a.CreatedBy,
                                 RequestID = a.requestID
                             }).SingleOrDefault();

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProcessResult SendDocument(int documentID)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                context.Database.BeginTransaction();
                if (DocSignIsSet(documentID))
                {
                    var oldDocument = context.Document.SingleOrDefault(x => x.ID == documentID);
                    oldDocument.Status = "Terkirim";

                    var getAllPart = context.DocumentParticipant.Where(x => x.DocumentID == documentID);

                    foreach (var item in getAllPart)
                    {
                        var notifModel = new Notification
                        {
                            Type = "Document",
                            Title = "New Document",
                            Detail = $"New Document ready to signing ({oldDocument.Title} - {oldDocument.DocumentName})",
                            UserID = item.UserID,
                            RedirectTo = $"Document/Detail?id={oldDocument.ID}",
                            Icon = "file",
                            CreatedDate = DateTime.Now,
                            CreatedBy = "System"
                        };

                        context.Notification.Add(notifModel);
                    }

                    result.message = "Document Telah Terkirim";
                    result.isSucceed = true;
                    context.SaveChanges();
                    context.Database.CommitTransaction();
                }
                else
                {
                    result.message = "Tanda Tangan Participant Belum Di Set Secara Lengkap";
                    result.isSucceed = false;
                }

            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
                context.Database.RollbackTransaction();
            }

            return result;
        }

        public bool DocSignIsSet(int DocId)
        {
            try
            {
                bool check = true;
                var idParticipant = context.DocumentParticipant.Where(x => x.DocumentID == DocId).Select(x => x.UserID).ToList();
                var DocSign = context.DocumentSign.Where(x => x.DocumentID == DocId).Select(x => x.UserID).ToList();

                foreach (var item in idParticipant)
                {
                    check = DocSign.Contains(item);
                    if (!check)
                    {
                        break;
                        return check;
                    }
                }

                return check;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Dictionary<string, int> CountStatusDocument(int userId, string roleName)
        {
            var data = new Dictionary<string, int>();

            try
            {
                int dataDocumentComplete = GetAll(roleName, userId).Where(x => x.Status == "Complete").Count();
                int dataDocumentOngoing = GetAll(roleName, userId).Where(x => x.Status == "Ongoing").Count();
                int dataDocumentPending = GetAll(roleName, userId).Where(x => x.Status == "Terkirim").Count();

                data.Add("Complete", dataDocumentComplete);
                data.Add("Ongoing", dataDocumentOngoing);
                data.Add("Pending", dataDocumentPending);

                return data;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public IQueryable<MasterDocumentViewModel> Search(int userId, string roleName, string keyword)
        {
            try
            {
                return GetAll(roleName, userId).Where(x => x.Title.ToLower().Contains(keyword.ToLower())).AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string GetGlobalConfigurationByKeyName(string keyName)
        {
            return context.GlobalConfiguration.FirstOrDefault(m => m.KeyName == keyName).KeyValue;
        }

        public DocumentMateraiViewModel GetDocumentForMaterai(int docId)
        {
            var query = context.Document.SingleOrDefault(m => m.ID == docId);
            var getDoc = new DocumentMateraiViewModel();
            if (query != null)
            {
                //getDoc.DocumentID = query.ID;
                getDoc.ContentPDF = query.DocumentFile;
                getDoc.FileName = query.DocumentName;
            }
            return getDoc;
        }
    }
}
