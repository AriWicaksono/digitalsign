﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Master.Notification;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Master
{
    public interface INotificationProvider : IBaseMasterProvider<Notification>
    {
        IEnumerable<NotificationViewModel> ShowNotification(int userID);
        ProcessResult CreateNotif(NotificationViewModel viewModel);
        ProcessResult ReadNotification(int notifID, int userID);
        ProcessResult DeleteNotification(int notifID, string username);
        ProcessResult WebHookProcess(CallbackSigningViewModel viewModel);
    }
    public class NotificationProvider : INotificationProvider
    {
        private readonly DigitalSignContext context;

        public NotificationProvider(DigitalSignContext context)
        {
            this.context = context;
        }

        public IEnumerable<NotificationViewModel> ShowNotification(int userID)
        {
            try
            {
                var notif = context.Notification.Where(a => a.UserID == userID && a.DeletedDate == null)
                    .Select(a => new NotificationViewModel
                    {
                        ID = a.ID,
                        Type = a.Type,
                        Title = a.Title,
                        Detail = a.Detail,
                        Icon = a.Icon,
                        UserID = a.UserID,
                        RedirectTo = a.RedirectTo,
                        ReadDate = a.ReadDate,
                        CreateDate = a.CreatedDate
                    }).OrderByDescending(x => x.ID).AsEnumerable();

                return notif;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ProcessResult CreateNotif(NotificationViewModel viewModel)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var notif = new Notification
                {
                    Type = viewModel.Type,
                    //ReadDate = null,
                    Title = viewModel.Title,
                    Detail = viewModel.Detail,
                    UserID = viewModel.UserID,
                    Icon = viewModel.Icon
                };
                EntityHelper.SetAuditFieldsForInsert(notif, String.IsNullOrEmpty(viewModel.CreatedBy) ? "System" : viewModel.CreatedBy);

                context.Notification.Add(notif);
                context.SaveChanges();
                result.ProcessSucceed("Notification Berhasil ditambahkan");
            }
            catch (Exception e)
            {
                result.ProcessFailed(e.Message);
            }
            return result;
        }

        public ProcessResult ReadNotification(int notifID, int userID)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var check = context.Notification.SingleOrDefault(a => a.ID == notifID && a.UserID == userID);
                if (check.ReadDate != null)
                {
                    result.ProcessSucceed("Notification sudah dibaca");
                    return result;
                }
                else
                {
                    check.ReadDate = DateTime.Now;
                    context.SaveChanges();
                    result.ProcessSucceed("Notification Berhasil dibaca");
                }
            }
            catch (Exception e)
            {
                result.ProcessFailed(e.Message);
            }
            return result;
        }

        public ProcessResult DeleteNotification(int notifID, string username)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var notif = context.Notification.SingleOrDefault(a => a.ID == notifID);
                notif.DeletedDate = DateTime.Now;
                notif.DeletedBy = username;
                context.SaveChanges();
                result.ProcessSucceed("Notification successfully deleted");
            }
            catch (Exception e)
            {
                result.ProcessFailed(e.Message);
            }
            return result;
        }

        public ProcessResult WebHookProcess(CallbackSigningViewModel viewModel)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                //context.Database.BeginTransaction();
                var findExist = context.WebHook.SingleOrDefault(m => m.request_id == viewModel.request_id && m.signer == viewModel.signer && m.signer_next == viewModel.signer_next);
                if (findExist == null)
                {
                    var webHook = new WebHook()
                    {
                        request_id = viewModel.request_id,
                        success = viewModel.success,
                        signer = viewModel.signer,
                        signer_next = viewModel.signer_next,
                        url = viewModel.url
                    };
                    EntityHelper.SetAuditFieldsForInsert(webHook, "System");
                    context.WebHook.Add(webHook);
                }
                var getUserSign = context.Users.FirstOrDefault(m => m.Username == viewModel.signer);
                if (getUserSign != null)
                {
                    var targetDoc = context.Document.FirstOrDefault(x => x.requestID == viewModel.request_id);
                    if (targetDoc != null)
                    {
                        var targetStatusPart = context.DocumentParticipant.SingleOrDefault(x => x.DocumentID == targetDoc.ID && x.UserID == getUserSign.ID);
                        targetStatusPart.Status = viewModel.success ? "Sukses" : "none";
                        EntityHelper.SetAuditFieldsForInsert(targetStatusPart, "System");
                        var DocPart = context.DocumentParticipant.Where(x => x.DocumentID == targetDoc.ID);
                        int countDocPart = DocPart.Count();
                        int countSuksesSign = DocPart.Where(x => x.Status == "Sukses").Count();
                        countSuksesSign = viewModel.success ? countSuksesSign+1 : countSuksesSign;

                        if (countDocPart == countSuksesSign)
                        {
                            targetDoc.Status = "Complete";
                        }
                        else if (countDocPart > countSuksesSign)
                        {
                            targetDoc.Status = "Ongoing";
                        }
                        EntityHelper.SetAuditFieldsForInsert(targetDoc, "System");
                    }
                }
                context.SaveChanges();
                //context.Database.CommitTransaction();
                result.InsertSucceed();
            }
            catch (Exception e)
            {
                result.ProcessFailed($"{e.Message}");
                //context.Database.RollbackTransaction();
            }
            return result;
        }

        public IQueryable<Notification> GetAll()
        {
            throw new NotImplementedException();
        }

        public Notification GetById(int id)
        {
            throw new NotImplementedException();
        }

        public ProcessResult Save(Notification model, string displayName)
        {
            throw new NotImplementedException();
        }

        public ProcessResult Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
