﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Master
{
    public interface IDocumentCategoryProvider
    {
        IQueryable<DocumentCategoryViewModel> GetList();
        DocumentCategoryViewModel GetDocumentCategoryById(int id);
        ProcessResult AddEditDocumentCategory(DocumentCategoryViewModel model, string currentUser);
        ProcessResult DeleteDocumentCategory(int id, string currentUser);
    }
    public class DocumentCategoryProvider : IDocumentCategoryProvider
    {
        DigitalSignContext _context;
        public DocumentCategoryProvider(DigitalSignContext context)
        {
            _context = context;
        }

        public IQueryable<DocumentCategoryViewModel> GetList()
        {
            var query = _context.DocumentCategory.Where(m => m.DeleteDate == null).Select(m => new DocumentCategoryViewModel
            {
                ID = m.ID,
                CategoryName = m.CategoryName,
                CategoryDescription = m.CategoryDescription
            });
            return query;
        }

        public ProcessResult AddEditDocumentCategory(DocumentCategoryViewModel model, string currentUser)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                if (model.ID == 0)
                {
                    var entity = new DocumentCategory()
                    {
                        CategoryName = model.CategoryName,
                        CategoryDescription = model.CategoryDescription
                    };
                    EntityHelper.SetAuditFieldsForInsert(entity, currentUser);
                    _context.DocumentCategory.Add(entity);
                    _context.SaveChanges();
                    result.InsertSucceed();
                }
                else
                {
                    var targetEntity = _context.DocumentCategory.SingleOrDefault(m => m.ID == model.ID);
                    targetEntity.CategoryName = model.CategoryName;
                    targetEntity.CategoryDescription = model.CategoryDescription;
                    EntityHelper.SetAuditFieldsForUpdate(targetEntity, currentUser);
                    _context.SaveChanges();
                    result.UpdateSucceed();
                }
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public ProcessResult DeleteDocumentCategory(int id, string currentUser)
        {
            ProcessResult result = new ProcessResult();
            var targetEntity = _context.DocumentCategory.SingleOrDefault(m => m.ID == id);
            targetEntity.DeleteDate = DateTime.Now;
            targetEntity.DeleteBy = currentUser;
            _context.SaveChanges();
            result.DeleteSucceed();
            return result;
        }

        public DocumentCategoryViewModel GetDocumentCategoryById(int id)
        { 
            var singleCategory = _context.DocumentCategory.Select(m => new DocumentCategoryViewModel() { ID = m.ID, CategoryName = m.CategoryName, CategoryDescription = m.CategoryDescription}).SingleOrDefault(m => m.ID==id);
            return singleCategory;
        }
    }
}
