﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.RoleMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Master
{
    public interface IMasterRoleMenuProvider : IBaseMasterProvider<MasterRoleMenuViewModel>
    {
        List<DropdownList> GetListRole();
        List<DropdownList> GetListMenu();
    }
    public class MasterRoleMenuProvider : IMasterRoleMenuProvider
    {
        DigitalSignContext context;

        public MasterRoleMenuProvider(DigitalSignContext context)
        {
            this.context = context;
        }

        public ProcessResult Delete(int id)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var roleMenu = context.RoleMenu.SingleOrDefault(masterbranch => masterbranch.ID == id);
                roleMenu.DelDate = DateTime.Now;
                context.SaveChanges();
                result.ProcessSucceed("Data has been deleted !!!");
            }
            catch (Exception ex)
            {

                result.ProcessFailed(ex.Message);
            }
            return result;
        }

        public IQueryable<MasterRoleMenuViewModel> GetAll()
        {
            try
            {
                var query = from roleMenu in context.RoleMenu
                            join role in context.Role on roleMenu.RoleID equals role.ID
                            join menu in context.Menu on roleMenu.MenuID equals menu.ID
                            where !roleMenu.DelDate.HasValue
                            select new MasterRoleMenuViewModel
                            {
                                ID = roleMenu.ID,
                                RoleID = roleMenu.RoleID,
                                RoleName = role.Name,
                                MenuID = roleMenu.MenuID,
                                Menu = menu.Title
                            };
                return query;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public MasterRoleMenuViewModel GetById(int roleMenuID)
        {
            try
            {
                var query = (from roleMenu in context.RoleMenu
                             join role in context.Role on roleMenu.RoleID equals role.ID
                             join menu in context.Menu on roleMenu.MenuID equals menu.ID
                             where roleMenu.ID == roleMenuID
                             select new MasterRoleMenuViewModel
                             {
                                 ID = roleMenu.ID,
                                 RoleID = roleMenu.RoleID,
                                 RoleName = role.Name,
                                 MenuID = roleMenu.MenuID,
                                 Menu = menu.Title
                             }).SingleOrDefault();
                return query;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ProcessResult Save(MasterRoleMenuViewModel model, string displayName)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                RoleMenu roleMenu = context.RoleMenu.SingleOrDefault(a => a.ID == model.ID);

                if (roleMenu != null)
                {
                    roleMenu.RoleID = model.RoleID.Value;
                    roleMenu.MenuID = model.MenuID.Value;
                    //create by, update by apakah perlu?
                    EntityHelper.SetAuditFieldsForUpdate(roleMenu, displayName);
                }
                else
                {
                    roleMenu = new RoleMenu();
                    roleMenu.RoleID = model.RoleID.Value;
                    roleMenu.MenuID = model.MenuID.Value;

                    EntityHelper.SetAuditFieldsForInsert(roleMenu, displayName);
                    context.RoleMenu.Add(roleMenu);
                }
                context.SaveChanges();
                result.InsertSucceed();

            }
            catch (Exception ex)
            {

                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public List<DropdownList> GetListRole()
        {

            var query = from role in context.Role
                        where role.IsActive == true
                        select new DropdownList
                        {
                            Text = role.Name,
                            Value = role.ID.ToString()
                        };

            return query.ToList();
        }
        public List<DropdownList> GetListMenu()
        {

            var query = from menu in context.Menu
                        where menu.IsActive == true
                        select new DropdownList
                        {
                            Text = menu.Title,
                            Value = menu.ID.ToString()
                        };

            return query.ToList();
        }
    }
}
