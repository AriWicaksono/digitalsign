﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Master
{
    public interface IMasterMenuProvider : IBaseMasterProvider<MasterMenuViewModel>
    {
        IEnumerable<Menu> GetParentLookUp(int menuID);
    }

    public class MasterMenuProvider : IMasterMenuProvider
    {
        DigitalSignContext context;

        public MasterMenuProvider(DigitalSignContext context)
        {
            this.context = context;
        }

        public IQueryable<MasterMenuViewModel> GetAll()
        {
            try
            {
                var query = from menu in context.Menu
                            where !menu.DelDate.HasValue
                            select new MasterMenuViewModel
                            {
                                ID = menu.ID,
                                Title = menu.Title,
                                NavigationTo = menu.NavigationTo,
                                Seq = menu.Seq,
                                ParentMenuID = menu.ParentMenuID,
                                IsRoot = menu.IsRoot,
                                IsActive = menu.IsActive,
                                Icon = menu.Icon ?? "",
                                Description = menu.Description
                            };
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MasterMenuViewModel GetById(int id)
        {
            try
            {
                var query = (from menu in context.Menu
                             where menu.ID == id
                             select new MasterMenuViewModel
                             {
                                 ID = menu.ID,
                                 Title = menu.Title,
                                 NavigationTo = menu.NavigationTo,
                                 Seq = menu.Seq,
                                 ParentMenuID = menu.ParentMenuID,
                                 IsRoot = menu.IsRoot,
                                 IsActive = menu.IsActive,
                                 Icon = menu.Icon ?? "",
                                 Description = menu.Description
                             }).SingleOrDefault();
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProcessResult Save(MasterMenuViewModel model, string displayName)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                Menu menu = context.Menu.SingleOrDefault(m => m.ID == model.ID);

                if (menu != null)
                {
                    menu.Title = model.Title;
                    menu.NavigationTo = model.NavigationTo;
                    menu.Seq = model.Seq;
                    menu.ParentMenuID = model.ParentMenuID;
                    menu.IsRoot = model.IsRoot;
                    menu.IsActive = model.IsActive;
                    menu.Icon = model.Icon;
                    menu.Description = model.Description;
                    EntityHelper.SetAuditFieldsForUpdate(menu, displayName);
                }
                else
                {
                    menu = new Menu();
                    menu.Title = model.Title;
                    menu.NavigationTo = model.NavigationTo;
                    menu.Seq = model.Seq;
                    menu.ParentMenuID = model.ParentMenuID;
                    menu.IsRoot = model.IsRoot;
                    menu.IsActive = model.IsActive;
                    menu.Icon = model.Icon;
                    menu.Description = model.Description;
                    EntityHelper.SetAuditFieldsForInsert(menu, displayName);
                    context.Menu.Add(menu);
                }

                context.SaveChanges();
                result.InsertSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public ProcessResult Delete(int id)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var menu = context.Menu.SingleOrDefault(masterbranch => masterbranch.ID == id);
                menu.IsActive = false;
                menu.DelDate = DateTime.Now;
                context.SaveChanges();
                result.ProcessSucceed("Data has been deleted !!!");
            }
            catch (Exception ex)
            {
                result.ProcessFailed(ex.Message);
            }
            return result;
        }

        public IEnumerable<Menu> GetParentLookUp(int menuID)
        {
            var query = context.Menu.Where(menu => menu.ParentMenuID != menuID && menu.DelDate == null);
            return query.ToList().OrderBy(menu => menu.Title);
        }
    }
}
