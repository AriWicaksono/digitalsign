﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Security;
using RestSharp.Extensions;

namespace DigitalSign.Provider.Master
{
    public interface IDocumentSignProvider : IBaseMasterProvider<Document>
    {
        IQueryable<DocumentSignViewModel> GetData(int DocId, int UserId);
        ProcessResult AddDocumentSign(DocumentSignViewModel model);
        ProcessResult DeleteDocumentSign(int id);
        RequestSignViewModel GetDataRequest(int DocId);
        ProcessResult SubmitMaterai(DocumentMateraiViewModel viewModel);
        ProcessResult SaveMaterai(MateraiResult model, int docId, bool isRestamp, string currentUser);
    }
	public class DocumentSignProvider : IDocumentSignProvider
    {
        DigitalSignContext context;

        public DocumentSignProvider(DigitalSignContext context)
        {
            this.context = context;
        }

        public IQueryable<DocumentSignViewModel> GetData(int DocId, int UserId)
        {
            try
            {
                var query = from ds in context.DocumentSign
                            where ds.DocumentID == DocId && ds.UserID == UserId 
                            select new DocumentSignViewModel
                            {
                                ID = ds.ID,
                                DocumentID = ds.DocumentID,
                                UserID = ds.UserID,
                                Width = ds.Width,
                                Height = ds.Height,
                                CoorX = ds.CoorX,
                                CoorY = ds.CoorY,
                                PageNumber = ds.PageNumber
                            };
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProcessResult AddDocumentSign(DocumentSignViewModel model)
        {
            ProcessResult result = new ProcessResult();
            try
            {
				if (model.ID == null)
				{
                    var entity = new DocumentSign()
                    {
                        DocumentID = model.DocumentID,
                        UserID = model.UserID,
                        Width = model.Width,
                        Height = model.Height,
                        CoorX = model.CoorX,
                        CoorY = model.CoorY,
                        PageNumber = model.PageNumber
                    };
                    context.DocumentSign.Add(entity);
                    context.SaveChanges();
                    result.InsertSucceed();
				}
				else
				{
                    var targetEntity = context.DocumentSign.SingleOrDefault(m => m.ID == model.ID);
                    targetEntity.Width = model.Width;
                    targetEntity.Height = model.Height;
                    targetEntity.CoorX = model.CoorX;
                    targetEntity.CoorY = model.CoorY;
                    targetEntity.PageNumber = model.PageNumber;
                    context.SaveChanges();
                    result.UpdateSucceed();
                }

                
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public ProcessResult DeleteDocumentSign(int id)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var entity = context.DocumentSign.SingleOrDefault(x => x.ID == id);
                context.DocumentSign.Remove(entity);
                context.SaveChanges();
                result.DeleteSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        private static string SplitFile(string file)
		{
            return file.Split(',')[1];
		}

        public RequestSignViewModel GetDataRequest(int DocId)
        {
            try
            {
                var getDocument = context.Document.SingleOrDefault(x => x.ID == DocId);

                var dataReq = new RequestSignViewModel()
                {
                    request_id = getDocument.requestID,
                    send_email = true,
                    list_pdf = new List<document>()
                };

                var query1 = (from doc in context.Document
                              where doc.ID == DocId
                              select new document
                              {
                                  file_name = doc.DocumentName,
                                  file = doc.DocumentFile,
                                  //file = SplitFile(doc.DocumentFile),
                                  signatures = new List<signatureConfig>()
                              }
                            ).ToList();

                var query2 = (from user in context.Users
                              join userIden in context.UsersIdentity on user.ID equals userIden.UserID
                              join docPart in context.DocumentParticipant on user.ID equals docPart.UserID
                              where docPart.DocumentID == DocId
                              orderby docPart.Sequence ascending
                              select new signature
                              {
                                  user_identifier = user.TilakaName,
                                  signature_image = SplitFile(userIden.Signature),
                                  //signature_image = SplitFile(userIden.Signature),
                                  sequence = docPart.Sequence
                              }
                            ).ToList();

                var query3 = (from docSign in context.DocumentSign
                              join user in context.Users on docSign.UserID equals user.ID
                              join docPart in context.DocumentParticipant on new { docSign.DocumentID, docSign.UserID } equals new { docPart.DocumentID, docPart.UserID }
                              where docSign.DocumentID == DocId
                              select new signatureConfig
                              {
                                  user_identifier = user.TilakaName,
                                  width = docSign.Width,
                                  height = docSign.Height,
                                  coordinate_x = docSign.CoorX,
                                  coordinate_y = docSign.CoorY,
                                  page_number = docSign.PageNumber
                              }
                            ).ToList();

                dataReq.list_pdf = query1;
                dataReq.signatures = query2;
                query1[0].signatures = query3;
                return dataReq;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProcessResult SubmitMaterai(DocumentMateraiViewModel viewModel)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                if (viewModel.ID == 0)
                {
                    var entity = new DocumentMaterai()
                    {
                        //DocumentID = viewModel.DocumentID,
                        DocumentType = viewModel.DocumentType,
                        Reason = viewModel.Reason,
                        PageNumber = viewModel.Page,
                        Location = viewModel.Location,
                        LowerLeftX = viewModel.VisLLX,
                        LowerLeftY = viewModel.VisLLY,
                        UpperRightX = viewModel.VisURX,
                        UpperRightY = viewModel.VisURY,
                        CreatedBy = viewModel.UserLogin,
                        CreatedDate = DateTime.Now,
                    };
                    context.DocumentMaterai.Add(entity);
                    context.SaveChanges();
                    result.InsertSucceed();
                    result.value = entity.ID.ToString();
                }
                else
                {
                    var targetEntity = context.DocumentMaterai.SingleOrDefault(m => m.ID == viewModel.ID);
                        targetEntity.DocumentType = viewModel.DocumentType;
                        targetEntity.Reason = viewModel.Reason;
                        targetEntity.PageNumber = viewModel.Page;
                        targetEntity.Location = viewModel.Location;
                        targetEntity.LowerLeftX = viewModel.VisLLX;
                        targetEntity.LowerLeftY = viewModel.VisLLY;
                        targetEntity.UpperRightX = viewModel.VisURX;
                        targetEntity.UpperRightY = viewModel.VisURY;
                    targetEntity.UpdatedBy = viewModel.UserLogin;
                    targetEntity.UpdatedDate = DateTime.Now;
                    context.SaveChanges();
                    result.UpdateSucceed();
                    result.value = targetEntity.ID.ToString();
                }


            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public ProcessResult SaveMaterai(MateraiResult model, int docId, bool isRestamp, string currentUser) 
        {
            ProcessResult result = new ProcessResult();
            var targetEntity = context.DocumentMaterai.SingleOrDefault(m => m.ID == docId);
            if (targetEntity != null)
            {
                result.value = targetEntity.ID.ToString();
                if (model.materai_img != null)
                {
                    targetEntity.Materai = model.materai_img;
                    targetEntity.SerialNumber = model.serial_number;
                }
                if (isRestamp)
                {
                    var getMaxRestamp = context.GlobalConfiguration.SingleOrDefault(m => m.KeyName == "RestampMaxCount");
                    if (Convert.ToInt32(getMaxRestamp.KeyValue) == targetEntity.RestampCount)
                    {
                        result.isSucceed = false;
                        result.message = "Failed to stamp and restamp document!";
                        return result;
                    }

                    targetEntity.RestampCount = targetEntity.RestampCount.HasValue ? targetEntity.RestampCount + 1 : 1;
                }
                if (model.result_doc != null)
                {
                    targetEntity.StampedDoc = model.result_doc;

                }
                targetEntity.UpdatedBy = currentUser;
                targetEntity.UpdatedDate = DateTime.Now;
                context.SaveChanges();
                result.UpdateSucceed();
            }
            return result;
        }

        public IQueryable<Document> GetAll()
        {
            throw new NotImplementedException();
        }

        public Document GetById(int id)
        {
            throw new NotImplementedException();
        }

        public ProcessResult Save(Document model, string displayName)
        {
            throw new NotImplementedException();
        }

        public ProcessResult Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
