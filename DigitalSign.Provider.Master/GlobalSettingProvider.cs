﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.ViewModels.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Master
{
	public interface IGlobalSettingProvider
	{
		int LogTilaka(LogTilakaViewModel viewModel, int? id = null);

		void LogTilaka(List<LogTilakaViewModel> listLog);

	}
	public class GlobalSettingProvider : IGlobalSettingProvider
	{
		private readonly DigitalSignContext _context;
		public GlobalSettingProvider(DigitalSignContext context)
		{
			_context = context;
		}

		public int LogTilaka(LogTilakaViewModel viewModel, int? id = null)
		{
			TilakaApiLog entity = null;

			if (id == null)
			{
				entity = new TilakaApiLog()
				{
					ApiName = viewModel.ApiName,
					EndPoint = viewModel.EndPoint,
					Method = viewModel.Method,
					RequestBy = viewModel.RequestBy,
					RequestStart = viewModel.RequestStart,
					ReffID = viewModel.ReffID,
					ResponseStatus = viewModel.ResponseStatus,
				};
				_context.TilakaApiLog.Add(entity);
			}
			else
			{
				entity = _context.TilakaApiLog.SingleOrDefault(m => m.ID == id);
				if (entity != null)
				{
					entity.ResponseStatus = viewModel.ResponseStatus;
					if (viewModel.ReffID != null)
					{
						entity.ReffID = viewModel.ReffID;
					}
					if (viewModel.ResponseStatus == ResponseStatus.Processing)
					{
						entity.RequestBody = viewModel.RequestBody;
					}
					else if (viewModel.ResponseStatus == ResponseStatus.Error)
					{
						entity.ErrorMessage = viewModel.ErrorMessage;
					}
					else
					{
						entity.RequestEnd = viewModel.RequestEnd;
						entity.ResponseBody = viewModel.ResponseBody;
					}
				}
			}
			_context.SaveChanges();
			return entity.ID;
		}

		public void LogTilaka(List<LogTilakaViewModel> listLog)
		{
			foreach (var viewModel in listLog)
			{
				var entity = new TilakaApiLog()
				{
					ApiName = viewModel.ApiName,
					EndPoint = viewModel.EndPoint,
					Method = viewModel.Method,
					RequestBody = viewModel.RequestBody,
					RequestBy = viewModel.RequestBy,
					RequestEnd = viewModel.RequestEnd,
					RequestStart = viewModel.RequestStart,
					ResponseBody = viewModel.ResponseBody,
				};
				_context.TilakaApiLog.Add(entity);
			}
			_context.SaveChanges();
		}

	}
}
