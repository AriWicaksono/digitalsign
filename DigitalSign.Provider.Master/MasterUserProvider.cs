﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.User;
using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Master
{
    public interface IMasterUserProvider : IBaseMasterProvider<MasterUserViewModel> {
        List<Dropdown> GetListRole();
        IQueryable<MasterUserViewModel> GetForDropdownList();
        ProcessResult UpdateAccountInfo(AccountInfoUserViewModel model);
        ProcessResult ChangePassword(ChangePasswordUserViewModel model);
        ProcessResult ChangeRegistrationID(ChangeRegIdViewModel model);
        ProcessResult UpdateTilakaName(string requestId, string tilakaName);
        ProcessResult UploadCertificate(UploadCertificateViewModel model);
        ProcessResult CreateSign(CreateSignViewModel model);
        ProcessResult ResetSignature(int id);
        AjaxViewModel RegisterAdmin(RegisterAdminInsertViewModel viewModel, string currentUser);
    }
    public class MasterUserProvider : IMasterUserProvider
    {
        DigitalSignContext context;
        ActivityLogProvider activityLogProvider;

        public MasterUserProvider(DigitalSignContext context, ActivityLogProvider activityLogProvider)
        {
            this.context = context;
            this.activityLogProvider = activityLogProvider;
        }

        public IQueryable<MasterUserViewModel> GetAll()
        {
            try
            {
                var query = from user in context.Users
                            join identity in context.UsersIdentity on user.ID equals identity.UserID
                            where !user.DelDate.HasValue
                            select new MasterUserViewModel
                            {
                                ID = user.ID,
                                Username = user.Username,
                                Fullname = $"{identity.FirstName} {identity.MiddleName} {identity.LastName}".Replace("  "," "),
                                Email = user.Email,
                                CompanyName = user.CompanyName,
                                RoleID = user.RoleID,
                                RoleName = user.Role.Name,
                                Status = user.Status,
                                IsActive = user.IsActive,
                                Certificate = identity.Certificate,
                                Signature = identity.Signature,
                                TilakaName = user.TilakaName
                            };
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable<MasterUserViewModel> GetForDropdownList()
        {
            try
            {
                var query = from user in context.Users
                            join identity in context.UsersIdentity on user.ID equals identity.UserID
                            where !user.DelDate.HasValue && user.IsActive == true && (user.Status == "Registered" || user.Status == "LinkedAccount")
                            select new MasterUserViewModel
                            {
                                ID = user.ID,
                                Fullname = $"{identity.FirstName} {identity.MiddleName} {identity.LastName}".Replace("  ", " "),
                                Email = user.Email
                            };
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProcessResult UpdateTilakaName(string requestId, string tilakaName)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var user = context.Users.SingleOrDefault(a => a.Registration_Id == requestId);
                if (user != null)
                {
                    user.TilakaName = tilakaName;
                    user.Status = "LinkedAccount";
                    context.SaveChanges();
                    result.ProcessSucceed("Link Account Berhasil Diupdate");
                }
                else
                {
                    result.ProcessFailed("User Tidak ditemukan");
                }
                
            }
            catch (Exception e)
            {
                result.ProcessFailed(e.Message);
                //throw;
            }
            return result;
        }

        public ProcessResult ChangeRegistrationID(ChangeRegIdViewModel model)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var userIdn = context.UsersIdentity.SingleOrDefault(a => a.NIK == model.NIK);
                var user = context.Users.SingleOrDefault(a => a.ID == userIdn.UserID);
                user.Status = "Registered".ToString();
                user.Registration_Id = model.RegistrationId;
                context.SaveChanges();
                result.ProcessSucceed("Registration_id berhasil diubah");
            }
            catch (Exception)
            {
                result.ProcessFailed("Registration_id gagal diubah");
                //throw;
            }
            return result;
        }

        public MasterUserViewModel GetById(int id)
        {
            try
            {
                var query = (from user in context.Users
                             join identity in context.UsersIdentity on user.ID equals identity.UserID
                             where user.ID == id
                             select new MasterUserViewModel
                             {
                                 ID = user.ID,
                                 RegistrationID = user.Registration_Id,
                                 Username = user.Username,
                                 Email = user.Email,
                                 PhoneNumber = user.PhoneNumber,
                                 CompanyName = user.CompanyName,
                                 RoleID = user.RoleID,
                                 Status = user.Status,
                                 IsActive = user.IsActive,
                                 FirstName = identity.FirstName,
                                 MiddleName = identity.MiddleName,
                                 LastName = identity.LastName,
                                 BirthPlace = identity.BirthPlace,
                                 NIK = identity.NIK,
                                 Address = identity.Address,
                                 DateExpire = user.DateExpire,
                                 BirthDate = identity.BirthDate,
                                 KTPFile = identity.KTPFile,
                                 Fullname = $"{identity.FirstName} {identity.MiddleName} {identity.LastName}".Replace("  ", " "),
                                 Password = user.Password,
                                 PhotoFile = identity.PhotoFile,
                                 Signature = identity.Signature,
                                 Certificate = identity.Certificate,
                                 TilakaName = user.TilakaName
                             }).SingleOrDefault();
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProcessResult Save(MasterUserViewModel model, string displayName)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                context.Database.BeginTransaction();
                var user = context.Users.FirstOrDefault(m => m.ID == model.ID);
                var identity = context.UsersIdentity.FirstOrDefault(m => m.UserID == model.ID);
                user.IsActive = model.IsActive;
                user.Status = model.Status;
                user.RoleID = model.RoleID;
                EntityHelper.SetAuditFieldsForInsert(user, displayName);
                context.SaveChanges();
                context.Database.CommitTransaction();

                result.UpdateSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
                context.Database.RollbackTransaction();
            }
            return result;
        }

        public ProcessResult Delete(int id)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var user = context.Users.SingleOrDefault(m => m.ID == id);
                var identity = context.UsersIdentity.SingleOrDefault(m => m.UserID == id);
                user.DelDate = DateTime.Now;
                identity.DelDate = DateTime.Now;
                context.SaveChanges();
                result.ProcessSucceed("Data has been deleted !!!");
            }
            catch (Exception ex)
            {
                result.ProcessFailed(ex.Message);
                throw;
            }
            return result;
        }

        public List<Dropdown> GetListRole()
        {

            var query = from role in context.Role
                        where role.IsActive == true
                        select new Dropdown
                        {
                            Text = role.Name,
                            Value = role.ID.ToString()
                        };

            return query.ToList();
        }

        public ProcessResult UpdateAccountInfo(AccountInfoUserViewModel model)
        {
            context.Database.BeginTransaction();
            ProcessResult result = new ProcessResult();
            try
            {
                var user = context.Users.SingleOrDefault(m => m.ID == model.ID);
                var userIden = context.UsersIdentity.SingleOrDefault(m => m.UserID == model.ID);
				if (!string.IsNullOrEmpty(model.PhotoFile))
				{
                    userIden.PhotoFile = model.PhotoFile;
				}
                user.Username = model.Username;
                user.Email = model.Email;
                user.CompanyName = model.CompanyName;
                user.PhoneNumber = model.PhoneNumber;
                EntityHelper.SetAuditFieldsForInsert(user, model.Username);
                result.UpdateSucceed();
                context.SaveChanges();
                if (result.isSucceed)
                {
                    activityLogProvider.InsertActivityLog(model.ID, "Update Account Info", "");
                }
                context.Database.CommitTransaction();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
                context.Database.RollbackTransaction();
            }
            return result;
        }

        public ProcessResult ChangePassword(ChangePasswordUserViewModel model)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var user = context.Users.SingleOrDefault(m => m.ID == model.ID);

                if (EncryptionHelper.Decrypt(user.Password, EncryptionHelper.ENCRYPTION_PASSWORD) == model.CurrentPassword)
                {
                    user.Password = EncryptionHelper.Encrypt(model.NewPassword, EncryptionHelper.ENCRYPTION_PASSWORD);  
                    EntityHelper.SetAuditFieldsForInsert(user, user.Username);
                    result.ProcessSucceed("Success To Change Password!");
                    context.SaveChanges();
					if (result.isSucceed)
					{
                        activityLogProvider.InsertActivityLog(model.ID, "Change Password", "");
					}
                }
                else
                {
                    result.ProcessFailed("Failed To Change Password! Current password inputted is not match!");
                }
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public ProcessResult UploadCertificate(UploadCertificateViewModel model)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var userIden = context.UsersIdentity.SingleOrDefault(m => m.UserID == model.ID);
                userIden.Certificate = model.Certificate;
                EntityHelper.SetAuditFieldsForInsert(userIden, model.Username);
                context.SaveChanges();
                result.UpdateSucceed();
                if (result.isSucceed)
                {
                    activityLogProvider.InsertActivityLog(model.ID, "Upload Certificate", "");
                }
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public ProcessResult CreateSign(CreateSignViewModel model)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var userIden = context.UsersIdentity.SingleOrDefault(m => m.UserID == model.ID);
                userIden.Signature = model.Signature;
                EntityHelper.SetAuditFieldsForInsert(userIden, model.Username);
                context.SaveChanges();
                result.UpdateSucceed();
                if (result.isSucceed)
                {
                    activityLogProvider.InsertActivityLog(model.ID, "Create Sign", "");
                }
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public ProcessResult ResetSignature(int id)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var userIden = context.UsersIdentity.SingleOrDefault(m => m.UserID == id);
                userIden.Signature = null;
                context.SaveChanges();
                result.UpdateSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public AjaxViewModel RegisterAdmin(RegisterAdminInsertViewModel registerModel, string currentUser) {
            var ajaxVm = new AjaxViewModel();
            try
            {
                var checkEmail = context.Users.FirstOrDefault(m => m.Email.ToLower() == registerModel.Email.ToLower());
                if (checkEmail != null)
                {
                    ajaxVm.SetValues(false, null, "Email sudah digunakan!");
                    return ajaxVm;
                }
                var checkUsername = context.Users.FirstOrDefault(m => m.Username.ToLower() == registerModel.Username.ToLower());
                if (checkUsername != null)
                {
                    ajaxVm.SetValues(false, null, "Username sudah digunakan!");
                    return ajaxVm;
                }
                
                context.Database.BeginTransaction();
                var user = new Users();
                user.RoleID = Convert.ToInt32(registerModel.RoleID);
                user.Username = registerModel.Username;
                //Kurang BCrypt
                user.Password = EncryptionHelper.Encrypt(registerModel.Password, EncryptionHelper.ENCRYPTION_PASSWORD);
                user.DateExpire = DateTime.Now.AddYears(1);
                user.Email = registerModel.Email;
                user.PhoneNumber = registerModel.PhoneNumber;
                user.CompanyName = registerModel.CompanyName;
                user.Code = registerModel.Code;
                user.IsActive = true;
                user.Status = "Aktif";
                EntityHelper.SetAuditFieldsForInsert(user, currentUser);
                context.Users.Add(user);
                context.SaveChanges();


                //Get UserID
                var lastUser = context.Users.SingleOrDefault(a => a.Username == registerModel.Username);

                var identity = new UsersIdentity();
                identity.UserID = lastUser.ID;
                identity.FirstName = registerModel.FirstName;
                identity.MiddleName = registerModel.MidleName;
                identity.LastName = registerModel.LastName;
                identity.NIK = registerModel.NIK;
                identity.BirthPlace = registerModel.BirthPlace;
                identity.BirthDate = registerModel.BirthDate;
                identity.Address = registerModel.Address;
                identity.KTPName = registerModel.KTPName;
                identity.KTPFile = registerModel.KTPFile;
                identity.PhotoFile = registerModel.PhotoFile;
                identity.PhotoName = registerModel.PhotoName;

                EntityHelper.SetAuditFieldsForInsert(identity, currentUser);
                context.UsersIdentity.Add(identity);
                context.SaveChanges();

                //saving role
                var roleUser = new RoleUser();
                roleUser.UserID = lastUser.ID;
                roleUser.RoleID = user.RoleID;
                EntityHelper.SetAuditFieldsForInsert(roleUser, currentUser);
                context.RoleUser.Add(roleUser);
                context.SaveChanges();

                context.Database.CommitTransaction();
                ajaxVm.SetValues(true, null, "Register Admin Success!");
            }
            catch (Exception ex)
            {
                context.Database.RollbackTransaction();
                ajaxVm.SetValues(false, null, ex.Message);
                if (ex.InnerException != null)
                {
                    ajaxVm.Message = ex.InnerException.Message;
                }
            }
            return ajaxVm;
        }
    }
}
