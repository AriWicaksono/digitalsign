﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using DigitalSign.ViewModels.Master.User;
using DigitalSign.ViewModels.Security;
using Kendo.Mvc.UI.Fluent;
using RestSharp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Master
{
    public interface IMateraiService : IBaseMasterProvider<MateraiViewModel>
    {
        List<DocumentMateraiNewViewModel> GetList(string curretUser);
        string GetGlobalConfigurationByKeyName(string keyName);
        AjaxViewModel SubmitStamping(DocumentMateraiNewViewModel viewModel);
        AjaxViewModel SaveMaterai(MateraiResult model, int docId, bool isRestamp, string currentUser);
        AjaxViewModel SaveMaterai(MateraiResult model, int docId, string currentUser);
        List<MateraiViewModel> GetListQuota(string currentUser);
        bool UpdateQuota(int materaiId, string currentUser);
        DocumentMateraiNewViewModel GetDocumentStampped(int docId);
        DocumentMateraiDetailViewModel GetDocument(int docId);
        List<LogTilakaViewModel> GetListLog(int id);
        AjaxViewModel UpdateStampLocation(DocumentMateraiDetailViewModel viewModel, string currentUser);
    }
    public class MateraiProvider : IMateraiService
    {
        DigitalSignContext context;
        public MateraiProvider(DigitalSignContext context)
        {
            this.context = context;
        }
        public ProcessResult Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<MateraiViewModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public MateraiViewModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public ProcessResult Save(MateraiViewModel model, string displayName)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                context.Database.BeginTransaction();
                var entity = new Materai();
                entity.Quota = model.Quota;
                entity.TotalPrice = model.TotalPrice;
                entity.TotalPaid = model.TotalPaid;
                entity.TrxDate = model.TrxDate;
                entity.UserID = displayName;
                entity.QuotaUsed = 0;
                EntityHelper.SetAuditFieldsForInsert(entity, displayName);
                context.Materai.Add(entity);
                context.SaveChanges();
                context.Database.CommitTransaction();

                result.InsertSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
                context.Database.RollbackTransaction();
            }
            return result;
        }

        public AjaxViewModel SubmitStamping(DocumentMateraiNewViewModel viewModel)
        {
            var result = new AjaxViewModel();
            try
            {
                var getUser = context.Users.FirstOrDefault(m => m.Username == viewModel.UserLogin);
                context.Database.BeginTransaction();
                var entity = new DocumentMaterai()
                {
                    FileName = viewModel.FileName,
                    FilePath = viewModel.FilePath,
                    DocumentName = viewModel.DocumentName,
                    DocumentType = viewModel.DocumentType,
                    DocumentTypeCode = viewModel.DocumentTypeCode,
                    Location = viewModel.Location,
                    LowerLeftX = viewModel.VisLLX,
                    LowerLeftY = viewModel.VisLLY,
                    UpperRightX = viewModel.VisURX,
                    UpperRightY = viewModel.VisURY,
                    Reason = viewModel.Reason,
                    PageNumber = viewModel.Page,
                    MateraiID = 1,
                    StampDate = viewModel.StampDate,
                    Owner = getUser.ID
                };
                EntityHelper.SetAuditFieldsForInsert(entity, viewModel.UserLogin);
                context.DocumentMaterai.Add(entity);
                context.SaveChanges();
                context.Database.CommitTransaction();

                result.SetValues(true, entity.ID, "Success saving to database");
            }
            catch (Exception ex)
            {
                result.SetValues(false, null, ex.Message);
            }
            return result;
        }

        public AjaxViewModel SaveMaterai(MateraiResult model, int docId, bool isRestamp, string currentUser)
        {
            var result = new AjaxViewModel();
            var targetEntity = context.DocumentMaterai.SingleOrDefault(m => m.ID == docId);
            if (targetEntity != null)
            {
                result.Data = targetEntity.ID.ToString();
                if (isRestamp)
                {
                    var getMaxRestamp = GetGlobalConfigurationByKeyName("RestampMaxCount");
                    if (Convert.ToInt32(getMaxRestamp) == targetEntity.RestampCount)
                    {
                        result.isSucceed = false;
                        result.Message = "Failed to stamp and restamp document!";
                        return result;
                    }

                    targetEntity.RestampCount = targetEntity.RestampCount.HasValue ? targetEntity.RestampCount + 1 : 1;
                }
                if (!string.IsNullOrEmpty(model.materai_img))
                {
                    targetEntity.Materai = model.materai_img;
                }
                if (!string.IsNullOrEmpty(model.serial_number))
                {
                    targetEntity.SerialNumber = model.serial_number;
                }
                if (!string.IsNullOrEmpty(model.result_doc))
                {
                    targetEntity.StampedDoc = model.result_doc;
                    targetEntity.StampDate = DateTime.Now;

                }
                targetEntity.UpdatedBy = currentUser;
                targetEntity.UpdatedDate = DateTime.Now;
                context.SaveChanges();
                result.SetValues(true, targetEntity.ID, "Data has been saved!");
            }
            return result;
        }

        public AjaxViewModel SaveMaterai(MateraiResult model, int docId, string currentUser)
        {
            var result = new AjaxViewModel();
            var targetEntity = context.DocumentMaterai.SingleOrDefault(m => m.ID == docId);
            if (targetEntity != null)
            {
                result.Data = targetEntity.ID.ToString();
                targetEntity.RestampCount = targetEntity.RestampCount.HasValue ? targetEntity.RestampCount + 1 : 1;

                if (!string.IsNullOrEmpty(model.materai_img))
                {
                    targetEntity.Materai = model.materai_img;
                }
                if (!string.IsNullOrEmpty(model.serial_number))
                {
                    targetEntity.SerialNumber = model.serial_number;
                }
                if (!string.IsNullOrEmpty(model.result_doc))
                {
                    targetEntity.StampedDoc = model.result_doc;
                    targetEntity.StampDate = DateTime.Now;

                }
                targetEntity.UpdatedBy = currentUser;
                targetEntity.UpdatedDate = DateTime.Now;
                context.SaveChanges();
                result.SetValues(true, targetEntity.ID, "Data has been saved!");
            }
            return result;
        }
        public AjaxViewModel UpdateStampLocation(DocumentMateraiDetailViewModel viewModel, string currentUser) {
            var result = new AjaxViewModel();
            var targetEntity = context.DocumentMaterai.SingleOrDefault(m => m.ID == Convert.ToInt32(viewModel.ID));
            if (targetEntity != null)
            {
                targetEntity.LowerLeftX = viewModel.VisLLX;
                targetEntity.LowerLeftY = viewModel.VisLLY;
                targetEntity.UpperRightX = viewModel.VisURX;
                targetEntity.UpperRightY = viewModel.VisURY;
                targetEntity.PageNumber = viewModel.Page;

                targetEntity.UpdatedBy = currentUser;
                targetEntity.UpdatedDate = DateTime.Now;

                context.SaveChanges();
                result.SetValues(true, targetEntity.ID, "Data has been saved!");
            }
            return result;
        }
        public List<DocumentMateraiNewViewModel> GetList(string curretUser)
        {
            var getUserDetail = context.Users.FirstOrDefault(m => m.Username == curretUser);
            var getUserRole = context.RoleUser.FirstOrDefault(m => m.UserID == getUserDetail.ID);
            var getRoleDetail = context.Role.FirstOrDefault(m => m.ID == getUserRole.RoleID);
            if (getRoleDetail.Name == RoleUserRequester.SuperAdmin)
            {
                var query = (from docMaterai in context.DocumentMaterai
                             select new DocumentMateraiNewViewModel
                             {
                                 FileName = docMaterai.FileName,
                                 ID = docMaterai.ID.ToString(),
                                 DocumentName = docMaterai.DocumentName,
                                 Location = docMaterai.Location,
                                 SerialNumber = docMaterai.SerialNumber,
                                 StampDate = docMaterai.StampDate,
                                 DocumentType = docMaterai.DocumentType,
                                 CreatedDate = docMaterai.CreatedDate,
                                 LastSubmit = docMaterai.UpdatedDate,
                                 StampedDoc = docMaterai.StampedDoc//.HasValue() ? "Ada" : null,
                             }).OrderByDescending(m => m.CreatedDate).ToList();
                return query;
            }
            else 
            {
                var getListUserByRole = context.RoleUser.Where(m => m.RoleID == getUserRole.RoleID).Select(m => m.UserID).ToList();
                var query = (from docMaterai in context.DocumentMaterai
                             where getListUserByRole.Contains(docMaterai.Owner.GetValueOrDefault())
                             select new DocumentMateraiNewViewModel
                             {
                                 FileName = docMaterai.FileName,
                                 ID = docMaterai.ID.ToString(),
                                 DocumentName = docMaterai.DocumentName,
                                 Location = docMaterai.Location,
                                 SerialNumber = docMaterai.SerialNumber,
                                 StampDate = docMaterai.StampDate,
                                 DocumentType = docMaterai.DocumentType,
                                 CreatedDate = docMaterai.CreatedDate,
                                 LastSubmit = docMaterai.UpdatedDate,
                                 StampedDoc = docMaterai.StampedDoc//.HasValue() ? "Ada" : null,
                             }).OrderByDescending(m => m.CreatedDate).ToList();
                return query;
            }
        }

        public List<MateraiViewModel> GetListQuota(string currentUser)
        {
            var query = context.Materai.Where(m => m.UserID == currentUser && m.Quota - m.QuotaUsed > 0).Select(m => new MateraiViewModel
            {
                ID = m.ID,
                UserID = m.UserID,
                Quota = m.Quota - m.QuotaUsed,
                QuotaUsed = m.QuotaUsed,
                TotalPaid = m.TotalPaid,
                TotalPrice = m.TotalPrice,
                TrxDate = m.TrxDate
            });
            return query.ToList();
        }

        public bool UpdateQuota(int materaiId, string currentUser)
        {
            var entity = context.Materai.SingleOrDefault(m => m.ID == materaiId);
            if (entity != null)
            {
                entity.Quota = entity.Quota - 1;
                entity.QuotaUsed = entity.QuotaUsed + 1;
                EntityHelper.SetAuditFieldsForInsert(entity, currentUser);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public DocumentMateraiNewViewModel GetDocumentStampped(int docId)
        {
            var query = context.DocumentMaterai.Select(m =>
                        new DocumentMateraiNewViewModel()
                        {
                            ID = m.ID.ToString(),
                            DocumentType = m.DocumentType,
                            DocumentTypeCode = m.DocumentTypeCode,
                            DocumentName = m.DocumentName,
                            StampDate = m.StampDate,
                            StampedDoc = m.StampedDoc,
                            FileName = m.FileName,
                            FilePath = m.FilePath,
                            Reason = m.Reason,
                            Location = m.Location
                        }).SingleOrDefault(m => m.ID == docId.ToString());

            return query;
        }

        public DocumentMateraiDetailViewModel GetDocument(int docId)
        {
            var query = context.DocumentMaterai.Select(m =>
                        new DocumentMateraiDetailViewModel()
                        {
                            ID = m.ID.ToString(),
                            DocumentType = m.DocumentType,
                            DocumentTypeCode = m.DocumentTypeCode,
                            DocumentName = m.DocumentName,
                            StampDate = m.StampDate,
                            StampedDoc = m.StampedDoc,
                            FileName = m.FileName,
                            FilePath = m.FilePath,
                            Reason = m.Reason,
                            Location = m.Location,
                            Materai = m.Materai,
                            MateraiID = m.MateraiID,
                            Page = m.PageNumber,
                            SerialNumber = m.SerialNumber,
                            VisLLX = m.LowerLeftX,
                            VisLLY = m.LowerLeftY,
                            VisURX = m.UpperRightX,
                            VisURY = m.UpperRightY
                        }).SingleOrDefault(m => m.ID == docId.ToString());
            query.IsStampped = !string.IsNullOrEmpty(query.StampedDoc);
            return query;
        }

        public string GetGlobalConfigurationByKeyName(string keyName)
        {
            return context.GlobalConfiguration.FirstOrDefault(m => m.KeyName == keyName).KeyValue;
        }

        public List<LogTilakaViewModel> GetListLog(int id) 
        {
            var query = context.TilakaApiLog.Where(m => m.ReffID == id).Select(m => new LogTilakaViewModel
            {
                ApiName = m.ApiName,
                EndPoint = m.EndPoint,
                ID = m.ID.ToString(),
                ErrorMessage = m.ErrorMessage,
                Method = m.Method,
                ReffID = m.ReffID,
                RequestBy = m.RequestBy,
                ResponseStatus = m.ResponseStatus,
                RequestStart = m.RequestStart,
                RequestEnd = m.RequestEnd,
            }).ToList();
            return query;
        }
    }
}
