﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Master
{
    public interface IDocumentDiscussionProvider : IBaseMasterProvider<DocumentDiscussion> {
        IQueryable<DocumentDiscussionViewModel> GetDiscussion(int DocumentID);
        ProcessResult AddDiscussion(DocumentDiscussionViewModel model);

    }
    public class DocumentDiscussionProvider : IDocumentDiscussionProvider
    {
        private readonly DigitalSignContext context;

        public DocumentDiscussionProvider(DigitalSignContext context)
        {
            this.context = context;
        }

        public IQueryable<DocumentDiscussionViewModel> GetDiscussion(int DocumentID)
        {
            var dis = (from docdis in context.DocumentDiscussion
                       join user in context.UsersIdentity on docdis.UserID equals user.UserID
                       where docdis.DocumentID == DocumentID
                       orderby docdis.ID descending
                       select new DocumentDiscussionViewModel
                       {
                           ID = docdis.ID,
                           UserID = docdis.UserID,
                           TimeStamp = docdis.TImeStamp,
                           Message = docdis.Message,
                           Poto = user.PhotoFile,
                           FullName = $"{user.FirstName} {user.MiddleName} {user.LastName}".Replace("  ", " ")
                       });
            return dis;
        }

        public ProcessResult AddDiscussion(DocumentDiscussionViewModel model)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var dis = new DocumentDiscussion
                {
                    DocumentID = model.DocumentID,
                    UserID = model.UserID,
                    Message = model.Message,
                    TImeStamp = DateTime.Now
                };
                context.DocumentDiscussion.Add(dis);
                context.SaveChanges();
                result.InsertSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public IQueryable<DocumentDiscussion> GetAll()
        {
            throw new NotImplementedException();
        }

        public DocumentDiscussion GetById(int id)
        {
            throw new NotImplementedException();
        }

        public ProcessResult Save(DocumentDiscussion model, string displayName)
        {
            throw new NotImplementedException();
        }

        public ProcessResult Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
