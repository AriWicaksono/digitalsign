﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Master.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Master
{
    public interface IMasterRoleProvider : IBaseMasterProvider<MasterRoleViewModel>
    {
    }
    public class MasterRoleProvider : IMasterRoleProvider
    {
        DigitalSignContext context;

        public MasterRoleProvider(DigitalSignContext context)
        {
            this.context = context;
        }

        public IQueryable<MasterRoleViewModel> GetAll()
        {
            try
            {
                var query = from role in context.Role
                            where !role.DelDate.HasValue
                            select new MasterRoleViewModel
                            {
                                ID = role.ID,
                                Code = role.Code,
                                Name = role.Name,
                                IsActive = role.IsActive,
                                IsAdmin = role.IsAdmin,
                                Description = role.Description
                            };
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MasterRoleViewModel GetById(int roleID)
        {
            try
            {
                var query = (from role in context.Role
                             where (role.ID == roleID)
                             && !role.DelDate.HasValue
                             select new MasterRoleViewModel
                             {
                                 ID = role.ID,
                                 Code = role.Code,
                                 Name = role.Name,
                                 IsActive = role.IsActive,
                                 IsAdmin = role.IsAdmin,
                                 Description = role.Description
                             }).SingleOrDefault();
                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProcessResult Save(MasterRoleViewModel model, string displayName)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                Role role = context.Role.SingleOrDefault(m => m.ID == model.ID);
                if (role != null)
                {
                    role.Name = model.Name;
                    role.Code = model.Code;
                    role.IsActive = model.IsActive;
                    role.Description = model.Description;
                    role.IsAdmin = model.IsAdmin;
                    EntityHelper.SetAuditFieldsForUpdate(role, displayName);
                }
                else
                {
                    role = new Role();
                    role.Name = model.Name;
                    role.Code = model.Code;
                    role.IsActive = model.IsActive;
                    role.Description = model.Description;
                    role.IsAdmin = model.IsAdmin;
                    EntityHelper.SetAuditFieldsForInsert(role, displayName);
                    context.Role.Add(role);
                }
                context.SaveChanges();
                result.InsertSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
            }
            return result;
        }

        public ProcessResult Delete(int id)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var role = context.Role.SingleOrDefault(master => master.ID == id);
                role.IsActive = false;
                role.DelDate = DateTime.Now;
                context.SaveChanges();
                result.ProcessSucceed("Data has been deleted !!!");
            }
            catch (Exception ex)
            {
                result.ProcessFailed(ex.Message);
            }
            return result;
        }        
    }
}
