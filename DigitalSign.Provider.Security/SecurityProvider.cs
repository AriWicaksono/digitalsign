﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.Utilities;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using DigitalSign.ViewModels.Master.User;

namespace DigitalSign.Provider.Security
{
    public interface ISecurityProvider
    {
        public IEnumerable<MenuViewModel> GetMenu(int? parentMenuID, string userName);
        bool IsAuthenticated(LoginRequestViewModel loginModel);
        MasterUserViewModel GetUser(LoginRequestViewModel loginModel);
        MasterUserViewModel GetUserRole(LoginRequestViewModel loginModel);
        ProcessResult VerifySendEmail(string nik);
        ProcessResult ForgotPasswordSendMail(string mail, string url);
        ProcessResult ActivationAccount(string nik, string code);
        object WhoAmI();
        string CreateToken(MasterUserViewModel user);
        public AjaxViewModel ConfirmResetPassword(string code, string email, DateTime timeClick);
        ProcessResult ResetPassword(string email, string newPassword);
	}

	public class SecurityProvider : ISecurityProvider
    {
        private readonly DigitalSignContext context;
        private readonly IConfiguration configuration;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly ActivityLogProvider activityLog;

        public SecurityProvider(DigitalSignContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, ActivityLogProvider activityLog)
        {
            this.context = context;
            this.configuration = configuration;
            this.httpContextAccessor = httpContextAccessor;
            this.activityLog = activityLog;
        }


        public bool IsAuthenticated(LoginRequestViewModel loginModel)
        {
            var userLogin = context.Users.SingleOrDefault(a => a.Username == loginModel.Username && a.DelDate == null && a.IsActive == true);
            if (userLogin != null)
            {
                if (EncryptionHelper.Decrypt(userLogin.Password, EncryptionHelper.ENCRYPTION_PASSWORD) == loginModel.Password)
                {
                    activityLog.InsertActivityLog(userLogin.ID, "Login", "");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        public ProcessResult VerifySendEmail(string nik)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var user = context.UsersIdentity
                    .Join(context.Users,
                        idn => idn.UserID,
                        usr => usr.ID,
                        (idn,user) => new { UsersIdentity = idn, Users = user })
                    .Where(a => a.UsersIdentity.NIK == nik)
                    .SingleOrDefault();
                var email = new MimeMessage();

                email.From.Add(MailboxAddress.Parse(DataConfiguration.Configuration.SmtpSettings.Email));
                email.To.Add(MailboxAddress.Parse(user.Users.Email));
                email.Subject = "Verification Email - Digital Sign";
                email.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                {
                    Text = $"<h2>Hello, {user.UsersIdentity.FirstName} {user.UsersIdentity.MiddleName} {user.UsersIdentity.LastName}</h2>" +
                        $"<p> please copy this code to verification your account</p>" +
                        $"<span bgcolor='#FFFFFF' style='padding:12px 24px;color:#666666;font-weight:400;display:inline-block;text-decoration:none;font-size:16px;line-height:1.25em;border-color:#A71F24;background-color:#ffffff;border-radius:34px;border-width:1px;border-style:solid'><strong>{user.Users.Code}</strong></span>"
                };

                using (var smtp = new SmtpClient())
                {
                    smtp.Connect("smtp.gmail.com", 587, MailKit.Security.SecureSocketOptions.StartTls);
                    smtp.Authenticate(DataConfiguration.Configuration.SmtpSettings.Email, DataConfiguration.Configuration.SmtpSettings.Password);
                    smtp.Send(email);
                    smtp.Disconnect(true);
                }
                result.ProcessSucceed("An email has been sent. Please check your inbox to recieve a activation code");
            }
            catch (Exception e)
            {
                result.ProcessFailed(e.Message);
            }
            return result;
        }

        public ProcessResult ForgotPasswordSendMail(string mail, string url)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var user = context.UsersIdentity
                    .Join(context.Users,
                        idn => idn.UserID,
                        usr => usr.ID,
                        (idn, user) => new { UsersIdentity = idn, Users = user })
                    .Where(a => a.Users.Email == mail)
                    .SingleOrDefault();

                if (user != null)
                {
                    var email = new MimeMessage();
					//var link = $"{url}/VerifyEmail?nik={user.NIK}";
					//var url = "https://localhost:7222/Auth/ForgotPassword";
                    var generateToken = EncryptionHelper.GetUniqueKeyOriginal_BIASED(24);
                    var updateToken = context.Users.SingleOrDefault(m => m.ID == user.Users.ID);
                    updateToken.ResetToken = generateToken;
                    updateToken.ResetTokenExp = DateTime.Now.AddHours(2);
                    context.SaveChanges();
                    var link = $"{url}?code={generateToken}&email={mail}";

                    email.From.Add(new MailboxAddress(DataConfiguration.Configuration.SmtpSettings.DisplayName, DataConfiguration.Configuration.SmtpSettings.Email));
                    email.To.Add(MailboxAddress.Parse(user.Users.Email));
                    email.Subject = "Reset Password - Digital Sign";
                    email.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                    {
                        Text = $"<h2>Hello, {user.UsersIdentity.FirstName} {user.UsersIdentity.MiddleName} {user.UsersIdentity.LastName}</h2>" +
                            $"<p> please click button below to reset your password</p>" +
                            $"<a href='{link}' bgcolor='#ffffff' style='padding:12px 24px;color:#ffffff;font-weight:400;display:inline-block;text-decoration:none;font-size:16px;line-height:1.25em;border-color:#3f51b5;background-color:#3f51b5;border-radius:34px;border-width:1px;border-style:solid'><strong> Reset Password </strong></a>" +
                            $"<p> this link will expired in 2 hours.</p>" 
                    };

                    using (var smtp = new SmtpClient())
                    {
                        smtp.Connect("smtp.gmail.com", 587, MailKit.Security.SecureSocketOptions.StartTls);
                        smtp.Authenticate(DataConfiguration.Configuration.SmtpSettings.Email, DataConfiguration.Configuration.SmtpSettings.Password);
                        smtp.Send(email);
                        smtp.Disconnect(true);
                    }
                    result.ProcessSucceed("An email has been sent. Please check your inbox to reset your password");
                }
                else
                {
                    result.ProcessFailed("Email isn't exist");
                }
                
            }
            catch (Exception e)
            {
                result.ProcessFailed(e.Message);
            }
            return result;
        }

        public ProcessResult ActivationAccount(string nik, string code)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var userIdn = context.UsersIdentity.SingleOrDefault(a => a.NIK == nik);
                var user = context.Users.SingleOrDefault(a => a.ID == userIdn.UserID);
                if (user.Code == code)
                {
                    user.Status = "Aktif";
                    context.SaveChanges();
                    result.ProcessSucceed($"Account {user.Username} berhasil di aktivasi");

                    var model = new RegisterTilakaViewModel();
                    model.registration_id = user.Registration_Id;
                    model.email = user.Email;
                    model.name = $"{userIdn.FirstName} {userIdn.MiddleName} {userIdn.LastName}";
                    model.company_name = user.CompanyName;
                    model.nik = userIdn.NIK;
                    model.photo_ktp = userIdn.KTPFile;
                    model.phone_number = user.PhoneNumber;
                    model.date_expire = user.DateExpire.Value.ToString("yyyy-MM-dd");
                    result.value = JsonConvert.SerializeObject(model);
                }
                else
                {
                    result.ProcessFailed($"Your code is wrong");
                }
                
            }
            catch (Exception e)
            {
                result.ProcessFailed($"account gagal diaktivasi \n {e.Message}");
                //throw;
            }
            return result;
        }

        public ProcessResult ResetPassword(string email, string newPassword)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                var user = context.Users.SingleOrDefault(a => a.Email == email);
                if (user != null)
                {
                    user.Password = Utilities.EncryptionHelper.Encrypt(newPassword, EncryptionHelper.ENCRYPTION_PASSWORD);
                    user.ResetToken = null;
                    user.ResetTokenExp = null;
                    context.SaveChanges();
                    result.ProcessSucceed("Reset password succesed");
                }
                else
                {
                    result.ProcessFailed("Your code is wrong");
                }
            }
            catch (Exception e)
            {
                result.ProcessFailed(e.Message);
            }
            return result;
        }

        public AjaxViewModel ConfirmResetPassword(string code, string email, DateTime timeClick)
        {
			AjaxViewModel result = new AjaxViewModel();
			try
			{
				var user = context.Users.SingleOrDefault(a => a.Email == email);
				if (user != null)
				{
                    if (user.ResetToken == code)
                    {
                        if (timeClick <= user.ResetTokenExp)
                        {
                            //user.ResetToken = null;
                            //user.ResetTokenExp = null;
                            //context.SaveChanges();
                            result.SetValues(true, null, "Proceed to next steps!");
                            var hasil = new ResetPasswordViewModel() { 
                                Code = code,
                                Email = email,
                            };
                            result.Data = JsonConvert.SerializeObject(hasil);
                        }
                        else {
                            result.SetValues(false, null, "Token is expired!");
                        }
                    }
                    else {
                        result.SetValues(false, null, "Token is not valid!");
                    }
				}
				else
				{
                    result.SetValues(false, null, "Email not found!");
				}
			}
			catch (Exception e)
			{
                result.SetValues(false, null, e.Message);
			}
			return result;
		}

        public string CreateToken(MasterUserViewModel user)
        {
            var roleName = context.Role.Where(x => x.ID == user.RoleID).SingleOrDefault();
            if (roleName == null)
            {
                roleName = new Role();
                roleName.Name = "Guest";
            }
            List<Claim> claims = new List<Claim>
            {
                new Claim("LoginID", user.ID.ToString()),
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Role, roleName.Name),
                new Claim("IsAdmin", roleName.IsAdmin.ToString())
            };
            var test = configuration.GetSection("Token:Key").Value;
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.GetSection("Token:Key").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                    claims: claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: creds
                );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }

        public MasterUserViewModel GetUser(LoginRequestViewModel loginModel)
        {
            var data = context.Users.Where(a => a.Username == loginModel.Username).SingleOrDefault();
            var user = new MasterUserViewModel();
            user.ID = data.ID;
            user.Username = data.Username;
            user.RegistrationID = data.Registration_Id;
            user.IsActive = data.IsActive;
            user.Password = data.Password;
            user.PhoneNumber = data.PhoneNumber;
            user.RoleID = data.RoleID;
            user.TilakaName = data.TilakaName;
            user.Status = data.Status;
            return user;
        }

        public MasterUserViewModel GetUserRole(LoginRequestViewModel loginModel)
        {
            var data = context.Users.Where(a => a.Username == loginModel.Username).SingleOrDefault();
            var user = new MasterUserViewModel();
            user.ID = data.ID;
            user.Username = data.Username;
            user.RegistrationID = data.Registration_Id;
            user.IsActive = data.IsActive;
            user.Password = data.Password;
            user.PhoneNumber = data.PhoneNumber;
            var getRole = context.RoleUser.FirstOrDefault(m => m.UserID == data.ID);
            if (getRole != null)
            {
                user.RoleID = getRole.RoleID;
            }
            user.TilakaName = data.TilakaName;
            user.Status = data.Status;
            return user;
        }

        public object WhoAmI()
        {
            ClaimResponseViewModel result = new ClaimResponseViewModel();

            if (httpContextAccessor.HttpContext.User.Claims.Count() > 0)
            {
                result.LoginID = Convert.ToInt32(httpContextAccessor.HttpContext.User.FindFirst("LoginId").Value);
                result.LoginName = httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Name).Value;
                result.RoleName = httpContextAccessor.HttpContext.User.FindFirst("RoleName").Value;
            }

            return result;
        }

        #region login
        public bool Validate(string userName)
        {
            try
            {
                var query = from emp in context.Users
                            where emp.Username == userName
                               && emp.IsActive
                            select emp;
                return query.Count() > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Menu
        public IEnumerable<MenuViewModel> GetMenu(int? parentMenuID, string userName)
        {
            try
            {
                IEnumerable<MenuViewModel> menus = null;
                int roleID = GetRoleID(userName);

                if (!parentMenuID.HasValue)
                    menus = from menu in context.Menu
                            from role in menu.RoleMenu
                            where !menu.ParentMenuID.HasValue && role.RoleID == roleID && menu.IsActive
                            orderby menu.Seq
                            //orderby menu.NavigationTo
                            select new MenuViewModel
                            {
                                ID = menu.ID,
                                Title = menu.Title,
                                NavigationTo = menu.NavigationTo,
                                Icon = menu.Icon
                            };
                else
                    menus = from menu in context.Menu
                            from role in menu.RoleMenu
                            where menu.ParentMenuID == parentMenuID.Value && role.RoleID == roleID && menu.IsActive
                            orderby menu.Seq
                            //orderby menu.NavigationTo
                            select new MenuViewModel
                            {
                                ID = menu.ID,
                                Title = menu.Title,
                                NavigationTo = menu.NavigationTo,
                                Icon = menu.Icon

                            };
                return menus;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private int GetRoleID(string userName)
        {
            try
            {
                return context.Users.FirstOrDefault(emp => emp.Username == userName).RoleID;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public IEnumerable<Menus> GetTree(int? parentMenuID, string userName)
        {
            try
            {
                IEnumerable<Menus> menus = null;
                int roleID = GetRoleID(userName);

                if (!parentMenuID.HasValue)
                    menus = from menu in context.Menu
                            from role in menu.RoleMenu
                            where !menu.ParentMenuID.HasValue && role.RoleID == roleID && menu.IsActive && (menu.DelDate.Value == null)
                            orderby menu.Seq
                            select new Menus
                            {
                                id = menu.ID,
                                Name = menu.Title,
                                hasChildren = menu.IsRoot
                            };
                else
                    menus = from menu in context.Menu
                            from role in menu.RoleMenu
                            where menu.ParentMenuID == parentMenuID.Value && role.RoleID == roleID && menu.IsActive && (menu.DelDate.Value == null)
                            orderby menu.Seq
                            select new Menus
                            {
                                id = menu.ID,
                                Name = menu.Title,
                                hasChildren = menu.IsRoot
                            };
                return menus;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public AccessMenuViewModel GetMenuAccess(int menuID)
        {
            try
            {
                AccessMenuViewModel model = new AccessMenuViewModel();
                IDictionary<RoleViewModel, bool> result = new Dictionary<RoleViewModel, bool>();
                var roles = context.Role.Where(o => o.IsActive == true).Select(r => new RoleViewModel
                {
                    ID = r.ID,
                    Name = r.Name,
                    IsActive = r.IsActive
                });

                var roleMenus = context.RoleMenu.Where(m => m.MenuID == menuID).Select(x => x.Role).Select(r => new RoleViewModel
                {
                    ID = r.ID,
                    Name = r.Name,
                    IsActive = r.IsActive
                });

                foreach (var role in roles)
                    result.Add(role, false);

                foreach (var roleMenu in roleMenus)
                {
                    var role = result.SingleOrDefault(r => r.Key.ID == roleMenu.ID);
                    result[role.Key] = true;
                }
                model.ToggleAccessMenu = result;
                model.MenuID = menuID;
                model.MenuTitle = context.Menu.SingleOrDefault(m => m.ID == menuID).Title;

                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void SetMenuAccess(int menuID, int[] roles)
        {
            try
            {
                var roleMenus = context.RoleMenu.FirstOrDefault(roleMenu => roleMenu.MenuID == menuID);
                //context.Delete(roleMenus);
                context.RoleMenu.Remove(roleMenus);
                context.SaveChanges();

                var currentMenu = context.Menu.SingleOrDefault(menu => menu.ID == menuID);

                foreach (int newRoleID in roles)
                {
                    Role newRole = context.Role.SingleOrDefault(role => role.ID == newRoleID);

                    RoleMenu newRoleMenu = new RoleMenu();
                    newRoleMenu.Menu = currentMenu;
                    newRoleMenu.Role = newRole;

                    context.RoleMenu.Add(newRoleMenu);
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Role
        public IEnumerable<RoleViewModel> GetRole()
        {
            return context.Role.Select(x => new RoleViewModel { ID = x.ID, Name = x.Name, IsAdmin = x.IsAdmin, IsActive = x.IsActive });
        }

        #endregion
    }
}
