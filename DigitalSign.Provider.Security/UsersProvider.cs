﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.Provider.Base;
using DigitalSign.ViewModels;
using DigitalSign.ViewModels.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using DigitalSign.Utilities;

namespace DigitalSign.Provider.Security
{
    public interface IUsersProvider : IBaseProvider<Users>
    {
        ProcessResult Register(RegisterInsertViewModel registerModel);
        ProcessResult CheckAccount(string nik);
    }
    public class UsersProvider : IUsersProvider
    {
        private readonly DigitalSignContext context;
        private readonly IConfiguration configuration;
        private readonly IHttpContextAccessor httpContextAccessor;

        public UsersProvider(DigitalSignContext context, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            this.context = context;
            this.configuration = configuration;
            this.httpContextAccessor = httpContextAccessor;
        }

        public ProcessResult CheckAccount(string nik)
        {
            var result = new ProcessResult();
            try
            {
                var check = context.UsersIdentity.Any(a => a.NIK == nik);
                if (check)
                {
                    result.ProcessSucceed("NIK tersedia");
                }
                result.ProcessSucceed("NIK Tidak tersedia");
            }
            catch (Exception e)
            {
                result.ProcessFailed(e.Message);
                //throw;
            }
            return result;
            
        }
        
        public ProcessResult Register(RegisterInsertViewModel registerModel)
        {
            ProcessResult result = new ProcessResult();
            
            try
            {
                var checkNik = CheckAccount(registerModel.NIK);
                if (!checkNik.isSucceed || checkNik.message == "NIK tersedia")
                {
                    return checkNik;
                }
                var checkEmail = context.Users.FirstOrDefault(m => m.Email.ToLower() == registerModel.Email.ToLower());
                if (checkEmail != null)
                {
                    result.isSucceed = false;
                    result.message = "Email sudah digunakan!";
                    return result;
                }
                var checkUsername = context.Users.FirstOrDefault(m => m.Username.ToLower() == registerModel.Username.ToLower());
                if (checkUsername != null) {
                    result.isSucceed = false;
                    result.message = "Username sudah digunakan!";
                    return result;
                }

                context.Database.BeginTransaction();
                var user = new Users();
                user.Registration_Id = registerModel.Registration_Id;
                user.Username = registerModel.Username;
                //Kurang BCrypt
                user.Password = EncryptionHelper.Encrypt(registerModel.Password, EncryptionHelper.ENCRYPTION_PASSWORD);
                user.DateExpire = DateTime.Now.AddYears(1);
                user.Email = registerModel.Email;
                user.PhoneNumber = registerModel.PhoneNumber;
                user.CompanyName = registerModel.CompanyName;
                user.Code = registerModel.Code;
                //Dev later
                user.RoleID = DataConfiguration.Configuration.RoleUserId;
                user.IsActive = true;
                EntityHelper.SetAuditFieldsForInsert(user, registerModel.Username);
                context.Users.Add(user);
                context.SaveChanges();

                //Get UserID
                var lastUser = context.Users.SingleOrDefault(a => a.Username == registerModel.Username);

                var identity = new UsersIdentity();
                identity.UserID = lastUser.ID;
                identity.FirstName = registerModel.FirstName;
                identity.MiddleName = registerModel.MidleName;
                identity.LastName = registerModel.LastName;
                identity.NIK = registerModel.NIK;
                identity.BirthPlace = registerModel.BirthPlace;
                identity.BirthDate = registerModel.BirthDate;
                identity.Address = registerModel.Address;
                identity.KTPName = registerModel.KTPName;
                identity.KTPFile = registerModel.KTPFile;
                identity.PhotoFile = registerModel.PhotoFile;
                identity.PhotoName = registerModel.PhotoName;

                EntityHelper.SetAuditFieldsForInsert(identity, registerModel.Username);
                context.UsersIdentity.Add(identity);
                context.SaveChanges();

                context.Database.CommitTransaction();
                result.InsertSucceed();
            }
            catch (Exception ex)
            {
                result.message = ex.Message;
                result.isSucceed = false;
                context.Database.RollbackTransaction();
            }
            return result;
        }



    }
}
