﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Utilities
{
    public class Base64Helper
    {
        public static string ConvertToBase64(IFormFile image)
        {
            string result = "data:";
            string base64 = "";
            using (var ms = new MemoryStream())
            {
                image.CopyTo(ms);
                var fileBytes = ms.ToArray();
                base64 = Convert.ToBase64String(fileBytes);
                result += image.ContentType;
                result += String.Format("{0}{1}",";base64,", base64);
            }

            return result;
        }
    }
}
