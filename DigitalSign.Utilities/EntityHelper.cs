﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Utilities
{
    public class EntityHelper
    {
        public static void SetAuditFieldsForInsert(dynamic entity, string username)
        {
            var date = DateTime.Now;
            entity.CreatedDate = date;
            entity.CreatedBy = username;
            entity.UpdatedDate = date;
            entity.UpdatedBy = username;
        }

        public static void SetAuditFieldsForUpdate(dynamic entity, string username)
        {
            entity.UpdatedDate = DateTime.Now;
            entity.UpdatedBy = username;
        }
    }
}
