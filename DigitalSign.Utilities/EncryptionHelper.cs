﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Utilities
{
    public class EncryptionHelper
    {
        public static readonly string ENCRYPTION_PASSWORD = "Iglo@123";
        public static readonly bool USE_WINDOW_AUTHENTICATION = false;
        private static readonly byte[] saltBytes = new byte[]
        {
            27,
            253,
            138,
            182,
            154,
            221,
            9,
            217
        };

        private static readonly int keySize = 256;

        private static readonly int blockSize = 128;

        private static readonly CipherMode mode = CipherMode.CBC;

        private static readonly int iterations = 1000;

        public static string Encrypt(string input, string password)
        {
            byte[] originalBytes = Encoding.UTF8.GetBytes(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
            byte[] saltBytes = EncryptionHelper.GetRandomBytes();
            byte[] bytesToBeEncrypted = new byte[saltBytes.Length + originalBytes.Length];
            for (int i = 0; i < saltBytes.Length; i++)
            {
                bytesToBeEncrypted[i] = saltBytes[i];
            }
            for (int i = 0; i < originalBytes.Length; i++)
            {
                bytesToBeEncrypted[i + saltBytes.Length] = originalBytes[i];
            }
            byte[] encryptedBytes = EncryptionHelper.Encrypt(bytesToBeEncrypted, passwordBytes);
            return Convert.ToBase64String(encryptedBytes);
        }

        public static string Decrypt(string input, string password)
        {
            byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
            byte[] bytesDecrypted = EncryptionHelper.Decrypt(bytesToBeDecrypted, passwordBytes);
            int _saltSize = 4;
            byte[] originalBytes = new byte[bytesDecrypted.Length - _saltSize];
            for (int i = _saltSize; i < bytesDecrypted.Length; i++)
            {
                originalBytes[i - _saltSize] = bytesDecrypted[i];
            }
            return Encoding.Default.GetString(originalBytes);
        }

        private static byte[] GetRandomBytes()
        {
            int _saltSize = 4;
            byte[] ba = new byte[_saltSize];
            RandomNumberGenerator.Create().GetBytes(ba);
            return ba;
        }

        private static byte[] Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;
            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.Mode = EncryptionHelper.mode;
                    Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(passwordBytes, EncryptionHelper.saltBytes, EncryptionHelper.iterations);
                    AES.Key = key.GetBytes(EncryptionHelper.keySize / 8);
                    AES.IV = key.GetBytes(EncryptionHelper.blockSize / 8);
                    using (CryptoStream cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }
            return encryptedBytes;
        }

        private static byte[] Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;
            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.Mode = EncryptionHelper.mode;
                    Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(passwordBytes, EncryptionHelper.saltBytes, EncryptionHelper.iterations);
                    AES.Key = key.GetBytes(EncryptionHelper.keySize / 8);
                    AES.IV = key.GetBytes(EncryptionHelper.blockSize / 8);
                    using (CryptoStream cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }
            return decryptedBytes;
        }

        public static string GetUniqueKey(int size)
        {
            char[] chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[4 * size];
            using (var crypto = RandomNumberGenerator.Create())
            {
                crypto.GetBytes(data);
            }
            StringBuilder result = new StringBuilder(size);
            for (int i = 0; i < size; i++)
            {
                var rnd = BitConverter.ToUInt32(data, i * 4);
                var idx = rnd % chars.Length;

                result.Append(chars[idx]);
            }

            return result.ToString();
        }

        public static string GetUniqueKeyOriginal_BIASED(int size)
        {
            char[] chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[size];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

		public static string EncryptUrlParam(string urlParam)
		{
			string key = "poje874293#";

			byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
			byte[] EncryptKey = Encoding.UTF8.GetBytes(key.Substring(0, 8));

			DESCryptoServiceProvider des = new DESCryptoServiceProvider();
			byte[] inputByte = Encoding.UTF8.GetBytes(urlParam);
			MemoryStream mStream = new MemoryStream();
			CryptoStream cStream = new CryptoStream(mStream, des.CreateEncryptor(EncryptKey, IV), CryptoStreamMode.Write);
			cStream.Write(inputByte, 0, inputByte.Length);
			cStream.FlushFinalBlock();
			var base64 = Convert.ToBase64String(mStream.ToArray()).Replace("+", "0pXPl5").Replace("/", "8rYj3i").Replace("=", "12Y13e");
			return base64;
		}

		public static string DecryptUrlParam(string urlParam)
		{
			urlParam = urlParam.Replace("0pXPl5", "+").Replace("8rYj3i", "/").Replace("12Y13e", "=");
			string key = "poje874293#";

			byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
			byte[] inputByte = new byte[urlParam.Length];

			byte[] DecryptKey = Encoding.UTF8.GetBytes(key.Substring(0, 8));
			DESCryptoServiceProvider des = new DESCryptoServiceProvider();
			inputByte = Convert.FromBase64String(urlParam);
			MemoryStream ms = new MemoryStream();
			CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(DecryptKey, IV), CryptoStreamMode.Write);
			cs.Write(inputByte, 0, inputByte.Length);
			cs.FlushFinalBlock();
			Encoding encoding = Encoding.UTF8;
			return encoding.GetString(ms.ToArray());
		}
	}
}
