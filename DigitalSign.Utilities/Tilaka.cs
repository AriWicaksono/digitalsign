﻿using DigitalSign.ViewModels.Security;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Net.Http.Headers;
using System.Net.Http.Json;

namespace DigitalSign.Utilities
{
    public class Tilaka
	{
        private static readonly string client_id = DataConfiguration.Configuration.AuthorizationTilaka.client_id;
        private static readonly string grant_type = DataConfiguration.Configuration.AuthorizationTilaka.grant_type;
        private static readonly string client_secret = DataConfiguration.Configuration.AuthorizationTilaka.client_secret;
        public static string base_url = DataConfiguration.Configuration.AuthorizationTilaka.endpoint_tilaka;

        public static string auth = base_url + "auth";
        public static string signing_requestsign = base_url + "signing-requestsign";
        public static string linked_account = base_url + "personal-webview/link-account";
        public static string check_exist_account = base_url + "checkAkunDSExist";
        public static string get_uuid = base_url + "generateUUID";
        public static string registerForKycCheck = base_url + "registerForKycCheck";
        public static string download_signed_document = base_url + "signing-downloadsignedpdf";
        public static string document_type = base_url + "emeterai-jenisdoc";
        public static string stamp_doc = base_url + "emeterai-stamping";
        public static string restamp_doc = base_url + "emeterai-restamping";

        public static async Task<string> GetTilakaToken()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("client_id", client_id);
            data.Add("grant_type", grant_type);
            data.Add("client_secret", client_secret);

            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, auth) { Content = new FormUrlEncodedContent(data) };
            var response = await client.SendAsync(request);

            var json = await response.Content.ReadAsStringAsync();
            var obj = JObject.Parse(json);
            return obj["access_token"].ToString();
        }

        public static string GetTilakaTokenSync()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("client_id", client_id);
            data.Add("grant_type", grant_type);
            data.Add("client_secret", client_secret);

            var client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, auth) { Content = new FormUrlEncodedContent(data) };
            var response = client.Send(request);

            var json = response.Content.ToString();
            var obj = JObject.Parse(json);
            return obj["access_token"].ToString();
        }

        public static async Task<string> GenerateUUID(string accessToken)
        {
            HttpClient tRequest = new HttpClient();
            tRequest.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            Task<HttpResponseMessage> getTask = tRequest.PostAsJsonAsync(new Uri(get_uuid).ToString(), "");

            HttpResponseMessage response = await getTask;

            var UUIDJson = await response.Content.ReadAsStringAsync();
            var UUID = JObject.Parse(UUIDJson);
            var UUIDData = UUID["data"] as JArray;
            return UUIDData[0].ToString();
        }

        public static async Task<ResponseDownloadPDF> DownloadSignedPDF(string request_id)
        {
            var token = await GetTilakaToken();
            string json = JsonConvert.SerializeObject(new { request_id = request_id });
            var viewModel = RestAPIHelper<ResponseDownloadPDF>.Submit(json, Method.POST, download_signed_document, token);
            return viewModel;
        }

        public static void LoggingTilaka(string value, string location) 
        {
            if (!File.Exists(location))
            {
                //File.Create(fileName).Dispose();

                using (TextWriter tw = new StreamWriter(location))
                {
                    tw.WriteLine(value);
                }

            }
            else if (File.Exists(location))
            {
                using (TextWriter tw = File.AppendText(location))
                {
                    tw.WriteLine(value);
                }
            }
        }
    }
}
