﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Utilities
{
    public static class ApiUrl
    {
        public static string Master => $"{DataConfiguration.Configuration.AppConfiguration.ApiServerUrlMaster}/Master/";
        public static string Security => $"{DataConfiguration.Configuration.AppConfiguration.ApiServerUrlSecurity}/Security/";
    }
    public class Route
    {
        public static readonly string GetAll = "/GetAll";
        public static readonly string Save = "/Save";
        public static readonly string Delete = "/Delete";
        public static readonly string GetByID = "/GetByID";
    }
    public class DataConfiguration
    {
        public static Configuration Configuration
        {
            get
            {
                string json = System.IO.File.ReadAllText("appsettings.json");
                var data = JsonConvert.DeserializeObject<Configuration>(json);
                return data;
            }
        }
    }
    public class Configuration
    {
        public AppConfiguration AppConfiguration { get; set; }
        public SmtpSettings SmtpSettings { get; set; }
        public ConnectionStrings ConnectionStrings { get; set; }
        public AuthorizationTilaka AuthorizationTilaka { get; set; }
        public int RoleUserId { get; set; }
        public string RoleSuperAdmin { get; set; }

    }

    public class SmtpSettings
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string DisplayName { get; set; }
    }
    public class ConnectionStrings
    {
        public string DefaultConnection { get; set; }
    }
    public class AuthorizationTilaka
    {
        public string client_id { get; set; }
        public string grant_type { get; set; }
        public string client_secret { get; set; }
        public string endpoint_tilaka { get; set; }
    }
    public class AppConfiguration
    {
        public string? ApiServerUrlMaster { get; set; }
        public string? ApiServerUrlSecurity { get; set; }
    }
    public class RestAPIHelper<T>
    {
        public static T Submit(string jsonBody, Method httpMethod, string endpoint)
        {
            var requests = new RestRequest(httpMethod);
            requests.AddHeader("Content-Type", "application/json");
            requests.Timeout = 300000;
            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }

            var client = new RestClient(endpoint);
            IRestResponse response = client.Execute(requests);
            var result = JsonConvert.DeserializeObject<T>(response.Content);
            return result;
        }

        public static T HitWorkflow(string jsonBody, Method httpMethod, string endpoint)
        {
            var requests = new RestRequest(httpMethod);
            requests.AddHeader("Content-Type", "application/json");
            requests.AddHeader("token", "2nUhSbnWJXEPn65hEmq4KA12Y13e12Y13e");
            requests.Timeout = 300000;
            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }

            var client = new RestClient(endpoint);
            IRestResponse response = client.Execute(requests);
            var result = JsonConvert.DeserializeObject<T>(response.Content);
            return result;
        }
        public static T Submit(string jsonBody, Method httpMethod, string endpoint, HttpRequest httpRequest)
        {

            var requests = new RestRequest(httpMethod);
            requests.AddHeader("Content-Type", "application/json");
            requests.Timeout = 300000;
            string token = null;
            var cookie = httpRequest.Cookies["token"];
            token = cookie;

            if (!string.IsNullOrEmpty(token))
            {
                requests.AddHeader("Authorization", $"Bearer {token}");
            }

            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }
            var client = new RestClient(endpoint);
            IRestResponse response = client.Execute(requests);

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new System.Exception("UnAuthorize");
            }
            var result = JsonConvert.DeserializeObject<T>(response.Content);

            return result;
        }

        public static T Submit(string jsonBody, Method httpMethod, string endpoint, string token)
        {

            var requests = new RestRequest(httpMethod);
            requests.AddHeader("Content-Type", "application/json");
            requests.Timeout = 300000;
            //string token = null;
            //var cookie = httpRequest.Cookies["token"];
            //token = cookie;

            if (!string.IsNullOrEmpty(token))
            {
                requests.AddHeader("Authorization", $"Bearer {token}");
            }

            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }
            var client = new RestClient(endpoint);
            IRestResponse response = client.Execute(requests);

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new System.Exception("UnAuthorize");
            }
            var result = JsonConvert.DeserializeObject<T>(response.Content);

            return result;
        }

        public static T Submit(string jsonBody, Method httpMethod, string endpoint, HttpRequest httpRequest, ref string message)
        {
            var requests = new RestRequest("/", httpMethod);

            requests.AddHeader("Content-Type", "application/json");
            requests.AddHeader("Authorization", string.Format("Bearer " + httpRequest.Cookies["token"]));
            requests.Timeout = 7200000; // 7200000 millisecond = 120 Menit

            if (!string.IsNullOrEmpty(jsonBody))
            {
                requests.AddParameter("application/json", jsonBody, ParameterType.RequestBody);
            }
            var client = new RestClient(endpoint);
            //client.ReadWriteTimeout = 7200000; // 7200000 millisecond = 120 Menit
            //client.Timeout = 7200000; // 7200000 millisecond = 120 Menit
            IRestResponse response = client.Execute(requests);


            if (response.IsSuccessful)
            {
                message = response.StatusDescription;
                return JsonConvert.DeserializeObject<T>(response.Content);
            }
            else
            {
                return default(T);
            }
        }

    }

}
