﻿using DigitalSign.ViewModels.Master.Notification;
using DigitalSign.Utilities;
using Microsoft.AspNetCore.Http;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace DigitalSign.Utilities
{
    public class ClaimHelper
    {
        readonly IHttpContextAccessor _context;
        private static ClaimHelper instance = new ClaimHelper(new HttpContextAccessor());

        public ClaimHelper(IHttpContextAccessor httpContextAccessor)
        {
            this._context = httpContextAccessor;
        }

        public static ClaimHelper Instance
        {
            get { return instance; }
        }

        public string Claim(string param)
        {
            if (!String.IsNullOrEmpty(_context.HttpContext.Request.Cookies["token"]))
            {
                string token = _context.HttpContext.Request.Cookies["token"];
                var decodeToken = new JwtSecurityToken(jwtEncodedString: token);
                var expDate = new DateTime(1970, 1, 1, 7, 0, 0, 0).AddSeconds(Convert.ToDouble(decodeToken.Claims.First(x => x.Type == "exp").Value));
                if (expDate < DateTime.Now)
                {
                    return "";
                }
                else
                {
                    string data = decodeToken.Claims.First(x => x.Type == param).Value;
                    return data;
                }
            }
            else
            {
                return "";
            }
        }

        public string GetToken()
        {
            return _context.HttpContext.Request.Cookies["token"];
        }

        public static bool CheckSession
        {
            get
            {
                var CheckIdentity = String.IsNullOrEmpty(Instance.Claim("LoginID")) ? 0 : Convert.ToInt32(Instance.Claim("LoginID"));
                if (CheckIdentity != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public static int LoginID
        {
            get
            {
                var CheckIdentity = String.IsNullOrEmpty(Instance.Claim("LoginID")) ? 0 : Convert.ToInt32(Instance.Claim("LoginID"));

                return CheckIdentity;

            }
        }

        public static string UserName
        {
            get
            {
                var CheckIdentity = Instance.Claim(ClaimTypes.Name);

                return CheckIdentity;

            }
        }

        public static string RoleName
        {
            get
            {
                var CheckIdentity = Instance.Claim(ClaimTypes.Role);
                return CheckIdentity;
            }
        }

        public static bool IsAdmin
        {
            get
            {
                var CheckIdentity = String.IsNullOrEmpty(Instance.Claim("IsAdmin")) ? false : Convert.ToBoolean(Instance.Claim("IsAdmin"));

                return CheckIdentity;

            }
        }

        public static List<NotificationViewModel> GetNotification(int userID)
        {
            string endpoint = $"{ApiUrl.Master}Notification/GetNotif?userID={userID}";
            var result = RestAPIHelper<List<NotificationViewModel>>.Submit("", RestSharp.Method.GET, endpoint, Instance.GetToken());
            return result;
        }

    }
}
