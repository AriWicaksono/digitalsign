﻿using DigitalSign.ViewModels;
using Kendo.Mvc.UI;
using Kendo.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Utilities
{
    public class GridUtilities
    {
        public static string ConvertKendoRequestToJson(DataSourceRequest request)
        {
            try
            {
                CustomDataSourceRequest customDataSourceRequest = new CustomDataSourceRequest();

                customDataSourceRequest.Page = request.Page;
                customDataSourceRequest.PageSize = request.PageSize;
                customDataSourceRequest.Sorts = request.Sorts != null ? request.Sorts.ToList() : null;
                customDataSourceRequest.Filters = new List<CustomDataSourceRequest.Filter>();

                if (request.Filters != null)
                {
                    foreach (var filter in request.Filters)
                    {
                        if (filter.GetType() == typeof(Kendo.Mvc.FilterDescriptor))
                        {
                            var filterItem = (Kendo.Mvc.FilterDescriptor)filter;

                            CustomDataSourceRequest.Filter customFilter = new CustomDataSourceRequest.Filter();
                            customFilter.FilterDescriptors = new List<CustomDataSourceRequest.FilterDescriptor>();
                            // (filterItem.Member, (FilterOperator)(filterItem.Operator), filterItem.Value);

                            customFilter.FilterType = "FilterDescriptor";

                            customFilter.FilterDescriptors.Add(new CustomDataSourceRequest.FilterDescriptor()
                            {
                                Member = filterItem.Member,
                                Operator = (int)filterItem.Operator,
                                Value = filterItem.Value,
                                MemberType = filterItem.Value.GetType()
                            });

                            customDataSourceRequest.Filters.Add(customFilter);
                        }
                        if (filter.GetType() == typeof(CompositeFilterDescriptor))
                        {
                            CompositeFilterDescriptor compositeFilterDescriptor = (CompositeFilterDescriptor)filter;

                            CustomDataSourceRequest.Filter compositeFilter = new CustomDataSourceRequest.Filter();
                            compositeFilter.FilterDescriptors = new List<CustomDataSourceRequest.FilterDescriptor>();

                            // (filterItem.Member, (FilterOperator)(filterItem.Operator), filterItem.Value);

                            compositeFilter.FilterType = "CompositeFilter";
                            compositeFilter.LogicalOperator = (int)compositeFilterDescriptor.LogicalOperator;

                            foreach (var compFilter in compositeFilterDescriptor.FilterDescriptors)
                            {
                                var filItem = (Kendo.Mvc.FilterDescriptor)compFilter;

                                compositeFilter.FilterDescriptors.Add(new CustomDataSourceRequest.FilterDescriptor()
                                {
                                    Member = filItem.Member,
                                    Operator = (int)filItem.Operator,
                                    Value = filItem.Value,
                                    MemberType = filItem.Value.GetType()
                                });
                            }

                            customDataSourceRequest.Filters.Add(compositeFilter);
                        }
                    }
                }

                var jsonCustom = JsonConvert.SerializeObject(customDataSourceRequest);
                return jsonCustom;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataSourceRequest ConvertToKendoFromCustomRequest(CustomDataSourceRequest customDataSourceRequest)
        {
            try
            {
                DataSourceRequest req = new DataSourceRequest();

                req.Page = customDataSourceRequest.Page;
                req.PageSize = customDataSourceRequest.PageSize;

                List<SortDescriptor> sorts = new List<SortDescriptor>();

                if (customDataSourceRequest.Sorts != null)
                {
                    foreach (var item in customDataSourceRequest.Sorts)
                    {
                        SortDescriptor newSort = new SortDescriptor();
                        newSort.Member = item.Member;
                        newSort.SortCompare = null; //item.SortCompare;
                        newSort.SortDirection = (ListSortDirection)item.SortDirection;
                        sorts.Add(newSort);
                    }
                }

                req.Sorts = sorts;

                List<IFilterDescriptor> filters = new List<IFilterDescriptor>();

                foreach (var item in customDataSourceRequest.Filters)
                {
                    if (item.FilterType == "FilterDescriptor")
                    {
                        var filItem = item.FilterDescriptors.FirstOrDefault();

                        Kendo.Mvc.FilterDescriptor filterDescriptor = new Kendo.Mvc.FilterDescriptor(filItem.Member, (FilterOperator)(filItem.Operator), filItem.Value.ToString());

                        var memberType = ((System.Text.Json.JsonElement)filItem.MemberType).ToString();
                        filterDescriptor.MemberType = Type.GetType(memberType);
                        filterDescriptor.Value = GetConvertedValue(filterDescriptor);

                        filters.Add(filterDescriptor);
                    }
                    if (item.FilterType == "CompositeFilter")
                    {
                        CompositeFilterDescriptor compositeFilter = new CompositeFilterDescriptor();
                        compositeFilter.LogicalOperator = (FilterCompositionLogicalOperator)item.LogicalOperator;
                        foreach (var filterItem in item.FilterDescriptors)
                        {
                            Kendo.Mvc.FilterDescriptor filterDescriptor = new Kendo.Mvc.FilterDescriptor(filterItem.Member, (FilterOperator)(filterItem.Operator), filterItem.Value.ToString());
                            var memberType = ((System.Text.Json.JsonElement)filterItem.MemberType).ToString();
                            filterDescriptor.MemberType = Type.GetType(memberType);
                            filterDescriptor.Value = GetConvertedValue(filterDescriptor);
                            compositeFilter.FilterDescriptors.Add(filterDescriptor);
                        }
                        filters.Add(compositeFilter);
                    }

                }

                req.Filters = filters;

                return req;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object GetConvertedValue(Kendo.Mvc.FilterDescriptor filterDescriptor)
        {
            var memberTypeName = filterDescriptor.MemberType.Name;
            var valueString = filterDescriptor.Value.ToString();
            switch (memberTypeName)
            {
                case "String":
                    return valueString;
                case "Double":
                    return double.Parse(valueString, System.Globalization.CultureInfo.InvariantCulture);
                case "DateTime":
                    return DateTime.Parse(valueString);
                default:
                    return null;
            }
        }
    }
}
