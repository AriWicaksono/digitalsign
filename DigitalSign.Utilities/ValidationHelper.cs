﻿using DigitalSign.ViewModels.Master;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Utilities
{
    public class ValidationHelper
    {
        public static IEnumerable<ValidationMessageVM> GetValidationMessage(ModelStateDictionary modelState)
        {
            var validationMessages = new List<ValidationMessageVM>();
            foreach (KeyValuePair<string, ModelStateEntry> error in modelState)
            {
                if (error.Value.Errors.Count < 1)
                {
                    continue;
                }
                else
                {
                    string firstErrorMessage = error.Value.Errors[0].ErrorMessage;
                    validationMessages.Add(new ValidationMessageVM(error.Key, firstErrorMessage));
                }
            }
            return validationMessages;
        }
    }
}
