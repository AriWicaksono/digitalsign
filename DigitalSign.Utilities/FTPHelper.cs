﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Utilities
{
    public static class FTPHelper
    {
        public static FTPResult CreateFolder(string folderName, string _ftpUsername, string _ftpPassword, string _ftpAddress)
        {
            //var ftpUsername = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpUserName"];
            //var ftpPassword = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpPassword"];
            var _result = new FTPResult();
            try
            {
                //string address = GetFtpAddress(folderName);
                string address = $"{_ftpAddress}/{folderName}";
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(address);
                ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
                ftpRequest.Credentials = new NetworkCredential(_ftpUsername, _ftpPassword);
                //ftpRequest.Credentials = new NetworkCredential(_generalSettingService.GetGeneralSettingByKeyword("FtpUsername"), _generalSettingService.GetGeneralSettingByKeyword("FtpPassword"));
                using (var resp = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    _result.Status = resp.StatusCode;
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                _result.Status = response.StatusCode;
            }
            return _result;
        }

        public static FTPResult UploadFile(byte[] fileContents, string filePath, string _ftpUsername, string _ftpPassword, string _ftpAddress)
        {
            //var ftpAddress = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpAddress"];
            //var ftpUsername = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpUserName"];
            //var ftpPassword = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpPassword"];

            var _result = new FTPResult();
            try
            {
                string address = $"{_ftpAddress}/{filePath}";
                //string address = $"{ftpAddress}/{filePath}";
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(address);
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                ftpRequest.Credentials = new NetworkCredential(_ftpUsername, _ftpPassword);
                //ftpRequest.Credentials = new NetworkCredential(_generalSettingService.GetGeneralSettingByKeyword("FtpUsername"), _generalSettingService.GetGeneralSettingByKeyword("FtpPassword"));
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;

                if (fileContents != null)
                {
                    using (MemoryStream requestMemStream = new MemoryStream(fileContents))
                    using (Stream requestStream = ftpRequest.GetRequestStream())
                    {
                        requestMemStream.CopyTo(requestStream);
                    }
                }

                using (FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse())
                using (MemoryStream responseBody = new MemoryStream())
                {

                    response.GetResponseStream().CopyTo(responseBody);
                    FtpWebResponse responseFtp = response as FtpWebResponse;
                    _result.Status = response.StatusCode;
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = ex.Response as FtpWebResponse;
                _result.Status = response.StatusCode;

                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
            return _result;
        }

        public static FTPResult UploadFile(byte[] fileContents, string folderName, string fileName, string _ftpUsername, string _ftpPassword, string _ftpAddress)
        {

            //var ftpUsername = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpUserName"];
            //var ftpPassword = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpPassword"];
            var _result = new FTPResult();
            //string address = GetFtpAddress(folderName, fileName);
            string address = $"{_ftpAddress}/{folderName}/{fileName}";
            try
            {
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(address);
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                ftpRequest.Credentials = new NetworkCredential(_ftpUsername, _ftpPassword);
                //ftpRequest.Credentials = new NetworkCredential(_generalSettingService.GetGeneralSettingByKeyword("FtpUsername"), _generalSettingService.GetGeneralSettingByKeyword("FtpPassword"));
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = false;
                ftpRequest.KeepAlive = true;


                if (fileContents != null)
                {
                    using (MemoryStream requestMemStream = new MemoryStream(fileContents))
                    using (Stream requestStream = ftpRequest.GetRequestStream())
                    {
                        requestMemStream.CopyTo(requestStream);
                    }
                }

                using (FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse())
                using (MemoryStream responseBody = new MemoryStream())
                {

                    response.GetResponseStream().CopyTo(responseBody);
                    FtpWebResponse responseFtp = response as FtpWebResponse;
                    _result.Status = response.StatusCode;
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = ex.Response as FtpWebResponse;
                _result.Status = response.StatusCode;

                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
            return _result;
        }

        public static string GetImageBase64(string folderName, string fileName, string _ftpUsername, string _ftpPassword, string _ftpAddress)
        {
            //var ftpUsername = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpUserName"];
            //var ftpPassword = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpPassword"];
            WebClient request = new WebClient();
            //string address = GetFtpAddress(folderName, fileName);
            string address = $"{_ftpAddress}/{folderName}/{fileName}";
            request.Credentials = new NetworkCredential(_ftpUsername, _ftpPassword);
            //request.Credentials = new NetworkCredential(_generalSettingService.GetGeneralSettingByKeyword("FtpUsername"), _generalSettingService.GetGeneralSettingByKeyword("FtpPassword"));
            string base64String = "";
            try
            {
                byte[] bytes = request.DownloadData(address);
                base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
            }
            return base64String;
        }
        public static FTPResult ReadFile(string folderName, string fileName, string _ftpUsername, string _ftpPassword, string _ftpAddress)
        {
            var _result = new FTPResult();
            try
            {
                string address = $"{_ftpAddress}/{folderName}/{fileName}";
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(address);
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                ftpRequest.Credentials = new NetworkCredential(_ftpUsername, _ftpPassword);
                ftpRequest.UsePassive = false;

                using (var resp = (FtpWebResponse)ftpRequest.GetResponse())
                {

                    Stream responseStream = resp.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream);

                    var result = Encoding.UTF8.GetBytes(reader.ReadToEnd());
                    reader.Close();
                    responseStream.Close();
                    _result.ReturnValue = responseStream;
                    _result.Status = resp.StatusCode;
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                _result.Status = response.StatusCode;
            }
            return _result;
        }
        public static void DeleteFile(string folderName, string fileName, string _ftpUsername, string _ftpPassword, string _ftpAddress)
        {
            //var ftpUsername = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpUserName"];
            //var ftpPassword = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build().GetSection("FTPConfig")["FtpPassword"];
            var _result = new FTPResult();
            try
            {
                //string address = GetFtpAddress(folderName, fileName);
                string address = $"{_ftpAddress}/{folderName}/{fileName}";
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(address);
                ftpRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                ftpRequest.Credentials = new NetworkCredential(_ftpUsername, _ftpPassword);
                //ftpRequest.Credentials = new NetworkCredential(_generalSettingService.GetGeneralSettingByKeyword("FtpUsername"), _generalSettingService.GetGeneralSettingByKeyword("FtpPassword"));

                using (var resp = (FtpWebResponse)ftpRequest.GetResponse())
                {
                    _result.Status = resp.StatusCode;
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                _result.Status = response.StatusCode;
            }
        }
        public static byte[] GetFileContentsByIFormFile(IFormFile file)
        {
            byte[] fileContents;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                fileContents = ms.ToArray();
            }
            return fileContents;
        }

        public static string GetStringBase64(string fileName, string _ftpUsername, string _ftpPassword, string _ftpAddress)
        {
            string fileContents = "";
            try
            {
                string address = $"{_ftpAddress}/{fileName}";
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(address);
                ftpRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                ftpRequest.Credentials = new NetworkCredential(_ftpUsername, _ftpPassword);
                ftpRequest.UsePassive = true;

                FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                Stream responseStream = response.GetResponseStream();

                using (var reader = new MemoryStream())
                {
                    response.GetResponseStream().CopyTo(reader);
                    fileContents = Convert.ToBase64String(reader.ToArray(), 0, reader.ToArray().Length);
                }
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
            }
            return fileContents;
        }
    }

    public class FTPResult
    {
        public FtpStatusCode Status { get; set; }
        public Stream ReturnValue { get; set; }
        public string CustomReturnValue { get; set; }
    }

}