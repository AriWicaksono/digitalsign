﻿using DigitalSign.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DigitalSign.Provider.Base
{
    public interface IBaseMasterProvider<T> where T : class
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        ProcessResult Save(T model, string displayName);
        ProcessResult Delete(int id);
    }

    public class BaseMasterProvider
    {

    }
}
