﻿using DigitalSign.DataAccess.Context;
using DigitalSign.DataAccess.Models;
using DigitalSign.ViewModels;

namespace DigitalSign.Provider.Base
{
    public class ActivityLogProvider
    {
        private readonly DigitalSignContext context;

        public ActivityLogProvider(DigitalSignContext context)
        {
            this.context = context;
        }

        public void InsertActivityLog(int userId, string activity, string deskripsi)
        {
            try
            {
                //insert activity log
                var activityLog = new ActivityLog()
                {
                    UserID = userId,
                    Activity = activity,
                    Description = deskripsi,
                    TimeStamp = DateTime.Now
                };
                context.ActivityLog.Add(activityLog);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable<ActivityLogViewModel> GetActivity(int userId)
		{
			try
			{
                var activity = (from act in context.ActivityLog
                                where act.UserID == userId
                                select new ActivityLogViewModel
                                {
                                    ID = act.ID,
                                    UserID = act.UserID,
                                    Activity = act.Activity,
                                    TimeStamp = act.TimeStamp,
                                    Deskripsi = act.Description
                                });
                return activity;
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}
    }
}
